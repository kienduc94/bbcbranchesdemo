#!/bin/bash

# works with a file called VERSION in the current directory,
# the contents of which should be a semantic version number
# such as "1.2.3"

# this script will display the current version, automatically
# suggest a "minor" version update, and ask for input to use
# the suggestion, or a newly entered value.

# once the new version number is determined, the script will
# pull a list of changes from git history, prepend this to
# a file called CHANGES (under the title of the new version
# number) and create a GIT tag.

#stty erase ^H

function pass_back_a_string() {
	V_MAJOR="${1}" # $1 represent first argument
	V_MINOR="${2}" # $2 represent first argument
    V_PATCH="${3}" # $3 represent second argument
	INPUT="${4}" # $4 represent first argument
	
	OUTPUT_RESULT="v"
	if [ "$INPUT" = "1" ]; then
		#Release Beta branch
		V_MINOR1=$((V_MINOR + 1))
		V_PATCH1=0
		
		SUGGESTED_VERSION="$V_MAJOR.$V_MINOR1.$V_PATCH1"
		
		read -e -p "Enter a version number [$SUGGESTED_VERSION]: release-" INPUT_STRING
		
		OUTPUT_RESULT="release-"
	#elif [ "$INPUT" = "2" ]; then
		#Release Live branch
		#V_MINOR1=$((V_MINOR + 1))
		#V_PATCH1=0
		
		#SUGGESTED_VERSION="$V_MAJOR.$V_MINOR1.$V_PATCH1"
		
		#read -e -p "Enter a version number [$SUGGESTED_VERSION]: release-live-" INPUT_STRING
		
		#OUTPUT_RESULT="release-live-"
	elif [ "$INPUT" = "2" ]; then
		#HotFix branch
		V_PATCH1=$((V_PATCH + 1))
		SUGGESTED_VERSION="$V_MAJOR.$V_MINOR.$V_PATCH1"
		
		read -e -p "Enter a version number [$SUGGESTED_VERSION]: hotfix-" INPUT_STRING
		
		OUTPUT_RESULT="hotfix-"
	else
		#default
		V_MINOR1=$((V_MINOR + 1))
		V_PATCH1=0
		
		SUGGESTED_VERSION="$V_MAJOR.$V_MINOR1.$V_PATCH1"
		
		read -e -p "Enter a version number [$SUGGESTED_VERSION]: v" INPUT_STRING
	fi
   
    if [ "$INPUT_STRING" = "" ]; then
        INPUT_STRING=$SUGGESTED_VERSION
	else
		INPUT_STRING="$OUTPUT_RESULT$INPUT_STRING"
    fi
	
	BASE_LIST=(`echo $INPUT_STRING | tr '.' ' '`)
	
	COUNT_LIST="${#BASE_LIST[*]}"
	if (($COUNT_LIST != 3)); then
		#LIST INPUT MUST BE 0.0.0 -> count = 3
		
		return_var=`pass_back_a_string $V_MAJOR $V_MINOR $V_PATCH $INPUT`
		INPUT_STRING="$return_var"
    fi
	
	echo "$INPUT_STRING"
}

if [ -f VERSION ]; then
	
	echo "The branches are as follows: "
	echo " 1 - Release branch"
	#echo " 2 - Release Live branch"
	echo " 2 - HotFix branch"
	echo " 3 - Default"
	read -e -p "Please Enter branches to set tag name (1 of 3): " INPUT_STRING

	V_VERSION="v"
	V_RESULT="You chose branches: Default"
	
	if [ "$INPUT_STRING" = "1" ]; then
		#Release branch
		
		V_RESULT="You chose branches: Release branch"
		
		if [ -f VERSION-LIST ]; then
			filelines=`cat VERSION-LIST`
			
			for _ver in $filelines; do
				if [[ $_ver = *"release-"* ]]; then
				  V_VERSION="$_ver"
				fi
			done
			
			if [ "$V_VERSION" = "v" ]; then
				V_VERSION="release-0.0.0"
			fi
		else
			V_VERSION="release-0.0.0"
		fi
		
	#elif [ "$INPUT_STRING" = "2" ]; then
		#Release Live branch
		#V_RESULT="You chose branches: Release Live branch"
		
		#if [ -f VERSION-LIST ]; then
			#filelines=`cat VERSION-LIST`
			
			#for _ver in $filelines; do
				#if [[ $_ver = *"release-live-"* ]]; then
				  #V_VERSION="$_ver"
				#fi
			#done
			
			#if [ "$V_VERSION" = "v" ]; then
				#V_VERSION="release-live-0.0.0"
			#fi
		#else
			#V_VERSION="release-live-0.0.0"
		#fi
		
	elif [ "$INPUT_STRING" = "2" ]; then
		#HotFix branch
		V_RESULT="You chose branches: HotFix branch"
		
		if [ -f VERSION-LIST ]; then
			filelines=`cat VERSION-LIST`
			
			for _ver in $filelines; do
				if [[ $_ver = *"hotfix-"* ]]; then
				  V_VERSION="$_ver"
				fi
			done
			
			if [ "$V_VERSION" = "v" ]; then
				V_VERSION="hotfix-0.0.0"
			fi
		else
			V_VERSION="hotfix-0.0.0"
		fi
	else
		
		if [ -f VERSION-LIST ]; then
			filelines=`cat VERSION-LIST`
			
			for _ver in $filelines; do
				if [[ $_ver = "v"* ]]; then
					echo "$_ver"
				  V_VERSION="$_ver"
				fi
			done
			
			
			if [ "$V_VERSION" = "v" ]; then
				V_VERSION="v0.0.0"
			fi
		else
			V_VERSION="v0.0.0"
		fi
	fi
	
	echo "$V_RESULT"
	
	echo "Current version : $V_VERSION"
	
	BASE_LIST=(`echo $V_VERSION | tr '.' ' '`)
	
    V_MAJOR=${BASE_LIST[0]}
    V_MINOR=${BASE_LIST[1]}
    V_PATCH=${BASE_LIST[2]}
	
	
	return_var=`pass_back_a_string $V_MAJOR $V_MINOR $V_PATCH $INPUT_STRING`
	INPUT_STRING="$return_var"
	
	#get previous version
	BASE_STRING=`cat VERSION`
	
    echo "Will set new version to be $INPUT_STRING"
    echo $INPUT_STRING > VERSION
    echo "Version $INPUT_STRING:" > tmpfile
   
	#get log and action (EX: add file abc, edit file...) and write log to file CHANGES
    git log --pretty=format:" - %s" "$BASE_STRING"...HEAD >> tmpfile
    echo "" >> tmpfile
    echo "" >> tmpfile
    cat CHANGES >> tmpfile
    mv tmpfile CHANGES
    git add CHANGES VERSION
    git commit -m "Version bump to $INPUT_STRING"
    git tag -a -m "Tagging version $INPUT_STRING" "$INPUT_STRING"
    git push origin --tags
	
	echo -e "$INPUT_STRING"$'\r' >> VERSION-LIST
	
else
 git log --pretty=format:" - %s" "v$BASE_STRING"...HEAD >> tmpfile
    echo "Could not find a VERSION file"
    read -e -p "Do you want to create a version file and start from scratch? [y]" RESPONSE
    if [ "$RESPONSE" = "" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Y" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "Yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "yes" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "YES" ]; then RESPONSE="y"; fi
    if [ "$RESPONSE" = "y" ]; then
        echo "1.0.0" > VERSION
        echo "Version 1.0.0" > CHANGES
        git log --pretty=format:" - %s" >> CHANGES
        echo "" >> CHANGES
        echo "" >> CHANGES
        git add VERSION CHANGES
        git commit -m "Added VERSION and CHANGES files, Version bump to v1.0.0"
        git tag -a -m "Tagging version 1.0.0" "v1.0.0"
        git push origin --tags
    fi

fi
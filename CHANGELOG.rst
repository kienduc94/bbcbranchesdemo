﻿CSCJackpot 2.0.0 3-8-2018
-------------------------

User:
~~~~
- Deposit BTC
- Play Game
- Authentication
- Get free tickets
- Withdrawl
- Check lucky draw
- History
- Homepage

Admin:
~~~~~
- Authentication
- Dashboard reporting 
- Member mgmt
- Ticket mgmt
- Adward mgmt
- Approve receive BTC

CSCJackpot 2.0.4 14-8-2018
-------------------------

1. BCCJ-305:	Email verify template
2. BCCJ-306:	Email forgot password template
3. BCCJ-313:	Homepage footer is the same with play-game
4. BCCJ-315:	SSL to cscjackpot
5. BCCJ-316:	Window server guide
			- Write document guide server, services, website, database...
6. BCCJ-318:	Import user data from CscBOS
7. BCCJ-319:	Web hook coinbase
8. BCCJ-321:	Modify CSC Jackpot and related rights belong to Partners
9. BCCJ-322:	Remove link mycarcoin.com at aboutus page
10. BCCJ-324:	up code checking tranparency on GitHub cscjackpot
11. BCCJ-325:	Open mainnet on beta server
12. BCCJ-328:	Add SSL
13. BCCJ-334:	Change content of email
14. BCCJ-337:	Import 30 users by postman
15. BCCJ-338:	Point API mycarcoin.com
16. BCCJ-343:	Hotfix smsspeed
17. BCCJ-344:	Thay đổi tiêu đề của email verify and reset password
18. BCCJ-345:	Giảm size đếm ngược và tăng size Jackpot trên homepage
19. BCCJ-347:	Link ref hiển thị khi redirect qua các trang
                	- add key config <add key="TimeCookiesLinkRef" value="15" />
20. BCCJ-348:	Đổi mật khẩu cmsadmin, accounting
21. BCCJ-366:	Additional Feature: "Change Password of web site CMS Admin"
			- Chuyển site-key (captcha) từ view ra web.config (CMS Admin).
			- issues BCCJ-348 & BCCJ-366 là 1
22. BCCJ-349:	Chức năng copy link ref ko work trên mobile
			- Edit _LayoutLotteryCarcoin.cshtml, demo.css, style.css
23. BCCJ-350:	Lỗi 404 verify email khi user đăng kì bên BOS
24. BCCJ-352:	Urgent-Double spend Get Adward
			- Update hot fix store SP_GetListTicketWinningByBookingID (134.119.181.117)
25. BCCJ-365:	Fix error when call api update AwardMegaball BOS.

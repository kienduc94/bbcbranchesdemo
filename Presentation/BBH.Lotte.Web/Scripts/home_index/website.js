var Website = {
	run: function(){
		$('.search-icon').click(function(){
			if($('.search-form').hasClass('active')) $('.search-form').removeClass('active');
			else $('.search-form').addClass('active');
		})
		$('html').click(function() {
		   $('.search-form').removeClass('active');
		});
		$('.search-form,.search-icon').click(function(event){
		   event.stopPropagation();
		});
		if($(window).width() <= 991){
			$('.adv-thumb').height($('.adv-thumb').width()/59*75);
			$(window).resize(function(){
				$('.adv-thumb').height($('.adv-thumb').width()/59*75);
			})
		}
		$('.news-thumb').each(function(index, element) {
            $(this).height($(this).width()/74*47);
        });
		
		$('.product-thumb').height($('.product-thumb').width()+20).width($('.product-content').width());
		$(window).resize(function(){
			$('.news-thumb').each(function(index, element) {
				$(this).height($(this).width()/74*47);
			});
			$('.product-thumb').height($('.product-thumb').width()+20).width($('.product-content').width());
		})
		$('.fanpage .fb-like-box').attr('data-width',$('.fanpage').width());
		var height_nav = $('.navbar-desktop').height();
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) $('#goTop').fadeIn();
			else $('#goTop').fadeOut();
			if ($(this).scrollTop() > $('header').outerHeight()){
				$('.navbar-desktop').addClass('fixed').height(height_nav);
			}else{
				$('.navbar-desktop').removeClass('fixed').removeAttr('style');
			}
		});
		$('#goTop').click(function () {
			$('body,html').animate({ scrollTop: 0 }, 'slow');
		});
		$('.alert-icon').click(function(){ $('.alert-block').remove();})
		/*if($(window).width() > 991){
			$(window).scroll(function () {
				if ($(this).scrollTop() > $('.top-page').outerHeight()) $('header').addClass('fixed');
				else $('header').removeClass('fixed');
			});
		}*/
		
	}
};
//Initialize
$(document).ready(function(){
	Website.run();
});
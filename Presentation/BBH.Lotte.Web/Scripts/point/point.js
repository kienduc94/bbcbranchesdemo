﻿$(document).ready(function () {
    GetExchangePoint();
    GetExchangeTicket();
});
function SubmitPointMember() {
    var language = $('#hdLanguage').val();
  

    var strEwallet = $("#txtEwallet").val();
    var strPoint = $("#txtPoint").val();
    if (strPoint == '' || strPoint == '0') {
        
            $('#lbPoint').text('Please input points number');
       
    }
    else {
        if (isNaN(strPoint)) {
           
                $('#lbPoint').text('Point number must be nummeric');
          
        }
        else {
            var textMessage = '';
          
                textMessage = 'Are you sure want to buy this points?';
           
            var confirmMessage = confirm(textMessage);
            if (!confirmMessage) {
                return false;
            }
            else {
                $('#imgLoading').css("display", "");
                $.ajax({
                    type: "post",
                    url: "/Point/BuyPointMember",
                    async: false,
                    data: { strEwallet: strEwallet, strPoint: strPoint },
                    beforeSend: function () {
                        $('#imgLoading').css("display", "");
                    },
                    success: function (d) {
                        
                        if (d == 'insertpointsSuccess') {
                            $('#imgLoading').css("display", "none");

                            $('#txtPoint').val('');
                            $('#numtickets').val('');
                            $('#spanCoin').val('');
                           
                                alertify.alert('Buy Success. We will check and active your points in 30 minutes. <br/> Please transfer BTC to Bitcoin Wallete address: <a href="#' + strEwallet + '&coin=' + strPoint + '" target="_blank" >' + strEwallet + '</a> to active.')
                        
                            //setTimeout(function () { window.location.reload(); }, 3000);
                        }

                        else {
                          
                                alertify.error('Buy error. Please contact with administrator');
                           
                        }

                    },
                    error: function () {

                    }
                });
            }
        }
    }
}
function GetExchangePoint() {
    var defaultValue = 1;
    var coinExchange = 0;
    //var pointvalue = 10.123;
    $("#txtPoint").val(defaultValue);
    $.ajax({
        type: "post",
        url: "/Point/GetExchangePoint",
        async: true,
        beforeSend: function () {
        },
        success: function (d) {
            coinExchange = (defaultValue * d.coinvalue) * d.pointvalue;
            $("#spanCoin").text(coinExchange);
            $("#txtPoint").attr("data-coin", d.coinvalue);
            $("#txtPoint").attr("data-point", d.pointvalue);

            recharge_qr = $("#recharge_qr").attr('src');
            var strQR = recharge_qr + defaultValue;
            $("#recharge_qr").attr('src', strQR)
        },
        error: function () {
            $("#lbPoint").text("Error 404!");
        }
    });
}
function OnchangePoint() {
    var value = $("#txtPoint").val()
    var coin = $("#txtPoint").attr("data-coin");
    var point = $("#txtPoint").attr("data-point");
    var coinExchange = 0;
    coinExchange = (value * coin) / point;
    $("#spanCoin").text(coinExchange);
    var ticketnumber = $("#txtPoint").attr("data-ticketnumber");
    var tickerpoint = $("#txtPoint").attr("data-tickerpoint");
    var ticketExchange = 0;
    ticketExchange = (value * ticketnumber) / tickerpoint;
    $("#spanTicket").text(ticketExchange);
    var strQR = recharge_qr + value;
    $("#recharge_qr").attr('src', strQR)
}
function GetExchangeTicket() {
    var defaultValue = 1;
    var ticketExchange = 0;
    $.ajax({
        type: "post",
        url: "/Point/GetExchangeTicket",
        async: true,
        beforeSend: function () {
        },
        success: function (d) {
            ticketExchange = (defaultValue * d.ticketvalue) / d.pointvalue;
            $("#spanTicket").text(ticketExchange);
            $("#txtPoint").attr("data-ticketnumber", d.ticketvalue);
            $("#txtPoint").attr("data-tickerpoint", d.pointvalue);
        },
        error: function () {
            $("#lbPoint").text("Error 404!");
        }
    });
}

function ResetField() {
    $('#lbPoint').text('');
}
function SearchPointCoin() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();

    var page = 1;
    $.ajax({
        type: "get",
        async: true,
        url: "/Point/SearchTransactionPoint",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPoint').html(json.ContentResult);
                if ($('#ulPaging') && $('#ulPaging').length > 0) {
                    $('#ulPaging').html(json.PagingResult);
                }
            }
            hideLoading();
        },
        error: function (e) {
            alert('a');
        }
    });
}
function PagingSearchPointCoin(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Point/SearchTransactionPoint",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")

            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPoint').html(json.ContentResult);
                if ($('#ulPaging') && $('#ulPaging').length > 0) {
                    $('#ulPaging').html(json.PagingResult);
                }
            }
        },
        error: function (e) {

        }
    });
}
function ShowTransactionWalletDetail(transactionid, yourwalletaddress, receivedwalletaddress, btc, transactionbitcoin, createdate, expireddate) {
    $("#txtTransactionID").val(transactionid);
    $("#txtYourWalletAddress").val(yourwalletaddress);
    $("#txtReceivedWalletAddress").val(receivedwalletaddress);
    $("#txtBTC").val(btc);
    $("#txtTransactionBitcoin").val(transactionbitcoin);
    $("#txtCreateDate").val(createdate);
    $("#txtExpiredDate").val(expireddate);
    var linkTestnet = $('#hdLinkTestNet').val() + transactionbitcoin;
    $('#aLink').attr("href", linkTestnet);
    $("#myModalDetailWallet").show();

    $("#over").show();

}
function CloseModalTransactionWallet() {
    $("#myModalDetailWallet").hide();
    $("#over").hide();
}
function ShowTransactionPointDetail(TransactionCode, E_Wallet, Points, CreateDate, Status) {
    $("#txtTransactionCode").val(TransactionCode);
    $("#txtYourWalletAddress").val(E_Wallet);
    $("#txtBTC").val(Points);
    $("#txtCreateDate").val(CreateDate);
    $("#txtStatus").val(Status);
    $("#myModalDetailTransactionPoint").show();
    $("#over").show();

}
function CloseModalTransactionPoint() {
    $("#myModalDetailTransactionPoint").hide();
    $("#over").hide();
}
﻿$(document).ready(function () {
    GetListAwardOrderByDate1();

    LoaddTransactionBookingOrderByDate();

    if (typeof $('#btnSubscribe') != 'undefined' && $('#btnSubscribe').length > 0) {
        $('#btnSubscribe').on('click', function () {
            sendSubscribe();
        })
    }
})
function AddMoreTicket() {
    var language = $('#hdLanguage').val();
    var textAuto = ''; var textClear = '';
    if (language == 'EN') {
        textAuto = "Auto";
        textClear = "Clear";
    }
    else {
        textAuto = "Ngẫu nhiên";
        textClear = "Làm mới";
    }
    var k = Math.floor(Math.random() * 99999) + 10000;
    var strTicket = '<div class="profile-ticket__item " id="divTicket_' + k + '">'
        + '<div class="box-ticket">'
        + '<div class="box-ticket__header"><div class="ticket-header__left">'
        + '<div class="ticket-header__num"></div>'
        + ' <a href="javascript:void(0)" onclick="SetAutoNumber(' + k + '); return false;" class="btn-empty"><span class="icon-auto"></span>' + textAuto + '</a></div>'
        + '<div class="ticket-header__right">'
        + '<a href="javascript:void(0)" onclick="ClearNumber(' + k + '); return false;" class="btn-empty ticket-header__btn-clear js-ticket-clear">' + textClear + '</a> <span class="btn-del__wrap"><a href="javascript:void(0)" onclick="RemoveFormTicket(' + k + '); return false;" class="btn-empty ticket-header__btn-del js-ticket-hidden"><span class="icon-del"></span></a></span></div></div>'
        + '<div class="box-ticket__body"><p>&nbsp;</p>'
        + '<ul class="num-list first-49" style="margin:0 auto" data-max="5" data-ready="true">'
        + '<li><input type="text" id="txtNumber_' + k + '" name="txtNumber_' + k + '" onkeyup="SetNumberTicket(event,' + k + ',this.value)" onkeypress="PreventInput(event,' + k + ')" style="border:1px solid #ccc; height:30px; padding:10px; text-align:center; font-size:18px; width:100%; box-sizing: inherit;" /></li>'
        + '</ul>'
        + '</div></div></div>';

    $('#divListTicket').append(strTicket);
    $('#txtNumber_' + k).numeric();
}

function ResetField(num) {
    $('#lbQuantity_' + num).text('');
}

function SetAutoNumber(num) {
    var numberTicket = '';
    for (var n = 0; n < 5; n++) {
        var number = Math.floor(Math.random() * 9);
        numberTicket += number;
    }
    $('#txtNumber_' + num).val(numberTicket);
    $('#txtNumber_' + num).attr("readonly", true);
    var strTicket = '<tr id="trTicket_' + num + '">'
        + '<td id="tdTicket_' + num + '" style="color: #000; width:65%; font-size: 13px; font-weight:bold">' + numberTicket + '</td>'
        + '<td style="color: #000; width:35%; font-size: 13px; font-weight:bold; text-align:center">1</td>'
        + '</tr>';
    if ($('#trTicket_' + num).length == 0) {
        $('#tbListTicket').append(strTicket);
    }
    else {
        $('#tdTicket_' + num).text(numberTicket);
    }
}

function ClearNumber(num) {
    $('#txtNumber_' + num).val('');
    $('#txtNumber_' + num).attr("readonly", false);
    $('#trTicket_' + num).remove();
}

function RemoveFormTicket(num) {
    $('#divTicket_' + num).remove();
    $('#trTicket_' + num).remove();
}

function PreventInput(e, num) {
    var lenght = $('#txtNumber_' + num).val().length;
    var numberTicket = $('#txtNumber_' + num).val();
    var strTicket = '<tr id="trTicket_' + num + '">'
        + '<td id="tdTicket_' + num + '" style="color: #000; width:65%; font-size: 13px; font-weight:bold">' + numberTicket + '</td>'
        + '<td style="color: #000; width:35%; font-size: 13px; font-weight:bold; text-align:center">1</td>'
        + '</tr>';
    if (lenght > 4) {
        e.preventDefault();
        if ($('#trTicket_' + num).length == 0) {
            $('#tbListTicket').append(strTicket);
        }
        else {
            $('#tdTicket_' + num).text(numberTicket);
        }
    }

}

function SetNumberTicket(event, num, numberTicket) {
    var strTicket = '<tr id="trTicket_' + num + '">'
        + '<td id="tdTicket_' + num + '" style="color: #000; width:65%; font-size: 13px; font-weight:bold">' + numberTicket + '</td>'
        + '<td style="color: #000; width:35%; font-size: 13px; font-weight:bold; text-align:center">1</td>'
        + '</tr>';
    if ($('#trTicket_' + num).length == 0) {
        $('#tbListTicket').append(strTicket);
    }
    else {
        $('#tdTicket_' + num).text(numberTicket);
    }
}

function GetListAwardOrderByDate1() {
    $.ajax({
        type: "post",
        url: "/Home/GetListAwardOrderByDate1",
        async: true,
        beforeSend: function () {
        },
        success: function (d) {
            if (d == '-1') {
                $("#ulLotteryResult").html('');
            }
            else {
                $("#ulLotteryResult").html(d.content);
                $("#spanLotteryDate").text(d.date);
            }
        },
        error: function () {
            $("#ulLotteryResult").html('');
        }
    });
}
function LoaddTransactionBookingOrderByDate() {
    $.ajax({
        type: "post",
        url: "/Home/LoaddTransactionBookingOrderByDate",
        async: true,
        beforeSend: function () {
        },
        success: function (d) {
            if (d == '-1') {
                $("#ulListTransactionBooking").html('');
            }
            else {
                $("#ulListTransactionBooking").html(d);
            }
        },
        error: function () {
            $("#ulListTransactionBooking").html('');
        }
    });
}
var sendSubscribe = function () {
    var strFullName = "";
    if (typeof $('#txtFullName') != 'undefined' && $('#txtFullName').length > 0) {
        strFullName = $('#txtFullName').val().trim();
    }
    var strEmail = "";
    if (typeof $('#txtEmail') != 'undefined' && $('#txtEmail').length > 0) {
        strEmail = $('#txtEmail').val().trim();
    }
    var strMessage = "";
    if (typeof $('#txtMessage') != 'undefined' && $('#txtMessage').length > 0) {
        strMessage = $('#txtMessage').val().trim();
    }
    var strMessageError = checkValidateChar(strFullName, "Your full name", 250, 2, "txtFullName").trim();
    strMessageError += checkValidateChar(strEmail, "Your email", 250, 1, "txtEmail").trim();
    strMessageError += checkValidateChar(strMessage, "Your message", 3500, 2, "txtMessage").trim();
    var recaptcha = $("#g-recaptcha-response").val();
    if (recaptcha === "") {
        strMessageError += "- Please check the <b>recaptcha</b>!";
        if (!Empty($('.g-recaptcha').children())) {
            $('.g-recaptcha').children().addClass('has-error');
        }
    }
    else {
        if (!Empty($('.g-recaptcha').children())) {
            $('.g-recaptcha').children().removeClass('has-error');
        }
    }

    if (strMessageError.trim().length > 0) {
        alertify.error(strMessageError.trim());
        checkMaxShowAlertNoti(maxItemNotification);
    }
    else {
        $.ajax({
            type: "POST",
            url: "http://mvcservices.bigbrothers.gold/insert-subscribe-mail/",
            data: {
                strSubscribeEmail: strEmail, strSubscribeName: strFullName, strSubscribeWebsite: window.location.href, intWebsiteID: 0, strSubscribeTitle: "Subscribe mail Wan2Lot", strSubscribeContent: strMessage.trim()
            },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                alertify.success('Your question is sent success!');
                checkMaxShowAlertNoti(maxItemNotification);
                if (typeof $('#txtFullName') != 'undefined' && $('#txtFullName').length > 0) {
                    $('#txtFullName').val('');
                }
                if (typeof $('#txtEmail') != 'undefined' && $('#txtEmail').length > 0) {
                    $('#txtEmail').val('');
                }
                if (typeof $('#txtMessage') != 'undefined' && $('#txtMessage').length > 0) {
                    $('#txtMessage').val('');
                }
                if (typeof grecaptcha !== 'undefined' && grecaptcha && grecaptcha.reset) {
                    grecaptcha.reset();
                }
            },
            error: function (msg) {
                alertify.error('Your question is sent failed! Please try again.');
                checkMaxShowAlertNoti(maxItemNotification);
            }
        });
    }
}
var checkValidateChar = function (strContent, strElementName, intMaxLength, intTypeFormat, strElementID) {
    var strMessage = "";
    if (strContent.trim().length == 0) {
        strMessage = "- Please enter <b>" + strElementName.trim() + "</b>!<br/>";
    }
    else if (strContent.trim().length > intMaxLength) {
        strMessage = "- Please enter <b>" + strElementName.trim() + "</b> less than " + intMaxLength + " character!<br/>";
    }
    else if (intTypeFormat == 1 && !isValidEmailAddress(strContent)) {
        strMessage = "- Please enter <b>" + strElementName + "</b> correct format e-mail again!<br/>";
    }
    else if (intTypeFormat == 2 && /<(br|basefont|hr|input|source|frame|param|area|meta|!--|col|link|option|base|img|wbr|!DOCTYPE).*?>|<(a|abbr|acronym|address|applet|article|aside|audio|b|bdi|bdo|big|blockquote|body|button|canvas|caption|center|cite|code|colgroup|command|datalist|dd|del|details|dfn|dialog|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frameset|head|header|hgroup|h1|h2|h3|h4|h5|h6|html|i|iframe|ins|kbd|keygen|label|legend|li|map|mark|menu|meter|nav|noframes|noscript|object|ol|optgroup|output|p|pre|progress|q|rp|rt|ruby|s|samp|script|section|select|small|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video).*?<\/\2>/i.test(strContent.trim())) {
        strMessage = "- Please enter <b>" + strElementName + "</b> not contains character html or script again!<br/>";
    }

    if (!Empty($('#' + strElementID))) {
        if (strMessage.trim().length > 0) {
            $('#' + strElementID).addClass('has-error-bottom');
        }
        else {
            $('#' + strElementID).removeClass('has-error-bottom');
        }
    }
    return strMessage;
}



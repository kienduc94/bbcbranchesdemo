﻿function ConfirmWithdrawAward(transactionID, status) {
    var textMessage = '';
    if (status == 1) {
        textMessage = 'Are you sure confirm this transaction?';
    }

    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: false,
            url: "/AwardWithdraw/UpdateStatusAwardWithdraw",
            data: { transactionID: transactionID, status: status },
            beforeSend: function () {
                $('#imgLoading_' + transactionID).css("display", "");
            },
            success: function (d) {
                $('#imgLoading_' + transactionID).css("display", "none");
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });

                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {

                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}

function SearchAwardWithdraw() {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    var page = 1;
    //if (keyword == '') {
    //    noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    //}

    $.ajax({
        type: "get",
        async: true,
        url: "/AwardWithdraw/SearchAwardWithdraw",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListAward').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearchAwardWithdraw(page) {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/AwardWithdraw/SearchAwardWithdraw",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListAward').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function ShowAwardWithdrawDetail(transactionID,status) {
    $.ajax({
        type: "get",
        async: true,
        url: "/AwardWithdraw/GetAwardWithdrawDetail",
        data: { transactionID: transactionID, status: status},
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#lbTransactionID').text(json.TransactionID);
                $('#lbUserName').text(json.UserName);
                //$('#lbAwardDate').text(json.AwardDate);
                $('#lbAwardValues').text(json.AwardValues);
                $('#lbAwardStatus').text(json.RequestStatus);
                $('#lbUserConfirm').text(json.ConfirmUser);
                $('#lbConfirmDate').text(json.ConfirmDate);
                $('#lbCreateDate').text(json.RequestDate);
                if (json.RequestStatus == 'UnProccess') {
                    $('#div_award').hide();
                }
                else {

                    $('#div_award').show();

                }
            }
        },
        error: function (e) {

        }
    });
}

function ExportExcelTrnasctionCoin() {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;
    var status = $('#cbStatus').val();
    if (toDate == '' && fromDate == '' && memberID == 0 && status == -1) {
        noty({ text: "Please choose keyword search", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/AwardWithdraw/ExportListAwardWithdraw",
            data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                window.location.href = '/FileExcels/TransactionCoin/' + d;
            },
            error: function (e) {

            }
        });
    }
}



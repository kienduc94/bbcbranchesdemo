﻿function LockAndUnlockGroupAdmin(groupID, status) {
    var confirmMessage = confirm('Are you sure lock this function?');
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/GroupAdmin/LockAndUnlockGroupAdmin",
            data: { groupID: groupID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Failes", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {
            }
        });
    }
}

function ShowPopUpEditGroupAdmin(groupID, groupName) {
    $('#hdGroupID').val(groupID);
    $('#txtGroupName').val(groupName);
}

function ShowPopupCreateNewGroupAdmin() {
    $('#hdGroupID').val(0);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}

function SaveGroupAdmin() {
    var checkReg = true;
    var groupID = $('#hdGroupID').val();
    var groupName = $('#txtGroupName').val();
 
    if (groupName == '') {
        $('#lbErrorGroupName').text('Function name is required');
        $('#lbErrorGroupName').css('display', '');
        checkReg = false;
    }
   
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/GroupAdmin/SaveGroupAdmin",
            data: { groupID: groupID, groupName: groupName },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if(d=='failes'){
                    noty({ text: "Update Failes", layout: "bottomRight", type: "error" });
                }               
                else if (d == 'GroupNameNull') {
                    $('#lbErrorGroupName').css("display", "");
                    $('#lbErrorGroupName').text('Function name is required');
                    window.scrollTo(100, 100);
                }
                else if (d == 'GroupNameExist') {
                    $('#lbErrorGroupName').css("display", "");
                    $('#lbErrorGroupName').text('GroupName Exit!!');
                    window.scrollTo(100, 100);
                }
            },
            error: function (e) {
            }
        });
    }
}
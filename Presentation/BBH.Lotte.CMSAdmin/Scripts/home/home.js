﻿function ConfirmBooking(bookingID, status, memberID, points) {
    
    var textMessage = '';
    if (status == 1) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    else if (status == 2) {
        textMessage = 'Are you sure lock this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $('#imgLoading_' + bookingID).css("display", "");
        $.ajax({
            type: "post",
            async: false,
            url: "/Booking/UpdateStatusBooking",
            data: { bookingID: bookingID, status: status, memberID: memberID, points: points },
            beforeSend: function () {
                $('#imgLoading_' + bookingID).css("display", "");
            },
            success: function (d) {
                $('#imgLoading_' + bookingID).css("display", "none");
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "top", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}
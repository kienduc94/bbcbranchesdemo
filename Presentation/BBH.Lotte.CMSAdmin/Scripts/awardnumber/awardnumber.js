﻿function LockAndUnlockAwardNumber(numberID, status) {
    var confirmMessage = confirm('Are you sure lock this?');
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/AwardResult/LockAndUnLockAwardNumber",
            data: { numberID: numberID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    window.location.reload();
                }
            },
            error: function (e) {
            }
        });
    }
}

function PreventInput(event, value) {
    var lenght = $('#txtNumberValue').val().length;
    if (lenght > 16) {
        event.preventDefault();
    }
}

function ShowPopUpEditAwardNumber(numberID, awardID, numberValue, stationNameEnglish) {
    $('#hdNumberID').val(numberID);
    $('#txtNumberValue').val(numberValue);
    //$('#txtStationName').val(stationName);
    //$('#txtStationNameEnglish').val(stationNameEnglish);
    $('#cbAward option[value=' + awardID + ']').prop("selected", true);
    $('#cbAward').trigger('chosen:updated'); 
}

function ShowPopupCreateNewAwardNumber() {
    $('#hdNumberID').val(0);
    $('#txtNumberValue').val('');
    //$('#txtStationName').val('');
    //$('#txtStationNameEnglish').val('');
    $('#cbAward option[value=0]').prop("selected", true);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}

function SaveAwardNumber() {
    var checkReg = true;
    var numberID = $('#hdNumberID').val();
    var numberValue = $('#txtNumberValue').val();
    var awardID = $('#cbAward').val();
    //var stationName = $('#txtStationName').val();
    //var stationNameEnglish = $('#txtStationNameEnglish').val();
    if (awardID == '0') {
        $('#lbErrorAward').text('Please select award');
        $('#lbErrorAward').css("display", "");
        checkReg = false;
    }
    if (numberValue == '' ) {
        $('#lbErrorNumberValue').text('Number Winner is required');
        $('#lbErrorNumberValue').css("display", "");
        checkReg = false;
    }
    if (numberValue.length > 17) {
        $('#lbErrorNumberValue').text('Max length is 17');
        $('#lbErrorNumberValue').css("display", "");
        checkReg = false;
    }
    //if (isNaN(numberValue)) {
    //    $('#lbErrorNumberValue').text('Numeric is required');
    //    $('#lbErrorNumberValue').css("display", "");
    //    checkReg = false;
    //}
    //if (stationNameEnglish == '' ) {
    //    $('#lbErrorStationEnglish').text('Station Name is required');
    //    $('#lbErrorStationEnglish').css("display", "");
    //    checkReg = false;
    //}
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/AwardResult/SaveAwardNumber",
            data: { numberID: numberID, awardID: awardID, numberValue: numberValue },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'isNumberError')
                {
                    $('#lbErrorNumberValue').text('Number Winner is incorect');
                    $('#lbErrorNumberValue').css("display", "");
                }
            },
            error: function (e) {

            }
        });
    }
}

function SearchAward() {
    var searchDay = $('#txtDatetime').val();
    if (searchDay == '') {
        alertify.error('Please select date search');
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/AwardMegaballResult/SearchAwardByDate",
            data: { searchDay: searchDay },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                $('#spanAwardDate').text(searchDay);
                $('#tbodyAwardNumber').html(d);
            },
            error: function (e) {

            }
        });

        $.ajax({
            type: "get",
            async: true,
            url: "/AwardMegaballResult/SearchWinnerAward",
            data: { searchDay: searchDay },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                $('#spanWinnerDate').text(searchDay);
                $('#tbodyWinnerAward').html(d);
            },
            error: function (e) {

            }
        });
    }
}

function ExportExcelAwardWinner()
{
    var searchDay = $('#txtDatetime').val();
    if (searchDay == '') {
        alertify.error('Please select date search');
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/AwardMegaballResult/ExportListWinnerAward",
            data: { searchDay: searchDay },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                $('#spanWinnerDate').text(searchDay);
                window.location.href = '/FileExcels/AwardWinner/' + d;
            },
            error: function (e) {

            }
        });
    }
}
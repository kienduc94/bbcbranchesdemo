﻿function LockAndUnlockConfig(configID, isAutoLottery) {
    var confirmMessage = '';
    if (isAutoLottery == 0) {
        confirmMessage = confirm('Are you sure turn on automatic AwardMegaball?');
    }
    if (isAutoLottery == 1) {
        confirmMessage = confirm('Are you sure turn off automatic AwardMegaball?');
    }
    if (!confirmMessage) {
        return false;
    }
    else {

        $.ajax({
            type: "post",
            async: true,
            url: "/SystemConfig/UpdateStatusActive",
            data: { configID: configID, isAutoLottery: isAutoLottery },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                } else {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}
 
function ShowPopUpEditTicketConfig(configID, isAutoLottery) {
    $('#cbConfigSystem').val(configID);
    $('#txtConfigName').val(coinfigName);
  

    
}

function ShowPopupCreateNewConfigTicket() {
    $('#cbConfigSystem').val(0);
    //$('#txtAwardName').val(awardName);
    $('#txtConfigName').val('');
    $('#txtNumberTicket').val('');
    $('#txtCoinvalues').val('');
    $('#txtCoinID').val('');
    //$('#txtAwardName').attr("readonly", false);
    //$('#txtAwardNameEnglish').attr("readonly", false);
}

 
 
function SaveConfigTicket() {
    var checkReg = true;
    var configID = $('#hdSystemfigID').val();  
    var isAuto = 0;
    if ($('#cbConfigSystem').is(':checked')) {
        isAuto = 1;
    }
        $.ajax({
            type: "post",
            async: true,
            url: "/TicketConfig/SaveTicketConfig",
            data: { configID: configID, isAuto: isAuto},
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'failes') {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
                else if (d == 'CONFIGTICKETEXITS') {
                    $('#lbErrorCoinID').text('CoinID Exit');
                    $('#lbErrorCoinID').css("display", "");
                    window.scrollTo(100, 100);
                }


            },
            error: function (e) {

            }
        });
}
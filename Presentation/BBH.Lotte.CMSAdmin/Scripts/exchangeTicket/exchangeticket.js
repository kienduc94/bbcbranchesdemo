﻿function LockAndUnlockExchangeTicket(exchangeID, status) {
    var confirmMessage = confirm('Are you sure lock this exchange ticket?');
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/ExchangeTicket/LockAndUnlockExchangeTicket",
            data: { exchangeID: exchangeID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {
            }
        });
    }
}

function ConfirmExchangeTicket(exchangeID, status,deleteDate,deleteUser) {
    var textMessage = '';
    if (status == 0) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    else if (status == 1) {
        textMessage = 'Are you sure lock this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: false,
            url: "/ExchangeTicket/UpdateStatusExchangeTicket",
            data: { exchangeID: exchangeID, status: status, deleteDate: deleteDate, deleteUser: deleteUser },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}
function OnchangePoint() {
    var valuepoint = 1;
    var ticket = 20000;
    var value = $("#txtPointValue").val(valuepoint);    

    //var ticketnumber = $("#txtPointValue").attr("data-ticketnumber");
    //var tickerpoint = $("#txtPointValue").attr("data-tickerpoint");
    var ticketExchange = 0;
    ticketExchange = value * ticket;
    $("#spanTicket").text(ticketExchange);


    //recharge_qr = $("#recharge_qr").attr('src');
   
}

//$(document).ready(function () {
//    var adminID = $('#hdAdminID').val();
//    if (adminID > 0) {
//        $('#txtUserName').attr("readonly", true);
//        $('#txtPass').attr("readonly", true);
//    }
//    else {
//        $('#cbCoin option[value=' + coinID + ']').attr("readonly", false);

//    }
//});

function ShowPopupCreateNewExchangeTicket() {
    $('#hdExchangeTicketID').val(0);
    $('#txtPointValue').val('');
    $('#txtTicketNumber').val('');  
    $('#cbCoin option[value=0]').attr("selected", true);
    //$('#cbCoin').attr("readonly", false);
    //$('#cbCoin option[value=' + coinID + ']').attr("readonly", false);
}

function ShowPopUpEditExchangeTicket(exchangeID,coinID, pointValue, ticketNumber) {
    $('#hdExchangeTicketID').val(exchangeID);
    $('#txtPointValue').val(pointValue);
    $('#txtTicketNumber').val(ticketNumber);
    $('#cbCoin option[value="' + coinID + '"]').attr("selected", true);
    $('#cbCoin').trigger('chosen:updated');  
   
}
function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}

function onlyNumbers(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function SaveExchangeTicket() {
    var checkReg = true;
    var exchangeID = $('#hdExchangeTicketID').val();
    var pointValue = $('#txtPointValue').val();
    var ticketNumber = $('#txtTicketNumber').val();
    var createUser = $('txtCreateUser').val();
    var coinID = $('#cbCoin').val();
   
    if (pointValue == '') {
        $('#lbErrorPointValue').text('PointValue is required');
        $('#lbErrorPointValue').css('display', '');
        checkReg = false;
    }   
    if (ticketNumber == '') {
        $('#lbErrorTicketNumber').text('TicketNumber is required');
        $('#lbErrorTicketNumber').css('display', '');
        checkReg = false;
    }
    if (coinID == 0) 
    {
        $('#lbErrorCoinID').text('Choose coin name');
        $('#lbErrorCoinID').css('display', '');
        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {       
        $.ajax({
            type: "post",
            async: true,
            url: "/ExchangeTicket/SaveExchangeTicket",
            data: { exchangeID: exchangeID, createUser: createUser, pointValue: pointValue, ticketNumber: ticketNumber, coinID: coinID },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'failes')
                {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
                else if (d == 'CoinIDExist')
                {
                    $('#lbErrorCoinID').text('CoinID exist');
                    $('#lbErrorCoinID').css('display', '');
                }
            },
            error: function (e) {
                noty({ text: "Update Error, Please try again", layout: "bottomRight", type: "error" });
            }
        });
    }
}

function SearchExchangeTicket() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;

    $.ajax({
        type: "get",
        async: true,
        url: "/ExchangeTicket/SearchExchangeTicket",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListExtrangeTicket').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearchExchangeTicket(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
  
    $.ajax({
        type: "get",
        async: true,
        url: "/ExchangeTicket/SearchExchangeTicket",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListExtrangeTicket').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}











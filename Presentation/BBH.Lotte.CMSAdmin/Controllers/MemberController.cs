﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.Repository;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using Microsoft.Practices.Unity;
using BBH.Lotte.Domain.Interfaces;
using BBH.Lotte.Domain;
using BBH.Lotte.CMSAdmin.Models;
using Newtonsoft.Json;
using BBH.Lotte.CLP.CMSAdmin.Models;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class MemberController : Controller
    {
        //
        // GET: /Member/
        // MemberRepository repository = new MemberRepository();
        [Dependency]
        protected IMemberServices repository { get; set; }

        [Dependency]
        protected ICompanyInformationServices repositoryCompany { get; set; }

        int groupIDAdmin = 0;

        [Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 10;
            //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

            int page = 1;
            try
            {
                if (p != null && p != string.Empty)
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            if (page > 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<MemberBO> lstMember = repository.GetListMember(start, end);
            ViewData[ViewDataKey.VIEWDATA_LISTMEMBER] = lstMember;

            if (lstMember != null && lstMember.Count() > 0)
            {
                totalRecord = lstMember.ElementAt(0).TotalRecord;
            }
            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;

            return View();
        }
        [Authorization]
        [HttpPost]
        public string LockAndUnLockMember(int memberID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            string result = string.Empty;
            int statusNew = -1;
            if (status == 1)
            {
                statusNew = 0;
            }
            else
            {
                statusNew = 1;
            }
            bool rs = repository.LockAndUnlockMember(memberID, statusNew);
            if (rs)
            {
                result = ResultKey.RESULT_SUCCESS;
            }
            return result;
        }
        [Authorization]
        [HttpGet]
        public string SearchMember(string keyword, string fromDate, string toDate, int page)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            DateTime? fromD = null;
            DateTime? toD = null;

            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();

            int totalRecord = 0;
            int intPageSize = 10;
            int start = 1, end = 10;

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                fromD = DateTime.Parse("01/01/1990 12:01:01");
                toD = DateTime.Now;
            }
            else if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
            }
            else
            {
                if (string.IsNullOrEmpty(fromDate))
                {
                    if (string.IsNullOrEmpty(toDate))
                    {
                        //fromD = DateTime.Parse("");
                        toD = DateTime.Parse("01/01/1990");
                    }
                    else
                    {
                        fromD = DateTime.Parse("01/01/1990");
                        toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                    }
                }
                else
                {
                    fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = DateTime.Now;
                }
            }
            if (page >= 1)
            {
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);
            }
            IEnumerable<MemberBO> lstMember = repository.GetListMemberBySearch(keyword, fromD, toD, start, end);
            if (lstMember != null && lstMember.Count() > 0)
            {
                totalRecord = lstMember.ElementAt(0).TotalRecord;
                foreach (MemberBO member in lstMember)
                {
                    string titleStatus = "Lock";
                    string status = "Active";
                    if (member.IsActive == 0)
                    {
                        status = "InActive";
                        titleStatus = "Unlock";
                    }

                    builder.Append("<tr id=\"trGroup_" + member.MemberID + "\" class=\"none-top-border\">");
                    //builder.Append("<td><a onclick =\"ShowPopupDetailCompany('"+member.MemberID+"')\" data - toggle = \"modal\" data - target =\"standardModal\" >" + member.MemberID + "</a></td>");
                    builder.Append("<td>" + member.MemberID + "</td>");
                    builder.Append("<td>" + member.Email + "</td>");
                    //builder.Append("<td>" + member.FullName + "</td>");
                    builder.Append("<td>" + member.CreateDate.ToString("MM/dd/yyyy HH:mm:ss") + "</td>");
                    builder.Append("<td>" + string.Format("{0:0,0}", member.Points) + "</td>");
                    builder.Append("<td><span class=\"label-success label label-default\" title=\"'" + titleStatus + "'\">" + status + "</span></td>");
                    builder.Append("<td>");

                    if (member.IsActive == 0)
                    {
                        builder.Append("<a class=\"btn btn-info btn-sm\" href=\"javascript:void(0)\" onclick=\"LockAndUnlockMember('" + member.MemberID + "','" + member.IsActive + "')\"><i class=\"glyphicon glyphicon-edit icon-white\"></i>" + titleStatus + "</a>");
                    }
                    else if (member.IsActive == 1)
                    {
                        builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"LockAndUnlockMember('" + member.MemberID + "','" + member.IsActive + "')\"><i class=\"glyphicon glyphicon-trash icon-white\"></i>" + titleStatus + "</a>");
                    }
                    builder.Append("</a>");
                    builder.Append("</td>");
                    builder.Append("</tr>");
                }
                
                string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
                firstLink = "?p=1";
                if (page <= 1)
                {
                    previewLink = "?p=1";
                }
                else
                {
                    previewLink = "?p=" + (page - 1).ToString();
                }

                builderPaging.Append("<nav class=\"text-center\">");
                builderPaging.Append("<ul id=\"ulPaging\" class=\"pagination\">");

                int pagerank = 5;
                int next = 10;
                int prev = 1;


                if (totalRecord!=0)
                {
                    int totalRecore = totalRecord;
                    int totalPage = totalRecore / intPageSize;
                    int balance = totalRecore % intPageSize;
                    if (balance != 0)
                    {
                        totalPage += 1;
                    }
                    if (page >= totalPage)
                    {
                        nextLink = "?p=" + totalPage.ToString();
                    }
                    else
                    {
                        nextLink = "?p=" + (page + 1).ToString();
                    }
                    int currentPage = page;
                    var m = 1;
                    if (totalPage > 10)
                    {
                        if (page > pagerank + 1)
                        {
                            next = (page + pagerank) - 1;
                            m = page - pagerank;
                        }
                        if (page <= pagerank)
                        {
                            prev = (page - pagerank);
                            m = 1;
                        }
                        if (next > totalPage)
                        {
                            next = totalPage;
                        }
                        if (prev < 1)
                        {
                            prev = 1;
                        }
                    }
                    else
                    {
                        next = totalPage;
                        prev = 1;
                    }
                    lastLink = "?p=" + totalPage;
                    //if (totalPage > 1)
                    //{
                    //    if (currentPage > 1)
                    //    {

                    //        builderPaging.Append("<li class=\"page-item\">");
                    //        builderPaging.Append("<a href=\"" + previewLink + "\" class=\"page-link\" aria-label=\"Previous\">Prev</a>");
                            
                    //        builderPaging.Append(" </li>");
                    //    }
                    //}

                    if (totalPage > 1)
                    { 

                        for (; m <= next; m++)
                        {
                            linkPage = "?p=" + m;
                            if (m == currentPage)
                            {
                                builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                            }
                            else
                            {
                                builderPaging.Append("<li class=\"page-item\"><a href=\"javascript:void(0)\" onclick=\"PagingSearchMember('" + m + "')\" class=\"page-link\">" + m + "</a></li>");
                            }
                        }
                    }

                    //if (totalPage > 1)
                    //{
                    //    if (currentPage < totalPage)
                    //    {
                    //        builderPaging.Append("<li class=\"page-item\">");
                    //        builderPaging.Append("<a href=\"" + nextLink + "\" class=\"page-link\" aria-label=\"Next\">Next</a>");
                           
                    //        builderPaging.Append("</li>");
                    //    }
                    //}
                }


                builderPaging.Append("</ul>");
                builderPaging.Append("</nav>");
            }
            else
            {
                builder.Append("<tr><td colspan=\"7\">No result found</td></tr>");
            }

            SearchObj obj = new SearchObj();
            obj.ContentResult = builder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            string json = JsonConvert.SerializeObject(obj);

            return json;
        }
        [Authorization]
        [HttpGet]
        public string ExportListMember(string keyword, string fromDate, string toDate)
        {
            List<MemberBO> lstTotalMember = new List<MemberBO>();

            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            HSSFWorkbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("LIST MEMBER");
            Row rowLogo = sheet.CreateRow(0);

            rowLogo.CreateCell(0).SetCellValue("LIST MEMBER - LOTTERY");
            rowLogo.HeightInPoints = 20;
            CellStyle styleLogo = workbook.CreateCellStyle();
            styleLogo.Alignment = HorizontalAlignment.CENTER;
            NPOI.SS.UserModel.Font font = workbook.CreateFont();
            font.FontHeightInPoints = 12;
            font.FontName = "Times New Roman";
            font.Boldweight = (short)FontBoldWeight.BOLD;

            styleLogo.SetFont(font);
            rowLogo.GetCell(0).CellStyle = styleLogo;

            sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

            sheet.SetColumnWidth(0, 5 * 256);
            sheet.SetColumnWidth(1, 25 * 256);
            sheet.SetColumnWidth(2, 25 * 256);
            sheet.SetColumnWidth(3, 25 * 256);
            sheet.SetColumnWidth(4, 15 * 256);

            Row titleList = sheet.CreateRow(2);

            titleList.CreateCell(0).SetCellValue("STT");
            titleList.HeightInPoints = 22;
            CellStyle styleTitleList = workbook.CreateCellStyle();
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            NPOI.SS.UserModel.Font fontTitleList = workbook.CreateFont();
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            fontTitleList.Color = (short)NPOI.HSSF.Util.HSSFColor.WHITE.index;
            //NPOI.HSSF.UserModel.HSSFTextbox.LINESTYLE_SOLID
            styleTitleList.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
            // styleTitleList.FillPattern = FillPatternType.THICK_VERT_BANDS;
            styleTitleList.FillPattern = FillPatternType.THIN_FORWARD_DIAG;
            styleTitleList.FillPattern = FillPatternType.THIN_BACKWARD_DIAG;
            styleTitleList.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;

            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(0).CellStyle = styleTitleList;

            titleList.CreateCell(1).SetCellValue("MemberID");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(1).CellStyle = styleTitleList;

            titleList.CreateCell(2).SetCellValue("Email");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(2).CellStyle = styleTitleList;

            //styleTitleList.SetFont(fontTitleList);
            //titleList.GetCell(1).CellStyle = styleTitleList;
            titleList.CreateCell(3).SetCellValue("	CreateDate");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.CENTER;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(3).CellStyle = styleTitleList;

            titleList.CreateCell(4).SetCellValue("Points");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(4).CellStyle = styleTitleList;

            titleList.CreateCell(5).SetCellValue("Status");
            titleList.HeightInPoints = 22;
            styleTitleList.Alignment = HorizontalAlignment.LEFT;
            styleTitleList.FillBackgroundColor = 4;
            fontTitleList.FontHeightInPoints = 12;
            fontTitleList.FontName = "Times New Roman";
            fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
            styleTitleList.SetFont(fontTitleList);
            titleList.GetCell(5).CellStyle = styleTitleList;


            DateTime? fromD = null;
            DateTime? toD = null;
            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                fromD = DateTime.Parse("01/01/1990 12:01:01");
                toD = DateTime.Now;
            }
            else if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
            {
                fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
            }
            else
            {
                if (string.IsNullOrEmpty(fromDate))
                {
                    if (string.IsNullOrEmpty(toDate))
                    {
                        toD = DateTime.Parse("01/01/1990");
                    }
                    else
                    {
                        fromD = DateTime.Parse("01/01/1990");
                        toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                    }
                }
                else
                {
                    fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = DateTime.Now;
                }
            }

            if (keyword == string.Empty && fromDate == string.Empty && toDate == string.Empty)
            {
                int rowNext = 3;
                int number = 1;
                int intPageSize = 500;
                int start = 0, end = 500;
                int totalRecord = 0;
                int pageSize = 1;
                //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                IEnumerable<MemberBO> lstMember = repository.GetListMember(start, end);
                if (lstMember != null && lstMember.Count() > 0)
                {
                    foreach (MemberBO obj in lstMember)
                    {
                        lstTotalMember.Add(obj);
                    }
                    totalRecord = lstMember.ElementAt(0).TotalRecord;
                    pageSize = totalRecord / intPageSize;
                    int balance = totalRecord % intPageSize;
                    if (balance != 0)
                    {
                        pageSize += 1;
                    }
                    if (pageSize > 1)
                    {
                        for (int i = 2; i <= pageSize; i++)
                        {
                            start = (i - 1) * intPageSize + 1;
                            end = (i * intPageSize);
                            IEnumerable<MemberBO> lstMemberItem = repository.GetListMember(start, end);
                            if (lstMemberItem != null && lstMemberItem.Count() > 0)
                            {
                                foreach (MemberBO obj in lstMemberItem)
                                {
                                    lstTotalMember.Add(obj);
                                }
                            }
                        }
                    }
                }

                if (lstTotalMember != null && lstTotalMember.Count() > 0)
                {
                    NPOI.SS.UserModel.Font fontNumber = workbook.CreateFont();
                    fontNumber.FontHeightInPoints = 12;
                    fontNumber.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontDate = workbook.CreateFont();
                    fontDate.FontHeightInPoints = 12;
                    fontDate.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontMemberID = workbook.CreateFont();
                    fontMemberID.FontHeightInPoints = 12;
                    fontMemberID.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontOrderID = workbook.CreateFont();
                    fontOrderID.FontHeightInPoints = 12;
                    fontOrderID.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontPoint = workbook.CreateFont();
                    fontPoint.FontHeightInPoints = 12;
                    fontPoint.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontEmail3 = workbook.CreateFont();
                    fontEmail3.FontHeightInPoints = 12;
                    fontEmail3.FontName = "Times New Roman";


                    // totalRecord = lstMember.ElementAt(0).TotalRecord;
                    foreach (MemberBO member in lstTotalMember)
                    {
                        string titleStatus = "Lock";
                        string status = "Active";
                        if (member.IsActive == 0)
                        {
                            status = "InActive";
                            titleStatus = "Unlock";
                        }
                        Row rowListInvoice = sheet.CreateRow(rowNext);

                        rowListInvoice.CreateCell(0).SetCellValue(number);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleNumber = workbook.CreateCellStyle();
                        styleNumber.Alignment = HorizontalAlignment.LEFT;

                        styleNumber.SetFont(fontNumber);
                        rowListInvoice.GetCell(0).CellStyle = styleNumber;

                        rowListInvoice.CreateCell(1).SetCellValue(member.MemberID);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleDate = workbook.CreateCellStyle();
                        styleDate.Alignment = HorizontalAlignment.LEFT;

                        styleDate.SetFont(fontDate);
                        rowListInvoice.GetCell(1).CellStyle = styleDate;

                        rowListInvoice.CreateCell(2).SetCellValue(member.Email);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleMemberID = workbook.CreateCellStyle();
                        styleMemberID.Alignment = HorizontalAlignment.LEFT;

                        styleMemberID.SetFont(fontMemberID);
                        rowListInvoice.GetCell(2).CellStyle = styleMemberID;

                        rowListInvoice.CreateCell(3).SetCellValue(member.CreateDate.ToString("MM/dd/yyyy HH:mm:ss"));
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleOrderID = workbook.CreateCellStyle();
                        styleOrderID.Alignment = HorizontalAlignment.CENTER;

                        styleOrderID.SetFont(fontOrderID);
                        rowListInvoice.GetCell(3).CellStyle = styleOrderID;

                        //Row rowOrderID = sheet.CreateRow(rowNext);
                        rowListInvoice.CreateCell(4).SetCellValue(string.Format("{0:0,0}", member.Points));
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleOrder = workbook.CreateCellStyle();
                        styleOrder.Alignment = HorizontalAlignment.LEFT;

                        styleOrder.SetFont(fontOrderID);
                        rowListInvoice.GetCell(4).CellStyle = styleOrder;

                        rowListInvoice.CreateCell(5).SetCellValue(status);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleEmail3 = workbook.CreateCellStyle();
                        styleEmail3.Alignment = HorizontalAlignment.LEFT;


                        styleEmail3.SetFont(fontEmail3);
                        rowListInvoice.GetCell(5).CellStyle = styleEmail3;

                        number++;
                        rowNext++;
                    }
                }
            }
            else
            {
                int rowNext = 3;
                int number = 1;
                int intPageSize = 500;
                int start = 0, end = 500;
                int totalRecord = 0;
                int pageSize = 1;
                // Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                IEnumerable<MemberBO> lstMember = repository.GetListMemberBySearch(keyword, fromD, toD, start, end);
                if (lstMember != null && lstMember.Count() > 0)
                {

                    foreach (MemberBO obj in lstMember)
                    {
                        lstTotalMember.Add(obj);
                    }
                    totalRecord = lstMember.ElementAt(0).TotalRecord;
                    pageSize = totalRecord / intPageSize;
                    int balance = totalRecord % intPageSize;
                    if (balance != 0)
                    {
                        pageSize += 1;
                    }
                    if (pageSize > 1)
                    {
                        for (int i = 2; i <= pageSize; i++)
                        {
                            start = (i - 1) * intPageSize + 1;
                            end = (i * intPageSize);
                            IEnumerable<MemberBO> lstMemberItem = repository.GetListMemberBySearch(keyword, fromD, toD, start, end);
                            if (lstMemberItem != null && lstMemberItem.Count() > 0)
                            {
                                foreach (MemberBO obj in lstMemberItem)
                                {
                                    lstTotalMember.Add(obj);
                                }
                            }
                        }
                    }
                }
                if (lstTotalMember != null && lstTotalMember.Count() > 0)
                {
                    NPOI.SS.UserModel.Font fontNumber = workbook.CreateFont();
                    fontNumber.FontHeightInPoints = 12;
                    fontNumber.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontMemberID = workbook.CreateFont();
                    fontMemberID.FontHeightInPoints = 12;
                    fontMemberID.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontDate = workbook.CreateFont();
                    fontDate.FontHeightInPoints = 12;
                    fontDate.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontOrderID = workbook.CreateFont();
                    fontOrderID.FontHeightInPoints = 12;
                    fontOrderID.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontPoint = workbook.CreateFont();
                    fontPoint.FontHeightInPoints = 12;
                    fontPoint.FontName = "Times New Roman";

                    NPOI.SS.UserModel.Font fontEmail3 = workbook.CreateFont();
                    fontEmail3.FontHeightInPoints = 12;
                    fontEmail3.FontName = "Times New Roman";
                    // totalRecord = lstMember.ElementAt(0).TotalRecord;
                    foreach (MemberBO member in lstTotalMember)
                    {
                        string titleStatus = "Lock";
                        string status = "Active";
                        if (member.IsActive == 0)
                        {
                            status = "InActive";
                            titleStatus = "Unlock";
                        }
                        Row rowListInvoice = sheet.CreateRow(rowNext);

                        rowListInvoice.CreateCell(0).SetCellValue(number);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleNumber = workbook.CreateCellStyle();
                        styleNumber.Alignment = HorizontalAlignment.LEFT;

                        styleNumber.SetFont(fontNumber);
                        rowListInvoice.GetCell(0).CellStyle = styleNumber;

                        rowListInvoice.CreateCell(1).SetCellValue(member.MemberID);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleMemberID = workbook.CreateCellStyle();
                        styleMemberID.Alignment = HorizontalAlignment.LEFT;

                        styleMemberID.SetFont(fontMemberID);
                        rowListInvoice.GetCell(1).CellStyle = styleMemberID;

                        rowListInvoice.CreateCell(2).SetCellValue(member.Email);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleDate = workbook.CreateCellStyle();
                        styleDate.Alignment = HorizontalAlignment.LEFT;

                        styleDate.SetFont(fontDate);
                        rowListInvoice.GetCell(2).CellStyle = styleDate;

                        rowListInvoice.CreateCell(3).SetCellValue(member.CreateDate.ToString("MM/dd/yyyy HH:mm:ss"));
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleOrderID = workbook.CreateCellStyle();
                        styleOrderID.Alignment = HorizontalAlignment.CENTER;

                        styleOrderID.SetFont(fontOrderID);
                        rowListInvoice.GetCell(3).CellStyle = styleOrderID;

                        //Row rowOrderID = sheet.CreateRow(rowNext);
                        rowListInvoice.CreateCell(4).SetCellValue(string.Format("{0:0,0}", member.Points));
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleOrder = workbook.CreateCellStyle();
                        styleOrder.Alignment = HorizontalAlignment.LEFT;

                        styleOrder.SetFont(fontOrderID);
                        rowListInvoice.GetCell(4).CellStyle = styleOrder;

                        rowListInvoice.CreateCell(5).SetCellValue(status);
                        rowListInvoice.HeightInPoints = 20;
                        CellStyle styleEmail3 = workbook.CreateCellStyle();
                        styleEmail3.Alignment = HorizontalAlignment.LEFT;

                        styleEmail3.SetFont(fontEmail3);
                        rowListInvoice.GetCell(5).CellStyle = styleEmail3;

                        number++;
                        rowNext++;
                    }

                }
            }
            string fileNameExcel = "Listmember_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".xls";
            string filename = Server.MapPath("~/FileExcels/Member/" + fileNameExcel);
            using (var fileData = new FileStream(filename, FileMode.Create))
            {
                workbook.Write(fileData);
            }
            return fileNameExcel;
        }

    }
}

﻿using BBC.Core.WebService;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Repository
{
    public class FreeTicketRepository : WCFClient<IFreeTicketServices>, IFreeTicketServices
    {
        public FreeTicketBO GetFreeTicketActive()
        {
            try
            {
                return Proxy.GetFreeTicketActive();
            }
            catch
            {
                return null;
            }
        }
    }
}

﻿
using BBC.Core.WebService;

using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.LotteFE.Repository
{
    public class BookingRepository : WCFClient<IBookingServices>, IBookingServices
    {
        
        public bool InsertBooking(BookingBO objBookingBO)
        {
            try
            {
                return Proxy.InsertBooking(objBookingBO);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public IEnumerable<BookingBO> ListTransactionBookingByMember(int memberID, int start, int end)
        {
            try
            {
                return Proxy.ListTransactionBookingByMember(memberID, start, end);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<BookingBO> ListTransactionBookingBySearch(int memberID, System.DateTime? fromDate, System.DateTime? toDate, int start, int end)
        {
            try
            {
                return Proxy.ListTransactionBookingBySearch(memberID, fromDate, toDate, start, end);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int CountMemberBuyNumber(int memberID, string numberValue, DateTime fromDate, DateTime toDate)
        {
            return Proxy.CountMemberBuyNumber(memberID, numberValue, fromDate, toDate);

        }

        public bool UpdateStatusBooking(int bookingID, int status)
        {
            return Proxy.UpdateStatusBooking(bookingID, status);
        }

      public IEnumerable<BookingBO> ListTransactionBookingByDate(DateTime fromDate, DateTime toDate)
        {
            return Proxy.ListTransactionBookingByDate(fromDate, toDate);
        }

    }
}

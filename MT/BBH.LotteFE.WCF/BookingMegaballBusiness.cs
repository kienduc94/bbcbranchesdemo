﻿using BBC.Core.Database;

using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Data
{
    public class BookingMegaballBusiness : IBookingMegaballServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public bool InsertBooking(BookingMegaball booking)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertBooking_FE";
                SqlParameter[] pa = new SqlParameter[15];
                pa[0] = new SqlParameter("@memberID", booking.MemberID);
                pa[1] = new SqlParameter("@note", booking.Note);
                pa[2] = new SqlParameter("@quantity", booking.Quantity);
                pa[3] = new SqlParameter("@createDate", booking.CreateDate);
                pa[4] = new SqlParameter("@openDate", booking.OpenDate);
                pa[5] = new SqlParameter("@status", booking.Status);
                pa[6] = new SqlParameter("@transactionCode", booking.TransactionCode);
                pa[7] = new SqlParameter("@noteEnglish", booking.NoteEnglish);
                pa[8] = new SqlParameter("@firstNumber", booking.FirstNumber);
                pa[9] = new SqlParameter("@secondNumber", booking.SecondNumber);
                pa[10] = new SqlParameter("@thirdNumber", booking.ThirdNumber);
                pa[11] = new SqlParameter("@fourthNumber", booking.FourthNumber);
                pa[12] = new SqlParameter("@fivethNumber", booking.FivethNumber);
                pa[13] = new SqlParameter("@extraNumber", booking.ExtraNumber);
                pa[14] = new SqlParameter("@ValuePoint", booking.ValuePoint);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        //select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,m.*,mb.UserName,p.ProvinceName,d.DistrictName,s.SquareName,pr.PriceName,c.Categoryname,dr.DirectionName from memberhobby m,province p, member mb, district d,priceproperty pr, squareproperty s,direction dr,categorynews c  where m.MemberID=mb.MemberID and m.ProvinceID=p.ProvinceID and m.DistrictID=d.DistrictID and m.SquareID=s.SquareID and m.PriceID=pr.PriceID and m.DirectionID=dr.DirectionID and m.CategoryID=c.CategoryID ) as Products  where Row>=@start and Row<=@end
        public IEnumerable<BookingMegaball> ListTransactionBookingByMember(int memberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingMegaballByMemberFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListTransactionBookingOrderByDate(int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingOrderByDateFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString()=="" ? "0": reader["ValuePoint"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListTransactionBookingByMemberPaging(int memberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingByMemberPagingFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                   // transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListTransactionBookingBySearch(int memberID, DateTime fromDate, DateTime toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListBookingMegaballBySearchFrontEnd";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListTransactionBookingByDate(DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingByDate_FE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@fromDate", fromDate);
                pa[1] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdateStatusBooking(int bookingID, int status)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusBooking_FE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@status", status);
                pa[1] = new SqlParameter("@bookingID", bookingID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public int CountMemberBuyNumber(int memberID, int firstNumber, int secondNumber, int thirdNumber, int fourthNumber, int fivethNumber, DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                int count = 0;
                string sql = "SP_CountMemberBuyNumber_FE";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@fromDate", fromDate);
                pa[2] = new SqlParameter("@toDate", toDate);
                pa[3] = new SqlParameter("@firstNumber", firstNumber);
                pa[4] = new SqlParameter("@secondNumber", secondNumber);
                pa[5] = new SqlParameter("@thirdNumber", thirdNumber);
                pa[6] = new SqlParameter("@fourthNumber", fourthNumber);
                pa[7] = new SqlParameter("@fivethNumber", fivethNumber);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    count = int.Parse(reader["TotalRecord"].ToString());
                }
                return count;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool InsertListBooking(IEnumerable<BookingMegaball> LstBooking, double doublePoint)
        {
            bool rs = false;
            int intMemberID = 0;
            if(LstBooking != null && LstBooking.Count() >0)
            {
                foreach (var item in LstBooking)
                {
                    intMemberID = item.MemberID;
                    rs = InsertBooking(item);
                    if(rs == false)
                    {
                        break;
                    }
                }
                //Update point of member
                if(rs)
                {
                    rs = UpdatePointMember(intMemberID, doublePoint);
                }
            }
            return rs;
        }
        public bool UpdatePointMember(int intMemberID, double intPoint)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdatePointMemberFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@Points", intPoint);
                pa[1] = new SqlParameter("@MemberID", intMemberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public float SumValuePoint()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                float sum = 0;
                string sql = "SP_SumValuePoints";
                //SqlParameter[] pa = new SqlParameter[1];
                //pa[0] = new SqlParameter("@createdate", createdate);

                SqlCommand command = helper.GetCommandNonParameter(sql,false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    sum = float.Parse(reader["TotalCount"].ToString());
                }
                return sum;
            }
            catch (Exception ex)
            {
                //Utilitys.WriteLog(fileLog, ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        //public float SumValuePointByMemberID(int memberID)
        //{
        //    Sqlhelper helper = new Sqlhelper("", "ConnectionString");
        //    try
        //    {
        //        float sum = 0;
        //        string sql = "SP_SumValuePointByMemberID";
        //        //SqlParameter[] pa = new SqlParameter[1];
        //        //pa[0] = new SqlParameter("@createdate", createdate);

        //        SqlCommand command = helper.GetCommandNonParameter(sql, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            sum = float.Parse(reader["TotalSum"].ToString());
        //        }
        //        return sum;
        //    }
        //    catch (Exception ex)
        //    {
        //        //Utilitys.WriteLog(fileLog, ex.Message);
        //        return 0;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}
    }
}

﻿using BBH.Lotte.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GroupAdminSvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select GroupAdminSvc.svc or GroupAdminSvc.svc.cs at the Solution Explorer and start debugging.
    public class GroupAdminSvc : IGroupAdminSvc
    {

        public bool InsertGroupAdmin(GroupAdminBO admin)
        {
            return BBH.Lotte.CLP.Data.GroupAdminBusiness.InsertGroupAdmin(admin);
        }

        public IEnumerable<GroupAdminBO> ListGroupAdmin()
        {
            return BBH.Lotte.CLP.Data.GroupAdminBusiness.ListGroupAdmin();
        }

        public GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            return BBH.Lotte.CLP.Data.GroupAdminBusiness.GetGroupAdminDetail(groupID);
        }

        public bool UpdateStatusGroupAdmin(int groupID, int isActive)
        {
            return BBH.Lotte.CLP.Data.GroupAdminBusiness.UpdateStatusGroupAdmin(groupID, isActive);
        }

        public bool UpdateGroupAdmin(int groupID, string groupName)
        {
            return BBH.Lotte.CLP.Data.GroupAdminBusiness.UpdateGroupAdmin(groupID, groupName);
        }
    }
}

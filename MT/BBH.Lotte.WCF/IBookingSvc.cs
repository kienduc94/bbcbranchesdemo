﻿using BBH.Lotte.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBookingSvc" in both code and config file together.
    [ServiceContract]
    public interface IBookingSvc
    {
        [OperationContract]
        IEnumerable<BookingBO> ListAllBooking();

        [OperationContract]
        IEnumerable<BookingBO> ListAllBookingPaging(int start, int end);

        [OperationContract]
        IEnumerable<BookingBO> ListAllBookingBySearch(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end);

        [OperationContract]
        bool UpdateStatusBooking(int bookingID, int status);

        [OperationContract]
        BookingBO CountAllBooking();

    }
}

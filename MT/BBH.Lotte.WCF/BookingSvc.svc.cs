﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BBH.Lotte.CLP.Domain;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BookingSvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BookingSvc.svc or BookingSvc.svc.cs at the Solution Explorer and start debugging.
    public class BookingSvc : IBookingSvc
    {

        public IEnumerable<BookingBO> ListAllBooking()
        {
            return BBH.Lotte.CLP.Data.BookingBusiness.ListAllBooking();
        }

        public IEnumerable<BookingBO> ListAllBookingPaging(int start, int end)
        {
            return BBH.Lotte.CLP.Data.BookingBusiness.ListAllBooking(start, end);
        }

        public IEnumerable<BookingBO> ListAllBookingBySearch(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end)
        {
            return BBH.Lotte.CLP.Data.BookingBusiness.ListAllBookingBySearch(memberID, fromDate, toDate, status, start, end);
        }

        public bool UpdateStatusBooking(int bookingID, int status)
        {
            return BBH.Lotte.CLP.Data.BookingBusiness.UpdateStatusBooking(bookingID, status);
        }

        public BookingBO CountAllBooking()
        {
            return BBH.Lotte.CLP.Data.BookingBusiness.CountAllBooking();
        }
    }
}

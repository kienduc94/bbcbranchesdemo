﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain
{
    public class PrizeBO
    {
        public int AwardID { get; set; }

        public string AwardName { get; set; }

        public double AwardValues { get; set; }

        public int TotalMember { get; set; }
    }
}

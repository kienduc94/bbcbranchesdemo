﻿using System;
using BBH.LotteFE.Domain;

using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Domain.Interfaces
{
    [ServiceContract]
    public interface ICoinServices
    {
        [OperationContract]
        IEnumerable<TransactionCoinBO> ListTransactionCoinPaging(string coinID, int memberID, DateTime? fromDate, DateTime? toDate, int start, int end);

        [OperationContract]
        IEnumerable<TransactionCoinBO> ListTransactionCoinWalletBySearch(string coinID, int memberID, DateTime? fromDate, DateTime? toDate, int start, int end);

    }
}

USE [betacscjackpot_noterror]
GO
/****** Object:  Table [dbo].[CompanyInfomation]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyInfomation](
	[MemberID] [int] NOT NULL,
	[CompanyName] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[Phone] [char](50) NULL,
	[Email] [nvarchar](250) NULL,
	[Website] [nvarchar](500) NULL,
	[Description] [nvarchar](4000) NULL,
 CONSTRAINT [PK_CompanyInfomation] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[CompanyInfomation] ([MemberID], [CompanyName], [Address], [Phone], [Email], [Website], [Description]) VALUES (1, N'abcdsfgg', N'123HCM', N'01293847567                                       ', N'123@gmail.com', N'123www.com', N'asdfghjkdskfhhs')
INSERT [dbo].[CompanyInfomation] ([MemberID], [CompanyName], [Address], [Phone], [Email], [Website], [Description]) VALUES (2, N'qwert', N'123HCM', N'4354543444                                        ', N'1234@gmail.com', N'1243www.com', N'asdfghjkdskfhhs')
/****** Object:  Table [dbo].[Coin]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coin](
	[CoinID] [char](10) NOT NULL,
	[CoinName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[CoinSymbol] [varchar](50) NULL,
 CONSTRAINT [PK_Coin] PRIMARY KEY CLUSTERED 
(
	[CoinID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Coin] ([CoinID], [CoinName], [IsActive], [CoinSymbol]) VALUES (N'BTC       ', N'Bitcoin', 1, NULL)
INSERT [dbo].[Coin] ([CoinID], [CoinName], [IsActive], [CoinSymbol]) VALUES (N'ETH       ', N'Carcoin', 1, NULL)
INSERT [dbo].[Coin] ([CoinID], [CoinName], [IsActive], [CoinSymbol]) VALUES (N'XRP       ', N'Ripple', 1, NULL)
/****** Object:  Table [dbo].[TransactionPoint]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionPoint](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[Points] [int] NULL,
	[CreateDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
	[TransactionBitcoin] [nvarchar](500) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_TransactionPoint] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FreeTicketDaily]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FreeTicketDaily](
	[FreeTicketID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[Total] [int] NULL,
	[IsActive] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_FreeTicket] PRIMARY KEY CLUSTERED 
(
	[FreeTicketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FreeTicketDaily] ON
INSERT [dbo].[FreeTicketDaily] ([FreeTicketID], [Date], [Total], [IsActive], [CreatedDate]) VALUES (1, CAST(0x0000A92D018B6A62 AS DateTime), 707, 0, CAST(0x0000A92D018B6A62 AS DateTime))
INSERT [dbo].[FreeTicketDaily] ([FreeTicketID], [Date], [Total], [IsActive], [CreatedDate]) VALUES (2, CAST(0x0000A92E00734B79 AS DateTime), 1407, 0, CAST(0x0000A92E00734B79 AS DateTime))
INSERT [dbo].[FreeTicketDaily] ([FreeTicketID], [Date], [Total], [IsActive], [CreatedDate]) VALUES (3, CAST(0x0000A92F00733F0A AS DateTime), 2117, 0, CAST(0x0000A92F00733F0A AS DateTime))
INSERT [dbo].[FreeTicketDaily] ([FreeTicketID], [Date], [Total], [IsActive], [CreatedDate]) VALUES (11, CAST(0x0000A9300122FE1F AS DateTime), 2915, 1, CAST(0x0000A9300122FE1F AS DateTime))
SET IDENTITY_INSERT [dbo].[FreeTicketDaily] OFF
/****** Object:  Table [dbo].[Wallet_Address]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallet_Address](
	[WalletAddressID] [int] IDENTITY(1,1) NOT NULL,
	[WalletID] [int] NOT NULL,
	[PrivateKey] [nvarchar](500) NULL,
	[IndexWallet] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_Wallet_Address] PRIMARY KEY CLUSTERED 
(
	[WalletAddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dia chi id cua vi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Wallet_Address', @level2type=N'COLUMN',@level2name=N'WalletAddressID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID cua vi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Wallet_Address', @level2type=N'COLUMN',@level2name=N'WalletID'
GO
/****** Object:  Table [dbo].[TypeTransaction]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeTransaction](
	[TypeTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TypeTransactionName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
 CONSTRAINT [PK_TypeTransaction] PRIMARY KEY CLUSTERED 
(
	[TypeTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TypeTransaction] ON
INSERT [dbo].[TypeTransaction] ([TypeTransactionID], [TypeTransactionName], [IsActive]) VALUES (1, N'Phí deposit', 1)
INSERT [dbo].[TypeTransaction] ([TypeTransactionID], [TypeTransactionName], [IsActive]) VALUES (2, N'Phí withdraw', 1)
INSERT [dbo].[TypeTransaction] ([TypeTransactionID], [TypeTransactionName], [IsActive]) VALUES (3, N'Phí rút thưởng', 1)
SET IDENTITY_INSERT [dbo].[TypeTransaction] OFF
/****** Object:  Table [dbo].[TypeNotification]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeNotification](
	[TypeNotificationID] [int] IDENTITY(1,1) NOT NULL,
	[TypeNotificationName] [nvarchar](500) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TypeNotification] PRIMARY KEY CLUSTERED 
(
	[TypeNotificationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteTypeNews_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_DeleteTypeNews_CMS]
(
	@IsDelete smallint,
	@DeleteDate datetime,
	@DeleteUser nvarchar(250),
	@TypeNewsID int
)
as
begin
update TypeNews
set
	IsDelete=@IsDelete,
	
	DeleteDate=@DeleteDate,
	DeleteUser=@DeleteUser
	where TypeNewsID=@TypeNewsID;

end;
GO
/****** Object:  Table [dbo].[TypeConfig]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeConfig](
	[TypeConfigID] [int] IDENTITY(1,1) NOT NULL,
	[TypeConfigName] [nvarchar](250) NOT NULL,
	[IsActive] [smallint] NOT NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TypeConfig] PRIMARY KEY CLUSTERED 
(
	[TypeConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionPackage]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionPackage](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[PackageID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](3500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
 CONSTRAINT [PK_TransactionPackage] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionFee]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionFee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionTypeID] [int] NULL,
	[Fee] [numeric](18, 8) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TransactionFee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionCoin]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionCoin](
	[TransactionID] [char](150) NOT NULL,
	[WalletAddressID] [nvarchar](500) NOT NULL,
	[MemberID] [int] NOT NULL,
	[ValueTransaction] [numeric](18, 10) NOT NULL,
	[QRCode] [nvarchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Status] [smallint] NULL,
	[Note] [nvarchar](2500) NULL,
	[WalletID] [nvarchar](500) NULL,
	[TypeTransactionID] [int] NULL,
	[TransactionBitcoin] [nvarchar](500) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_TransactionCoin] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ma Giao dich coin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'TransactionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dia chi vi cua nguoi muon giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'WalletAddressID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nguoi giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gia tri giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'ValueTransaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QR code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'QRCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ngay giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'CreateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Thoi gian het han giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'ExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Trang thai giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ghi chu giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vi cua nguoi nhan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'WalletID'
GO
/****** Object:  Table [dbo].[TicketWinning]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketWinning](
	[TicketWinningID] [int] IDENTITY(1,1) NOT NULL,
	[BookingID] [int] NULL,
	[DrawID] [int] NULL,
	[BuyDate] [datetime] NULL,
	[Status] [int] NULL,
	[AwardID] [int] NULL,
	[FirstNumberBuy] [int] NULL,
	[SecondNumberBuy] [int] NULL,
	[ThirdNumberBuy] [int] NULL,
	[FourthNumberBuy] [int] NULL,
	[FivethNumberBuy] [int] NULL,
	[ExtraNumberBuy] [int] NULL,
	[FirstNumberAward] [int] NULL,
	[SecondNumberAward] [int] NULL,
	[ThirdNumberAward] [int] NULL,
	[FourthNumberAward] [int] NULL,
	[FivethNumberAward] [int] NULL,
	[ExtraNumberAward] [int] NULL,
	[NumberBallNormal] [int] NULL,
	[NumberBallGold] [int] NULL,
	[AwardValue] [decimal](18, 5) NULL,
	[AwardDate] [datetime] NULL,
	[MemberID] [int] NULL,
	[Priority] [int] NULL,
	[AwardFee] [decimal](18, 8) NULL,
 CONSTRAINT [PK_TicketWinning] PRIMARY KEY CLUSTERED 
(
	[TicketWinningID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketNumberTimeLog]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketNumberTimeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[TicketNumber] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TicketNumberTimeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ticket Number Of Times' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TicketNumberTimeLog', @level2type=N'COLUMN',@level2name=N'TicketNumber'
GO
SET IDENTITY_INSERT [dbo].[TicketNumberTimeLog] ON
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (0, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D005850CC AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (1, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D005850D1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (2, N'nhan.luong@bigbrothers.gold', 0, CAST(0x0000A92D007D70F2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (3, N'hathanhpc20@gmail.com', 0, CAST(0x0000A92D008EB1EA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (4, N'hathanhpc20@gmail.com', 35, CAST(0x0000A92D008EBB48 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (5, N'kienduc94@gmail.com', 99, CAST(0x0000A92D00915D91 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (6, N'kienduc94@gmail.com', 22, CAST(0x0000A92D00924A04 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (7, N'hathanhpc20@gmail.com', 2, CAST(0x0000A92D00950D88 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (8, N'hathanhpc20@gmail.com', 69, CAST(0x0000A92D00983E13 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (9, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D009A36A8 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (10, N'kienduc94@gmail.com', 0, CAST(0x0000A92D009E2E03 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (11, N'thanhha12196@gmail.com', 1, CAST(0x0000A92D009E4947 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (12, N'hathanhpc20@gmail.com', 82, CAST(0x0000A92D00A16131 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (13, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D00A60E7D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (14, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92D00ABC42D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (15, N'thanhha12196@gmail.com', 100, CAST(0x0000A92D00ACC91E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (16, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92D00B14B6F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (17, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D00B1D07A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (18, N'nguyenlytruongson@gmail.com', 100, CAST(0x0000A92D00B7721A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (19, N'nguyenlytruongson@gmail.com', 100, CAST(0x0000A92D00B7848F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (20, N'nguyenlytruongson@gmail.com', 97, CAST(0x0000A92D00B82396 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (21, N'daisy.k@cscjackpot.com', 99, CAST(0x0000A92D01717974 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (22, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92D01719E7F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (23, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92D017D9C7B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (24, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92D017DAA2D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (25, N'thanhha12196@gmail.com', 0, CAST(0x0000A92D017EF40B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (26, N'duc.ly@bigbrothers.gold', 0, CAST(0x0000A92D017F58E5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (27, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92D0188A0D3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (28, N'thanhha12196@gmail.com', 100, CAST(0x0000A92D01897944 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (29, N'thanhha12196@gmail.com', 0, CAST(0x0000A92D0189BC90 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (30, N'thanhha12196@gmail.com', 3, CAST(0x0000A92D0189CE9B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (31, N'hathanhpc20@gmail.com', 0, CAST(0x0000A92E0000C58C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (32, N'thanhha12196@gmail.com', 1, CAST(0x0000A92E0007A0D4 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (33, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92E00335856 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (34, N'thanhha12196@gmail.com', 4, CAST(0x0000A92E00339AD2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (35, N'thanhha12196@gmail.com', 3, CAST(0x0000A92E0033D777 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (36, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92E0043CCEF AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (37, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E004FBDE1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (38, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E004FCD50 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (39, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E004FDD77 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (40, N'thanh.ha@bigbrothers.gold', 53, CAST(0x0000A92E00542179 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (41, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E00542E1A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (42, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E00543EE0 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (43, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E00544BD7 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (44, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E00544DBE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (45, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E005455C3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (46, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E00545D53 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (47, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E00548755 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (48, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E00549B17 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (49, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E0054A5CC AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (50, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0054C74F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (51, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E0054CE44 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (52, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0055269D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (53, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E00554842 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (54, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E005568B2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (55, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E00557F54 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (56, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E0055AFFF AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (57, N'daisy.k@cscjackpot.com', 3, CAST(0x0000A92E0055BB26 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (58, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E0055C4DF AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (59, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E0055D0DD AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (60, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E0055DBD5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (61, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92E0055E8B0 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (62, N'aurora.d@cscjackpot.com', 100, CAST(0x0000A92E005ABC77 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (63, N'aurora.d@cscjackpot.com', 100, CAST(0x0000A92E005AC74E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (64, N'aurora.d@cscjackpot.com', 1, CAST(0x0000A92E005AF644 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (65, N'thanh.ha@bigbrothers.gold', 0, CAST(0x0000A92E005C48FB AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (66, N'thanh.ha@bigbrothers.gold', 0, CAST(0x0000A92E005CFAB1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (67, N'phim.ngo@bigbrothers.gold', 44, CAST(0x0000A92E00644496 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (68, N'phim.ngo@bigbrothers.gold', 0, CAST(0x0000A92E00672198 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (69, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0073A1D1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (70, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0073A53E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (71, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0073AF1F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (72, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0073BC34 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (73, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0073CD8E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (74, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0073D00E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (75, N'duc.ly@bigbrothers.gold', 99, CAST(0x0000A92E00749D31 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (76, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E0074AAAF AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (77, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E0074B3F7 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (78, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E0074BF35 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (79, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E0074C8D2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (80, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E0074D5EE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (81, N'thanh.ha@bigbrothers.gold', 99, CAST(0x0000A92E0074E1FA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (82, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0074ED63 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (83, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0074F63B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (84, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E0075A7A0 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (85, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92E0075B2AE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (86, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E00764021 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (87, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E00764A47 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (88, N'hathanhpc20@gmail.com', 99, CAST(0x0000A92E007686C6 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (89, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E00769149 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (90, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E0076D053 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (91, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E0076DAA2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (92, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E0076E562 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (93, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E0076EDD3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (94, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E0076F89C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (95, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E00770844 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (96, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92E0077144C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (97, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E00772B2B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (98, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92E00773114 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (99, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92E00773ABA AS DateTime))
GO
print 'Processed 100 total records'
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (100, N'duc.ly@bigbrothers.gold', 12, CAST(0x0000A92E00778012 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (101, N'duc.ly@bigbrothers.gold', 2, CAST(0x0000A92E00779732 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (102, N'duc.ly@bigbrothers.gold', 50, CAST(0x0000A92E0077B544 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (103, N'thanh.ha@bigbrothers.gold', 0, CAST(0x0000A92E007B8809 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (104, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92E0171B395 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (105, N'hathanhpc20@gmail.com', 20, CAST(0x0000A92E01864316 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (106, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92E018ACC37 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (107, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92F00003105 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (108, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92F00003728 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (109, N'hathanhpc20@gmail.com', 70, CAST(0x0000A92F00003F35 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (110, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F0000D633 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (111, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F0000DF76 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (112, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F0000E78E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (113, N'duc.ly@bigbrothers.gold', 3, CAST(0x0000A92F00013CAE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (114, N'buitanphat1991@gmail.com', 5, CAST(0x0000A92F00020FDB AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (115, N'buitanphat1991@gmail.com', 10, CAST(0x0000A92F00026C8B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (116, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92F00034A1A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (117, N'duc.ly@bigbrothers.gold', 3, CAST(0x0000A92F0003E682 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (118, N'duc.ly@bigbrothers.gold', 1, CAST(0x0000A92F0003ED99 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (119, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F000406A5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (120, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F00040FE9 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (121, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F00041F59 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (122, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F00042A9B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (123, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F000434D3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (124, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F00047F96 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (125, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F0004886E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (126, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F00049044 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (127, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F0004992A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (128, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F0004B7C3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (129, N'duc.ly@bigbrothers.gold', 1, CAST(0x0000A92F0005CDF9 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (130, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F000CDEC3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (131, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F000CE8D1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (132, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F000D6E7C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (133, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F000D79D4 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (134, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F000D8105 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (135, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92F0041A6FE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (136, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0051545F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (137, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F00515E92 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (138, N'duc.ly@bigbrothers.gold', 32, CAST(0x0000A92F00516842 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (139, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0059BE6D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (140, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0059C9C7 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (141, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0059D31E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (142, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0059DC62 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (143, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0059E56E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (144, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F0059FB70 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (145, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F005A058B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (146, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F005A127D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (147, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92F005A1993 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (148, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F005A21CC AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (149, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F005ABE39 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (150, N'duc.ly@bigbrothers.gold', 3, CAST(0x0000A92F005DB8A8 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (151, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F005DC2DB AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (152, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F005DCD5E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (153, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F005DDD72 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (154, N'hathanhpc20@gmail.com', 65, CAST(0x0000A92F005DE6F7 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (155, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F00617DC5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (156, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0061869D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (157, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F00618FD8 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (158, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F006198E3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (159, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92F0061A268 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (160, N'hathanhpc20@gmail.com', 14, CAST(0x0000A92F0061A921 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (161, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F006889CC AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (162, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F006893CB AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (163, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F00689D89 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (164, N'thanh.ha@bigbrothers.gold', 1, CAST(0x0000A92F0068A58A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (165, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0068ADAB AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (166, N'duc.ly@bigbrothers.gold', 33, CAST(0x0000A92F0068B5FB AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (167, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006B1617 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (168, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006B1F28 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (169, N'thanh.ha@bigbrothers.gold', 3, CAST(0x0000A92F006B280E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (170, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F006B33AE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (171, N'thanh.ha@bigbrothers.gold', 51, CAST(0x0000A92F006B3B60 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (172, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006D7D58 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (173, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006D8761 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (174, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006D8FFC AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (175, N'thanh.ha@bigbrothers.gold', 3, CAST(0x0000A92F006D9CAD AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (176, N'thanh.ha@bigbrothers.gold', 87, CAST(0x0000A92F006DA6F7 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (177, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006DD43F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (178, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006DE144 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (179, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F006DEE87 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (180, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F006DFFA0 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (181, N'thanh.ha@bigbrothers.gold', 90, CAST(0x0000A92F006E09FD AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (182, N'thanh.ha@bigbrothers.gold', 3, CAST(0x0000A92F006E1E26 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (183, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0071629C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (184, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F00716E17 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (185, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F00717930 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (186, N'thanh.ha@bigbrothers.gold', 3, CAST(0x0000A92F00717F57 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (187, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F0074AACA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (188, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F0074B971 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (189, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0074D354 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (190, N'duc.ly@bigbrothers.gold', 3, CAST(0x0000A92F0074DA2E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (191, N'thanh.ha@bigbrothers.gold', 39, CAST(0x0000A92F00751150 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (192, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F007B2AC3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (193, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F007B342C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (194, N'k.duchappy@gmail.com', 46, CAST(0x0000A92F007B3C15 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (195, N'thanh.ha@bigbrothers.gold', 0, CAST(0x0000A92F007B97D2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (196, N'thanh.ha@bigbrothers.gold', 3, CAST(0x0000A92F007CCA48 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (197, N'thanh.ha@bigbrothers.gold', 100, CAST(0x0000A92F016F61DA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (198, N'thanh.ha@bigbrothers.gold', 50, CAST(0x0000A92F016F96C5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (199, N'k.duchappy@gmail.com', 100, CAST(0x0000A92F01716BFA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (200, N'k.duchappy@gmail.com', 100, CAST(0x0000A92F0171757B AS DateTime))
GO
print 'Processed 200 total records'
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (201, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0171DEF3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (202, N'duc.ly@bigbrothers.gold', 100, CAST(0x0000A92F0171E984 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (203, N'duc.ly@bigbrothers.gold', 3, CAST(0x0000A92F0174175D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (204, N'duc.ly@bigbrothers.gold', 3, CAST(0x0000A92F01741E78 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (205, N'hathehuyen@gmail.com', 0, CAST(0x0000A93000055B37 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (206, N'carcoin123@gmail.com', 0, CAST(0x0000A93000065777 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (207, N'k.duchappy@gmail.com', 3, CAST(0x0000A930000680A1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (208, N'thanh.ha@bigbrothers.gold', 2, CAST(0x0000A9300006D864 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (209, N'bui.hoangdungtn@gmail.com', 6, CAST(0x0000A9300007EBDB AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (210, N'carcoin12345@gmail.com', 2, CAST(0x0000A93000089BE4 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (211, N'carcoin123@gmail.com', 0, CAST(0x0000A9300008B2F0 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (212, N'hathanhpc20@gmail.com', 100, CAST(0x0000A9300013DD55 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (213, N'hathanhpc20@gmail.com', 100, CAST(0x0000A9300013E699 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (214, N'hathanhpc20@gmail.com', 100, CAST(0x0000A9300013EF83 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (215, N'hathanhpc20@gmail.com', 100, CAST(0x0000A9300013F7D8 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (216, N'hathanhpc20@gmail.com', 100, CAST(0x0000A93000140233 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (217, N'hathanhpc20@gmail.com', 100, CAST(0x0000A93000140A02 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (218, N'hathanhpc20@gmail.com', 100, CAST(0x0000A93000141211 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (219, N'hathanhpc20@gmail.com', 100, CAST(0x0000A93000141C8F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (220, N'hathanhpc20@gmail.com', 100, CAST(0x0000A930002F9334 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (221, N'hathanhpc20@gmail.com', 100, CAST(0x0000A930002F9D9A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (222, N'buitanphat1991@gmail.com', 49, CAST(0x0000A930002FAFCA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (223, N'carcoin123@gmail.com', 100, CAST(0x0000A930002FCF31 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (224, N'carcoin123@gmail.com', 100, CAST(0x0000A930002FD947 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (225, N'carcoin123@gmail.com', 100, CAST(0x0000A930002FE39B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (226, N'carcoin123@gmail.com', 100, CAST(0x0000A930002FEC32 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (227, N'carcoin123@gmail.com', 38, CAST(0x0000A930002FF445 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (228, N'hathanhpc20@gmail.com', 91, CAST(0x0000A93000300F4B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (229, N'carcoin123@gmail.com', 100, CAST(0x0000A93000382396 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (230, N'carcoin123@gmail.com', 100, CAST(0x0000A93000382E88 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (231, N'carcoin123@gmail.com', 100, CAST(0x0000A9300038378A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (232, N'carcoin123@gmail.com', 100, CAST(0x0000A93000384017 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (233, N'carcoin123@gmail.com', 100, CAST(0x0000A93000384ACE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (234, N'carcoin123@gmail.com', 100, CAST(0x0000A93000385638 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (235, N'carcoin123@gmail.com', 100, CAST(0x0000A930003860FA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (236, N'carcoin123@gmail.com', 100, CAST(0x0000A93000386B9D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (237, N'carcoin123@gmail.com', 100, CAST(0x0000A930003877F0 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (238, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A199A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (239, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A2378 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (240, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A2CBC AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (241, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A362B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (242, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A3F5B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (243, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A48EA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (244, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A5177 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (245, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A5C08 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (246, N'carcoin123@gmail.com', 100, CAST(0x0000A930003A8308 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (247, N'carcoin123@gmail.com', 100, CAST(0x0000A930003AB9B2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (248, N'carcoin123@gmail.com', 100, CAST(0x0000A930003AC381 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (249, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003AE096 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (250, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003AEB66 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (251, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003AF4E8 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (252, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003AFF12 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (253, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B07BF AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (254, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B1014 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (255, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B18D9 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (256, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B2585 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (257, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B2FA1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (258, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B395A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (259, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B450D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (260, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B4DEE AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (261, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B5741 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (262, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B60B5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (263, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B6A7A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (264, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B73BF AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (265, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B81B7 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (266, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B8C3E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (267, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B9469 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (268, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003B9E40 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (269, N'carcoin12345@gmail.com', 100, CAST(0x0000A930003BAE3F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (270, N'buitanphat1991@gmail.com', 50, CAST(0x0000A9300053E2F5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (271, N'sonleader91@gmail.com', 1, CAST(0x0000A930005456B5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (272, N'carcoin123@gmail.com', 1, CAST(0x0000A93000552AF9 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (273, N'carcoin123@gmail.com', 1, CAST(0x0000A9300058739C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (274, N'carcoin12345@gmail.com', 98, CAST(0x0000A930006C7D09 AS DateTime))
SET IDENTITY_INSERT [dbo].[TicketNumberTimeLog] OFF
/****** Object:  Table [dbo].[TicketConfig]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketConfig](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[ConfigName] [nvarchar](250) NOT NULL,
	[NumberTicket] [int] NOT NULL,
	[CoinValues] [decimal](18, 10) NOT NULL,
	[CoinID] [nchar](10) NOT NULL,
	[IsActive] [smallint] NOT NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TicketConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TicketConfig] ON
INSERT [dbo].[TicketConfig] ([ConfigID], [ConfigName], [NumberTicket], [CoinValues], [CoinID], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (1, N'So coin mua vé', 1, CAST(0.0002000000 AS Decimal(18, 10)), N'BTC       ', 0, 0, CAST(0x0000A8D700000000 AS DateTime), NULL, CAST(0x0000A92F00CC57A3 AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TicketConfig] OFF
/****** Object:  Table [dbo].[SystemConfig]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemConfig](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[IsAutoLottery] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[SystemConfig] ON
INSERT [dbo].[SystemConfig] ([ConfigID], [IsAutoLottery], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (1, 1, CAST(0x0000A90700000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[SystemConfig] OFF
/****** Object:  UserDefinedFunction [dbo].[StringSplit]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[StringSplit]
(
    @String  VARCHAR(MAX), @Separator CHAR(1)
)
RETURNS @RESULT TABLE(Value VARCHAR(MAX))
AS
BEGIN     
 DECLARE @SeparatorPosition INT = CHARINDEX(@Separator, @String ),
        @Value VARCHAR(MAX), @StartPosition INT = 1
 
 IF @SeparatorPosition = 0  
  BEGIN
   INSERT INTO @RESULT VALUES(@String)
   RETURN
  END
     
 SET @String = @String + @Separator
 WHILE @SeparatorPosition > 0
  BEGIN
   SET @Value = SUBSTRING(@String , @StartPosition, @SeparatorPosition- @StartPosition)
 
   IF( @Value <> ''  ) 
    INSERT INTO @RESULT VALUES(@Value)
   
   SET @StartPosition = @SeparatorPosition + 1
   SET @SeparatorPosition = CHARINDEX(@Separator, @String , @StartPosition)
  END    
     
 RETURN
END
GO
/****** Object:  Table [dbo].[StoreFreeTicket]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreFreeTicket](
	[StoreFreeTicketID] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [int] NULL,
 CONSTRAINT [PK_StoreFreeTicket] PRIMARY KEY CLUSTERED 
(
	[StoreFreeTicketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[StoreFreeTicket] ON
INSERT [dbo].[StoreFreeTicket] ([StoreFreeTicketID], [Amount]) VALUES (1, 1562)
SET IDENTITY_INSERT [dbo].[StoreFreeTicket] OFF
/****** Object:  StoredProcedure [dbo].[SP_UpdatePriorityTopic_CMS]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_UpdatePriorityTopic_CMS]
(
	@TopicID int,
	@Priority int
)
as 
begin
update topic
set [Priority]=@Priority
where topicid=@topicid;
end;
GO
/****** Object:  Table [dbo].[ExchangeTicket]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangeTicket](
	[ExchangeID] [int] IDENTITY(1,1) NOT NULL,
	[PointValue] [float] NULL,
	[TicketNumber] [int] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[MemberID] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [date] NULL,
	[DeleteUser] [nvarchar](250) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_ExchangeTicket] PRIMARY KEY CLUSTERED 
(
	[ExchangeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ExchangeTicket] ON
INSERT [dbo].[ExchangeTicket] ([ExchangeID], [PointValue], [TicketNumber], [CreateDate], [CreateUser], [MemberID], [IsActive], [IsDelete], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser], [CoinID]) VALUES (29, 1, 20, CAST(0x0000A86700F2B28E AS DateTime), N'administrator', 1, 1, 0, CAST(0x0000A86700F2B28E AS DateTime), N'', CAST(0xC23D0B00 AS Date), N'administrator', N'XRP       ')
SET IDENTITY_INSERT [dbo].[ExchangeTicket] OFF
/****** Object:  Table [dbo].[ExchangePoint]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangePoint](
	[ExchangeID] [int] IDENTITY(1,1) NOT NULL,
	[PointValue] [float] NULL,
	[BitcoinValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[MemberID] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_ExchangePoint] PRIMARY KEY CLUSTERED 
(
	[ExchangeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ExchangePoint] ON
INSERT [dbo].[ExchangePoint] ([ExchangeID], [PointValue], [BitcoinValue], [CreateDate], [CreateUser], [MemberID], [IsActive], [IsDelete], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (4, 20000, 1, CAST(0x0000A8180023A1FC AS DateTime), N'administrator', 1, 1, 0, CAST(0x0000A86501265AAD AS DateTime), N'administrator', NULL, NULL)
SET IDENTITY_INSERT [dbo].[ExchangePoint] OFF
/****** Object:  Table [dbo].[Draw]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draw](
	[DrawID] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Draw] PRIMARY KEY CLUSTERED 
(
	[DrawID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Draw] ON
INSERT [dbo].[Draw] ([DrawID], [StartDate], [EndDate], [CreatedDate]) VALUES (33, CAST(0x0000A92D00D63BC0 AS DateTime), CAST(0x0000A93000DE7920 AS DateTime), CAST(0x0000A92D00EC1334 AS DateTime))
INSERT [dbo].[Draw] ([DrawID], [StartDate], [EndDate], [CreatedDate]) VALUES (68, CAST(0x0000A930009450C0 AS DateTime), CAST(0x0000A932009450C0 AS DateTime), CAST(0x0000A93000AFA175 AS DateTime))
SET IDENTITY_INSERT [dbo].[Draw] OFF
/****** Object:  Table [dbo].[Country]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryID] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](250) NULL,
	[PhoneZipCode] [char](10) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Country] ON
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (200, N'Afghanistan', N'+93       ', 1, 0, CAST(0x0000A92D011FC456 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (201, N'Albania', N'+355      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (202, N'Algeria', N'+213      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (203, N'AmericanSamoa', N'+1 684    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (204, N'Andorra', N'+376      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (205, N'Angola', N'+244      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (206, N'Anguilla', N'+1 264    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (207, N'Antigua and Barbuda', N'+1268     ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (208, N'Argentina', N'+54       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (209, N'Armenia', N'+374      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (210, N'Aruba', N'+297      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (211, N'Australia', N'+61       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (212, N'Austria', N'+43       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (213, N'Azerbaijan', N'+994      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (214, N'Bahamas', N'+1 242    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (215, N'Bahrain', N'+973      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (216, N'Bangladesh', N'+880      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (217, N'Barbados', N'+1 246    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (218, N'Belarus', N'+375      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (219, N'Belgium', N'+32       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (220, N'Belize', N'+501      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (221, N'Benin', N'+229      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (222, N'Bermuda', N'+1 441    ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (223, N'Bhutan', N'+975      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (224, N'Bosnia and Herzegovina', N'+387      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (225, N'Botswana', N'+267      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (226, N'Brazil', N'+55       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (227, N'British Indian Ocean Territory', N'+246      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (228, N'Bulgaria', N'+359      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (229, N'Burkina Faso', N'+226      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (230, N'Burundi', N'+257      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (231, N'Cambodia', N'+855      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (232, N'Cameroon', N'+237      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (233, N'Cape Verde', N'+238      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (234, N'Cayman Islands', N'+345      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (235, N'Central African Republic', N'+236      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (236, N'Chad', N'+235      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (237, N'Chile', N'+56       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (238, N'China', N'+86       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (239, N'Christmas Island', N'+61       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (240, N'Colombia', N'+57       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (241, N'Comoros', N'+269      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (242, N'Congo', N'+242      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (243, N'Cook Islands', N'+682      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (244, N'Costa Rica', N'+506      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (245, N'Croatia', N'+385      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (246, N'Cuba', N'+53       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (247, N'Cyprus', N'+537      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (248, N'Czech Republic', N'+420      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (249, N'Denmark', N'+45       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (250, N'Djibouti', N'+253      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (251, N'Dominica', N'+1 767    ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (252, N'Dominican Republic', N'+1 849    ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (253, N'Ecuador', N'+593      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (254, N'Egypt', N'+20       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (255, N'El Salvador', N'+503      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (256, N'Equatorial Guinea', N'+240      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (257, N'Eritrea', N'+291      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (258, N'Estonia', N'+372      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (259, N'Ethiopia', N'+251      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (260, N'Faroe Islands', N'+298      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (261, N'Fiji', N'+679      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (262, N'Finland', N'+358      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (263, N'France', N'+33       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (264, N'French Guiana', N'+594      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (265, N'French Polynesia', N'+689      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (266, N'Gabon', N'+241      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (267, N'Gambia', N'+220      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (268, N'Georgia', N'+995      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (269, N'Germany', N'+49       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (270, N'Ghana', N'+233      ', 1, 0, CAST(0x0000A92D011FC46D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (271, N'Gibraltar', N'+350      ', 1, 0, CAST(0x0000A92D011FC472 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (272, N'Greece', N'+30       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (273, N'Greenland', N'+299      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (274, N'Grenada', N'+1 473    ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (275, N'Guadeloupe', N'+590      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (276, N'Guam', N'+1 671    ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (277, N'Guatemala', N'+502      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (278, N'Guinea', N'+224      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (279, N'Guinea-Bissau', N'+245      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (280, N'Guyana', N'+595      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (281, N'Haiti', N'+509      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (282, N'Honduras', N'+504      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (283, N'Hungary', N'+36       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (284, N'Iceland', N'+354      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (285, N'India', N'+91       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (286, N'Indonesia', N'+62       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (287, N'Iraq', N'+964      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (288, N'Ireland', N'+353      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (289, N'Israel', N'+972      ', 1, 0, CAST(0x0000A92D011FC47B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (290, N'Italy', N'+39       ', 1, 0, CAST(0x0000A92D011FC47B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (291, N'Jamaica', N'+1 876    ', 1, 0, CAST(0x0000A92D011FC485 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (292, N'Japan', N'+81       ', 1, 0, CAST(0x0000A92D011FC489 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (293, N'Jordan', N'+962      ', 1, 0, CAST(0x0000A92D011FC492 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (294, N'Kazakhstan', N'+7 7      ', 1, 0, CAST(0x0000A92D011FC497 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (295, N'Kenya', N'+254      ', 1, 0, CAST(0x0000A92D011FC49C AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (296, N'Kiribati', N'+686      ', 1, 0, CAST(0x0000A92D011FC4A1 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (297, N'Kyrgyzstan', N'+996      ', 1, 0, CAST(0x0000A92D011FC4A5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (298, N'Latvia', N'+371      ', 1, 0, CAST(0x0000A92D011FC4AF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (299, N'Lesotho', N'+266      ', 1, 0, CAST(0x0000A92D011FC4B3 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (300, N'Liberia', N'+231      ', 1, 0, CAST(0x0000A92D011FC4B3 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (301, N'Liechtenstein', N'+423      ', 1, 0, CAST(0x0000A92D011FC4BD AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (302, N'Lithuania', N'+370      ', 1, 0, CAST(0x0000A92D011FC4BD AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (303, N'Luxembourg', N'+352      ', 1, 0, CAST(0x0000A92D011FC4C6 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (304, N'Madagascar', N'+261      ', 1, 0, CAST(0x0000A92D011FC4CB AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (305, N'Malawi', N'+265      ', 1, 0, CAST(0x0000A92D011FC4D4 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (306, N'Malaysia', N'+60       ', 1, 0, CAST(0x0000A92D011FC4DD AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (307, N'Maldives', N'+960      ', 1, 0, CAST(0x0000A92D011FC4E2 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (308, N'Mali', N'+223      ', 1, 0, CAST(0x0000A92D011FC4E7 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (309, N'Malta', N'+356      ', 1, 0, CAST(0x0000A92D011FC4E7 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (310, N'Marshall Islands', N'+692      ', 1, 0, CAST(0x0000A92D011FC4F0 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (311, N'Martinique', N'+596      ', 1, 0, CAST(0x0000A92D011FC4F0 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (312, N'Mauritania', N'+222      ', 1, 0, CAST(0x0000A92D011FC4F0 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (313, N'Mauritius', N'+230      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (314, N'Mayotte', N'+262      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (315, N'Mexico', N'+52       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (316, N'Monaco', N'+377      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (317, N'Mongolia', N'+976      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (318, N'Montenegro', N'+382      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (319, N'Montserrat', N'+1664     ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (320, N'Morocco', N'+212      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (321, N'Myanmar', N'+95       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (322, N'Namibia', N'+264      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (323, N'Nauru', N'+674      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (324, N'Nepal', N'+977      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (325, N'Netherlands', N'+31       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (326, N'Netherlands Antilles', N'+599      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (327, N'New Caledonia', N'+687      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (328, N'New Zealand', N'+64       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (329, N'Nicaragua', N'+505      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (330, N'Niger', N'+227      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (331, N'Nigeria', N'+234      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (332, N'Niue', N'+683      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (333, N'Norfolk Island', N'+672      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (334, N'Northern Mariana Islands', N'+1 670    ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (335, N'Norway', N'+47       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (336, N'Oman', N'+968      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (337, N'Pakistan', N'+92       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (338, N'Palau', N'+680      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (339, N'Panama', N'+507      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (340, N'Papua New Guinea', N'+675      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (341, N'Paraguay', N'+595      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (342, N'Peru', N'+51       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (343, N'Philippines', N'+63       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (344, N'Poland', N'+48       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (345, N'Portugal', N'+351      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (346, N'Puerto Rico', N'+1 939    ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (347, N'Romania', N'+40       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (348, N'Rwanda', N'+250      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (349, N'Samoa', N'+685      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (350, N'San Marino', N'+378      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (351, N'Saudi Arabia', N'+966      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (352, N'Senegal', N'+221      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (353, N'Serbia', N'+381      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (354, N'Seychelles', N'+248      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (355, N'Sierra Leone', N'+232      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (356, N'Slovakia', N'+421      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (357, N'Slovenia', N'+386      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (358, N'Solomon Islands', N'+677      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (359, N'South Africa', N'+27       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (360, N'South Georgia and the South Sandwich Islands', N'+500      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (361, N'Spain', N'+34       ', 1, 0, CAST(0x0000A92D011FC4FE AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (362, N'Sri Lanka', N'+94       ', 1, 0, CAST(0x0000A92D011FC4FE AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (363, N'Sudan', N'+249      ', 1, 0, CAST(0x0000A92D011FC503 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (364, N'Suriname', N'+597      ', 1, 0, CAST(0x0000A92D011FC508 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (365, N'Swaziland', N'+268      ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (366, N'Sweden', N'+46       ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (367, N'Switzerland', N'+41       ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (368, N'Tajikistan', N'+992      ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (369, N'Thailand', N'+66       ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (370, N'Togo', N'+228      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (371, N'Tokelau', N'+690      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (372, N'Tonga', N'+676      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (373, N'Trinidad and Tobago', N'+1 868    ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (374, N'Tunisia', N'+216      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (375, N'Turkey', N'+90       ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (376, N'Turkmenistan', N'+993      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (377, N'Turks and Caicos Islands', N'+1 649    ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (378, N'Tuvalu', N'+688      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (379, N'Uganda', N'+256      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (380, N'Ukraine', N'+380      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (381, N'United Kingdom', N'+44       ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (382, N'Uruguay', N'+598      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (383, N'Uzbekistan', N'+998      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (384, N'Vanuatu', N'+678      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (385, N'Wallis and Futuna', N'+681      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (386, N'Yemen', N'+967      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (387, N'Zambia', N'+260      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (388, N'Zimbabwe', N'+263      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (389, N'Bolivia, Plurinational State of', N'+591      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (390, N'Cocos (Keeling) Islands', N'+61       ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (391, N'Congo, The Democratic Republic of the', N'+243      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (392, N'Cote d"Ivoire', N'+225      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (393, N'Falkland Islands (Malvinas)', N'+500      ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (394, N'Guernsey', N'+44       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (395, N'Holy See (Vatican City State)', N'+379      ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (396, N'Hong Kong', N'+852      ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (397, N'Iran, Islamic Republic of', N'+98       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (398, N'Isle of Man', N'+44       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (399, N'Jersey', N'+44       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (400, N'Korea, Democratic People"s Republic of', N'+850      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (401, N'Korea, Republic of', N'+82       ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (402, N'Lao People"s Democratic Republic', N'+856      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (403, N'Libyan Arab Jamahiriya', N'+218      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (404, N'Macao', N'+853      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (405, N'Macedonia, The Former Yugoslav Republic of', N'+389      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (406, N'Micronesia, Federated States of', N'+691      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (407, N'Moldova, Republic of', N'+373      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (408, N'Mozambique', N'+258      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (409, N'Palestinian Territory, Occupied', N'+970      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (410, N'Pitcairn', N'+872      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (411, N'Réunion', N'+262      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (412, N'Russia', N'+7        ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (413, N'Saint Barthélemy', N'+590      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (414, N'Saint Helena, Ascension and Tristan Da Cunha', N'+290      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (415, N'Saint Kitts and Nevis', N'+1 869    ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (416, N'Saint Lucia', N'+1 758    ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (417, N'Saint Martin', N'+590      ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (418, N'Saint Pierre and Miquelon', N'+508      ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (419, N'Saint Vincent and the Grenadines', N'+1 784    ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (420, N'Sao Tome and Principe', N'+239      ', 1, 0, CAST(0x0000A92D01246808 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (421, N'Somalia', N'+252      ', 1, 0, CAST(0x0000A92D0124680D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (422, N'Svalbard and Jan Mayen', N'+47       ', 1, 0, CAST(0x0000A92D01246811 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (423, N'Syrian Arab Republic', N'+963      ', 1, 0, CAST(0x0000A92D01246816 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (424, N'Taiwan, Province of China', N'+886      ', 1, 0, CAST(0x0000A92D01246816 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (425, N'Tanzania, United Republic of', N'+255      ', 1, 0, CAST(0x0000A92D0124681A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (426, N'Timor-Leste', N'+670      ', 1, 0, CAST(0x0000A92D0124681F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (427, N'Venezuela, Bolivarian Republic of', N'+58       ', 1, 0, CAST(0x0000A92D01246824 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (428, N'Viet Nam', N'+84       ', 1, 0, CAST(0x0000A92D01246828 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (429, N'Virgin Islands, British', N'+1 284    ', 1, 0, CAST(0x0000A92D0124682D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (430, N'Virgin Islands, U.S.', N'+1 340    ', 1, 0, CAST(0x0000A92D0124682D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Country] OFF
/****** Object:  Table [dbo].[Package]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Package](
	[PackageID] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [nvarchar](250) NULL,
	[PackageValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[NumberExpire] [int] NULL,
	[IsActive] [smallint] NULL,
	[Priority] [int] NULL,
	[PackageNameEnglish] [nvarchar](250) NULL,
 CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED 
(
	[PackageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Package] ON
INSERT [dbo].[Package] ([PackageID], [PackageName], [PackageValue], [CreateDate], [NumberExpire], [IsActive], [Priority], [PackageNameEnglish]) VALUES (1, N'1 Euro/tuần', 1, CAST(0x0000A79000000000 AS DateTime), 7, 1, 1, N'1 Euro/week')
INSERT [dbo].[Package] ([PackageID], [PackageName], [PackageValue], [CreateDate], [NumberExpire], [IsActive], [Priority], [PackageNameEnglish]) VALUES (2, N'6 Euro/tháng', 5, CAST(0x0000A79000000000 AS DateTime), 30, 1, 2, N'6 Euro/month')
INSERT [dbo].[Package] ([PackageID], [PackageName], [PackageValue], [CreateDate], [NumberExpire], [IsActive], [Priority], [PackageNameEnglish]) VALUES (3, N'35 Euro/năm', 15, CAST(0x0000A79000000000 AS DateTime), 365, 1, 3, N'35 Euro/year')
SET IDENTITY_INSERT [dbo].[Package] OFF
/****** Object:  Table [dbo].[NotificationSystem]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSystem](
	[NotificationID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](4000) NULL,
	[SenderID] [int] NULL,
	[ReceiveMemberID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Status] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[TypeNotificationID] [int] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_NotificationSystem] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemberReference]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberReference](
	[MemberID] [int] NOT NULL,
	[LinkReference] [nvarchar](250) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[MemberIDReference] [int] NOT NULL,
	[LockID] [nvarchar](250) NULL,
	[Status] [smallint] NULL,
	[Amount] [decimal](18, 5) NULL,
 CONSTRAINT [PK_MemberReference] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC,
	[MemberIDReference] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (910, N'daisy.k@cscjackpot.com/2d697732311d73645cf44932ad30b95e', CAST(0x0000A92E0099F301 AS DateTime), 910, N'c8600268-9468-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (913, N'hathanhpc20@gmail.com/0702697ae6c70215153aa362fe60f54b', CAST(0x0000A92E00B1668A AS DateTime), 913, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (913, N'hathanhpc20@gmail.com/0702697ae6c70215153aa362fe60f54b', CAST(0x0000A92E00B2DCAD AS DateTime), 914, N'4bd0326b-9475-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (914, N'thanhha12196@gmail.com/e7dcb3321918c5ac7a7b8c8fa4e748e5', CAST(0x0000A92E00B2DCAD AS DateTime), 914, N'4bc7e30e-9475-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (915, N'thienbui1987@gmail.com/b3eabf9a2a7747c1734743ded56c5141', CAST(0x0000A92E00B36B22 AS DateTime), 915, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A92E01094A8E AS DateTime), 916, N'de7451dd-94a2-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A92E010E2BA6 AS DateTime), 919, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A92E01114AAF AS DateTime), 921, N'62110777-94a5-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A92E01292C93 AS DateTime), 924, N'6051c28f-94b1-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A92E012B6F5F AS DateTime), 925, N'7bac3e85-94b2-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A92F00E143B3 AS DateTime), 927, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A92F012C0271 AS DateTime), 928, N'12ce6dae-957c-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (916, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', CAST(0x0000A93000BABEE2 AS DateTime), 930, N'aebdef4d-960b-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (917, N'daisy.l@mycarcoin.com/de4188e1101974e968073a5c0e779fce', CAST(0x0000A92E010C74C3 AS DateTime), 917, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (918, N'daisy.k@mycarcoin.com/2581495f5f975b79e64f3e66097f120d', CAST(0x0000A92E010D193A AS DateTime), 918, N'15e38ebd-94a3-11e8-8c19-00163ec5394d', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (919, N'nguyenlytruongson@gmail.com/dd9b2cbbb0067099208980c67a14c623', CAST(0x0000A92E010E2BA1 AS DateTime), 919, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (920, N'aurora.d@cscjackpot.com/05ca41d53ac1d414bd9d1911e8ab40d3', CAST(0x0000A92E010E9690 AS DateTime), 920, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (921, N'thu.nguyen@cryptolending.org/7ede302a2dd37ddfe165e7dce87ea3fa', CAST(0x0000A92E01114A97 AS DateTime), 921, N'62080bde-94a5-11e8-8c19-00163ec5394d', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (922, N'phim.ngo@bigbrothers.gold/6aca2802b5f9d5cdc556026a929dee1e', CAST(0x0000A92E01131FDA AS DateTime), 922, N'346c13d5-94aa-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (923, N'trungnhan.luong@gmail.com/dd0da7bf59b287a35a889389a6ddaccd', CAST(0x0000A92E011352EC AS DateTime), 923, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (924, N'duc.ly@bigbrothers.gold/438c75c5d41d50bfee926aa361bd3271', CAST(0x0000A92E01292C8E AS DateTime), 924, N'43454e92-9542-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (925, N'hathanhpc20@gmail.com/6f639edbb127e259c8cec3b19d45c59d', CAST(0x0000A92E012B6F56 AS DateTime), 925, N'7ba408a0-94b2-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (926, N'buitanphat1991@gmail.com/100275db8f727230729916348516c0b7', CAST(0x0000A92F00B51E6E AS DateTime), 926, N'612f0ce0-9540-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (927, N'toan8181@gmail.com/5614c5b3c6f2cace820a5c9e1605eabe', CAST(0x0000A92F00E143AE AS DateTime), 927, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (928, N'k.duchappy@gmail.com/67e2e2620e2c7109c5fe0db829adbcf9', CAST(0x0000A92F012C026C AS DateTime), 928, N'12bf1b43-957c-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (929, N'hathehuyen@gmail.com/0fec30a0db657fecb7f9d63bbac5d8dd', CAST(0x0000A93000AF0985 AS DateTime), 929, N'f1baf976-9605-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (930, N'carcoin123@gmail.com/6443457ff92986866d4f0d5d3a00d2ee', CAST(0x0000A93000BABEDE AS DateTime), 930, N'aeb4dd5a-960b-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (930, N'carcoin123@gmail.com/6443457ff92986866d4f0d5d3a00d2ee', CAST(0x0000A93000BCCC45 AS DateTime), 932, N'b576febd-960c-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (931, N'bui.hoangdungtn@gmail.com/30682a2c7e655a231a12c807250fc53e', CAST(0x0000A93000BCB838 AS DateTime), 931, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (932, N'carcoin12345@gmail.com/4d28b3eb6cd742dd80720b5afd0a90cb', CAST(0x0000A93000BCCC40 AS DateTime), 932, N'b56e6890-960c-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (933, N'sonleader91@gmail.com/acb68e1eb0aa825edcb02b184fad566d', CAST(0x0000A9300108E060 AS DateTime), 933, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (934, N'nguyenthiyen2204@gmail.com/c6aa67dbf46e8df28cf3a4b2b6d91aa9', CAST(0x0000A930010CA662 AS DateTime), 934, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (935, N'duckien94@gmail.com/c1a3cabadd3f1b355897807c04787e89', CAST(0x0000A930011BA399 AS DateTime), 935, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
/****** Object:  Table [dbo].[Member_Wallet]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Member_Wallet](
	[WalletID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[IndexWallet] [int] NULL,
	[NumberCoin] [float] NULL,
	[IsActive] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
	[IsDelete] [smallint] NULL,
	[WalletAddress] [nvarchar](250) NULL,
	[RippleAddress] [nvarchar](250) NULL,
	[SerectKeyRipple] [nvarchar](250) NULL,
 CONSTRAINT [PK_Member_Wallet] PRIMARY KEY CLUSTERED 
(
	[WalletID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Member]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[MemberID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NULL,
	[E_Wallet] [nvarchar](250) NULL,
	[Password] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[Points] [numeric](18, 10) NULL,
	[CreateDate] [datetime] NULL,
	[FullName] [nvarchar](250) NULL,
	[Mobile] [char](15) NULL,
	[Avatar] [nvarchar](250) NULL,
	[Gender] [smallint] NULL,
	[Birthday] [datetime] NULL,
	[ClientID] [int] NULL,
	[IsCompany] [smallint] NULL,
	[IsUserType] [smallint] NULL,
	[UserTypeID] [varchar](100) NULL,
	[MemberIDSync] [varchar](50) NULL,
	[LinkLogin] [nvarchar](500) NULL,
	[NumberTicketFree] [int] NULL,
	[LinkReference] [nvarchar](500) NULL,
	[SmsCode] [nvarchar](50) NULL,
	[IsIndentifySms] [smallint] NULL,
	[ExpireSmSCode] [datetime] NULL,
	[TotalSmsCodeInput] [int] NULL,
 CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Login normal, 1: Login facebook, 2: Login google+' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'IsUserType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FacebookID or GoogleID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'UserTypeID'
GO
SET IDENTITY_INSERT [dbo].[Member] ON
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (910, N'daisy.k@cscjackpot.com', NULL, N'6db49b6ed8940914aae82790dcf297fe', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E0099EE9F AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'daisy.k@cscjackpot.com/464032dacc912cd0c55f3a0390ba3500', 0, N'daisy.k@cscjackpot.com/2d697732311d73645cf44932ad30b95e', N'YnT4Jbyv', 1, CAST(0x0000A92E00CBFAC0 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (915, N'thienbui1987@gmail.com', NULL, N'4e995768a6900f4ea22d2664a4c6691a', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E00B369EE AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'thienbui1987@gmail.com/b291363355cf1b35011cfcdbc1f8aa92', 0, N'thienbui1987@gmail.com/b3eabf9a2a7747c1734743ded56c5141', NULL, 0, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (916, N'thanh.ha@bigbrothers.gold', NULL, N'4e995768a6900f4ea22d2664a4c6691a', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E010948A5 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'thanh.ha@bigbrothers.gold/f8be97ac92cb98899a4633170fafc176', 0, N'thanh.ha@bigbrothers.gold/cffcbf0f2de18ff5b7c165a08e8d74ea', N'Px9zo1Px', 1, CAST(0x0000A92E013E2550 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (917, N'daisy.l@mycarcoin.com', NULL, N'dbda0a38a7ad4c1e018f3841a104ecfb', 0, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E010C739A AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'daisy.l@mycarcoin.com/0e90977e3539dd7d31063c29374fd318', 0, N'daisy.l@mycarcoin.com/de4188e1101974e968073a5c0e779fce', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (918, N'daisy.k@mycarcoin.com', NULL, N'3e1ae218a5694ebec134fffb7d29dff6', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E010D1764 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'daisy.k@mycarcoin.com/b232f878567f5a7afabb728abb151c3c', 1, N'daisy.k@mycarcoin.com/2581495f5f975b79e64f3e66097f120d', N'WzUP1O2i', 1, CAST(0x0000A92E013EBED4 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (919, N'nguyenlytruongson@gmail.com', NULL, N'4e995768a6900f4ea22d2664a4c6691a', 0, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E010E2A78 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'nguyenlytruongson@gmail.com/bac0011c139d9d9b624ea6bb23c54f1e', 0, N'nguyenlytruongson@gmail.com/dd9b2cbbb0067099208980c67a14c623', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (920, N'aurora.d@cscjackpot.com', NULL, N'f0084562657f573595764eb7cbf499d5', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E010E948B AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'aurora.d@cscjackpot.com/71c524b7abb7989f0c09481f58e73606', 0, N'aurora.d@cscjackpot.com/05ca41d53ac1d414bd9d1911e8ab40d3', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (921, N'thu.nguyen@cryptolending.org', NULL, N'6db49b6ed8940914aae82790dcf297fe', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E01114911 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'thu.nguyen@cryptolending.org/f526b34f9f2b22dbf2b58f5541ac83d0', 1, N'thu.nguyen@cryptolending.org/7ede302a2dd37ddfe165e7dce87ea3fa', N'PdPIp4Tu', 1, CAST(0x0000A92E01435188 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (922, N'phim.ngo@bigbrothers.gold', NULL, N'346b6a073930a067038709dce99622ac', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E01131DD1 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'phim.ngo@bigbrothers.gold/5c581e7d8cd1dc6d17a718c4855c0f6f', 0, N'phim.ngo@bigbrothers.gold/6aca2802b5f9d5cdc556026a929dee1e', N'mVOPPQCB', 1, CAST(0x0000A92E014A7F08 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (923, N'trungnhan.luong@gmail.com', NULL, N'84adb6bc8a512c59f9805b43989ff7ef', 0, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E01135246 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'trungnhan.luong@gmail.com/9dc089c0b4ef951d5cfd19742bb80007', 0, N'trungnhan.luong@gmail.com/dd0da7bf59b287a35a889389a6ddaccd', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (924, N'duc.ly@bigbrothers.gold', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E01292B16 AS DateTime), N' ', N'01649663678    ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'duc.ly@bigbrothers.gold/9532ed5896bfd6f4ea1aa2cfb6bcaf96', 0, N'duc.ly@bigbrothers.gold/438c75c5d41d50bfee926aa361bd3271', N'uK7DiHpa', 1, CAST(0x0000A92F00EC0A90 AS DateTime), 2)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (925, N'hathanhpc20@gmail.com', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E012B6E94 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'hathanhpc20@gmail.com/ae16873f29c4a9786f40894e0535eddc', 0, N'hathanhpc20@gmail.com/6f639edbb127e259c8cec3b19d45c59d', N'm6xaYI3s', 1, CAST(0x0000A92E015D1D0C AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (926, N'buitanphat1991@gmail.com', NULL, N'b643a5b3bf9512c6036bf1edf4433746', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92F00B515C3 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'buitanphat1991@gmail.com/c5083c1ab90257f87016fd6191d0cc6a', 0, N'buitanphat1991@gmail.com/100275db8f727230729916348516c0b7', N'oYNqo7p9', 1, CAST(0x0000A92F00E87AD8 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (927, N'toan8181@gmail.com', NULL, N'', 0, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92F00E141DD AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'toan8181@gmail.com/c1191ed8c4ddc154ece7abe4d2b65cc3', 0, N'toan8181@gmail.com/5614c5b3c6f2cace820a5c9e1605eabe', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (928, N'k.duchappy@gmail.com', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92F012C0062 AS DateTime), N' ', N'0975071338     ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'k.duchappy@gmail.com/5497b8dad5549faa5df5dc3f90b92098', 0, N'k.duchappy@gmail.com/67e2e2620e2c7109c5fe0db829adbcf9', N'go7fnjbv', 1, CAST(0x0000A92F015DDAE4 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (929, N'hathehuyen@gmail.com', NULL, N'bbeb78b4af060faa8b665a383fd34ccd', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A93000AF0655 AS DateTime), N' ', N'985290355      ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'hathehuyen@gmail.com/f32756a44255c49b8724d644d86b792e', 0, N'hathehuyen@gmail.com/0fec30a0db657fecb7f9d63bbac5d8dd', N'2cSJN71y', 1, CAST(0x0000A93000E17404 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (930, N'carcoin123@gmail.com', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A93000BABD6B AS DateTime), N' ', N'0979690154     ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'carcoin123@gmail.com/39b2a3fef591c7d745cc23b944e549c6', 0, N'carcoin123@gmail.com/6443457ff92986866d4f0d5d3a00d2ee', N'OSjYC5CU', 1, CAST(0x0000A93000ECA9F0 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (931, N'bui.hoangdungtn@gmail.com', NULL, N'cbd38c62b55c93be20b8cd6b13e58b02', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A93000BCB772 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'bui.hoangdungtn@gmail.com/fed12aafc74f575038f9f0f95de38b59', 0, N'bui.hoangdungtn@gmail.com/30682a2c7e655a231a12c807250fc53e', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (932, N'carcoin12345@gmail.com', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A93000BCCB87 AS DateTime), N' ', N'0909910975     ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'carcoin12345@gmail.com/84c4a781e1415b3afcf25439da705779', 0, N'carcoin12345@gmail.com/4d28b3eb6cd742dd80720b5afd0a90cb', N'BX8is6PO', 1, CAST(0x0000A93000EEC758 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (933, N'sonleader91@gmail.com', NULL, N'0fa8314639d730e826c244eb8eeec205', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A9300108DDB6 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'sonleader91@gmail.com/f627c8208508203ec5e69521cceb5e19', 0, N'sonleader91@gmail.com/acb68e1eb0aa825edcb02b184fad566d', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (934, N'nguyenthiyen2204@gmail.com', NULL, N'e57e64096aebf4e76cc201daa8f72c2d', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A930010CA323 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'nguyenthiyen2204@gmail.com/110eb9a07ac45d652274cdd65720cc55', 0, N'nguyenthiyen2204@gmail.com/c6aa67dbf46e8df28cf3a4b2b6d91aa9', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (935, N'duckien94@gmail.com', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 0, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A930011BA198 AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'duckien94@gmail.com/3da824bd02c7c9cd09c91c853a81ddd3', 0, N'duckien94@gmail.com/c1a3cabadd3f1b355897807c04787e89', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Member] OFF
/****** Object:  Table [dbo].[LinkResetPassword]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LinkResetPassword](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[LinkReset] [nvarchar](1000) NULL,
	[Status] [smallint] NULL,
	[NumberSend] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ExpireLink] [datetime] NULL,
	[IPAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_LinkResetPassword] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[LinkResetPassword] ON
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (55, N'hathanhpc20@gmail.com', N'/email=hathanhpc20@gmail.com&&key=0f1f4a8184b1c7f7459164a4c57fdbfe', 1, 1, CAST(0x0000A92D01446842 AS DateTime), CAST(0x0000A92D01487DC0 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (56, N'daisy.k@cscjackpot.com', N'/email=daisy.k@cscjackpot.com&&key=e65441999647aceb9fb464a14709a6a3', 1, 1, CAST(0x0000A92E00AFEE1C AS DateTime), CAST(0x0000A92E00B3E6B0 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (57, N'hathanhpc20@gmail.com', N'/email=hathanhpc20@gmail.com&&key=8e6569eeec31d8ea46c55310ee259e8e', 1, 1, CAST(0x0000A92E012A85E4 AS DateTime), CAST(0x0000A92E012E6610 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (58, N'buitanphat1991@gmail.com', N'/email=buitanphat1991@gmail.com&&key=049329cb57c0c7d6e0e7ef428d65e16e', 0, 1, CAST(0x0000A92F00B98179 AS DateTime), CAST(0x0000A92F00BD83A0 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (59, N'toan8181@gmail.com', N'/email=toan8181@gmail.com&&key=ffc32ab212c8e588eb52464177b09ef3', 1, 1, CAST(0x0000A93000C4AF9C AS DateTime), CAST(0x0000A93000C8C670 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (60, N'daisy.k@cscjackpot.com', N'/email=daisy.k@cscjackpot.com&&key=9ac3b3a9dbb2cbb8f24e150153863129', 0, 1, CAST(0x0000A93000C56342 AS DateTime), CAST(0x0000A93000C95310 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (61, N'toan8181@gmail.com', N'/email=toan8181@gmail.com&&key=865b6efaf3016ca1441133470cc4e3c3', 0, 1, CAST(0x0000A9300100EAE7 AS DateTime), CAST(0x0000A9300104ECE0 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (62, N'hathanhpc20@gmail.com', N'/email=hathanhpc20@gmail.com&&key=49c5f024fc9462679ac0d8bd8c8267fb', 1, 1, CAST(0x0000A93001294437 AS DateTime), CAST(0x0000A93201292E20 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (63, N'hathanhpc20@gmail.com', N'/email=hathanhpc20@gmail.com&&key=1423f9fd9630b3a8c2139b15aeb96840', 0, 1, CAST(0x0000A93001295C9A AS DateTime), CAST(0x0000A93201292E20 AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[LinkResetPassword] OFF
/****** Object:  Table [dbo].[IPConfig]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IPConfig](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](50) NULL,
	[ServiceDomain] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_IPConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[IPConfig] ON
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (1, N'127.0.0.1', N'http://localhost:61923', 1, 0, CAST(0x0000A8D700000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (2, N'::1', N'http://localhost:61950/CommonSvc.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (3, N'::1', N'http://services.lotte.bigbrothers.gold/CommonSvc.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (4, N'127.0.0.1', N'http://services.lotte.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (5, N'::1', N'http://localhost:61923/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (6, N'::1', N'http://services.lotte.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (7, N'::1', N'http://localhost:61923/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (8, N'::1', N'http://lotte.services.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (9, N'127.0.0.1', N'http://lotte.services.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (10, N'::1', N'http://services.lotterycarcoin.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (11, N'14.161.24.184', N'http://services.lotterycarcoin.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (12, N'14.161.24.184', N'http://servicecmslotteclp.bigbrothers.gold/CommonSvc.svc', 1, 0, CAST(0x0000A8EB00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (13, N'127.0.0.1', N'http://localhost:61950/CommonSvc.svc', 1, 0, CAST(0x0000A8D700000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (14, N'192.168.1.7', N'http://servicecmslotteclp.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (15, N'134.119.181.122', N'http://services.cscjackpot.com/CommonServices.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (16, N'134.119.181.122', N'http://cmsservices.cscjackpot.com/CommonSvc.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (17, N'127.0.0.1', N'http://services.cscjackpot.com/CommonServices.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (18, N'134.119.181.122', N'http://noterrorservices.cscjackpot.com/CommonServices.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'kienduc', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (19, N'134.119.181.122', N'http://noterrorcmsservices.cscjackpot.com/CommonSvc.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'kienduc', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (21, N'192.168.1.30', N'http://noterrorcmsservices.cscjackpot.com/CommonSvc.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'kienduc', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[IPConfig] OFF
/****** Object:  Table [dbo].[GroupAdmin]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupAdmin](
	[GroupID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
 CONSTRAINT [PK_GroupAdmin] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[GroupAdmin] ON
INSERT [dbo].[GroupAdmin] ([GroupID], [GroupName], [IsActive]) VALUES (1, N'administrator', 1)
INSERT [dbo].[GroupAdmin] ([GroupID], [GroupName], [IsActive]) VALUES (2, N'accounting', 1)
INSERT [dbo].[GroupAdmin] ([GroupID], [GroupName], [IsActive]) VALUES (3, N'staff', 1)
SET IDENTITY_INSERT [dbo].[GroupAdmin] OFF
/****** Object:  Table [dbo].[Award]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Award](
	[AwardID] [int] IDENTITY(1,1) NOT NULL,
	[AwardName] [nvarchar](250) NULL,
	[AwardValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[Priority] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[AwardNameEnglish] [nvarchar](250) NULL,
	[NumberBallGold] [int] NULL,
	[NumberBallNormal] [int] NULL,
	[AwardFee] [decimal](18, 8) NULL,
	[AwardPercent] [float] NULL,
 CONSTRAINT [PK_Award] PRIMARY KEY CLUSTERED 
(
	[AwardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Award] ON
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (1, N'Giải đặc biệt', 10, CAST(0x0000A79C00000000 AS DateTime), 1, 1, 0, N'Jackpot', 1, 5, CAST(0.50000000 AS Decimal(18, 8)), 5)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (2, N'16.0000', 4, CAST(0x0000A8E100000000 AS DateTime), 2, 1, 0, N'First pzire', 0, 5, CAST(0.16000000 AS Decimal(18, 8)), 4)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (3, N'4.0000', 0.5, CAST(0x0000A8E20047E230 AS DateTime), 2, 1, 0, N'Second prize', 1, 4, CAST(0.01500000 AS Decimal(18, 8)), 3)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (4, N'0.08', 0.02, CAST(0x0000A8E20047FD86 AS DateTime), 2, 1, 0, N'Third prize', 0, 4, CAST(0.00040000 AS Decimal(18, 8)), 2)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (5, N'0.02', 0.006, CAST(0x0000A8E200483840 AS DateTime), 2, 1, 0, N'Fourth prize', 1, 3, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (6, N'0.008', 0.001, CAST(0x0000A8E200484873 AS DateTime), 2, 1, 0, N'Fifth prize	', 0, 3, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (7, N'0.004', 0.0008, CAST(0x0000A8E200485DB6 AS DateTime), 2, 1, 0, N'Sixth prize', 1, 2, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (8, N'0.002', 0.0005, CAST(0x0000A8E200486EEC AS DateTime), 2, 1, 0, N'Seventh prize', 1, 1, CAST(0.00000000 AS Decimal(18, 8)), 0)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (9, N'0.0012', 0.0004, CAST(0x0000A8E2004909D6 AS DateTime), 2, 1, 0, N'Eighth prize', 1, 0, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (10, N'0.0004', 0.0002, CAST(0x0000A8E200491E16 AS DateTime), 2, 1, 0, N'Ninth prize', 0, 2, CAST(0.00000000 AS Decimal(18, 8)), 0)
SET IDENTITY_INSERT [dbo].[Award] OFF
/****** Object:  Table [dbo].[AuthenticateLink]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticateLink](
	[AuthenticateID] [int] IDENTITY(1,1) NOT NULL,
	[LinkAuthenticate] [nvarchar](1000) NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Note] [nvarchar](3500) NULL,
	[CreateUser] [datetime] NULL,
 CONSTRAINT [PK_AuthenticateLink] PRIMARY KEY CLUSTERED 
(
	[AuthenticateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AwardNumber]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardNumber](
	[NumberID] [int] IDENTITY(1,1) NOT NULL,
	[AwardID] [int] NULL,
	[NumberValue] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[Priority] [int] NULL,
	[StationName] [nvarchar](250) NULL,
	[StationNameEnglish] [nvarchar](250) NULL,
	[Gencode] [nvarchar](500) NULL,
 CONSTRAINT [PK_AwardNumber] PRIMARY KEY CLUSTERED 
(
	[NumberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AwardNumber] ON
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (1, 5, N'23790', CAST(0x0000A79F011F5480 AS DateTime), 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (2, 4, N'51783', CAST(0x0000A7AC011F5DD3 AS DateTime), 1, 0, 1, N'Long An', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (3, 3, N'63927', CAST(0x0000A7AD011F6817 AS DateTime), 1, 0, 1, N'An Giang', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (4, 2, N'84374', CAST(0x0000A7AE011F7187 AS DateTime), 1, 0, 1, N'Tiền Giang', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (5, 1, N'25176', CAST(0x0000A7AF011F86EA AS DateTime), 1, 0, 1, N'Vĩnh Long', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (6, 1, N'23324', CAST(0x0000A7B0009454D6 AS DateTime), 1, 0, 1, N'Tp Hồ Chí Minh', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (9, 1, N'35790', CAST(0x0000A7B9009454D6 AS DateTime), 1, 0, 1, N'Long An', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (10, 1, N'32432', CAST(0x0000A7BA0099671D AS DateTime), 1, 0, 1, N'Hồ Chí Minh', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (18, 1, N'54870', CAST(0x0000A7CE0099671D AS DateTime), 1, 0, 1, N'Hồ Chí Minh', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (19, 1, N'46456', CAST(0x0000A7D0011D5643 AS DateTime), 1, 0, 1, N'Vĩnh Long', N'VL Television', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (20, 1, N'03273', CAST(0x0000A7D100BC1690 AS DateTime), 1, 0, 1, N'Tiền Giang', N'TG Television', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (21, 1, N'99999', CAST(0x0000A7D801112083 AS DateTime), 1, 0, 1, N'TP.HCM', N'TP.HCM', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (22, 2, N'88289', CAST(0x0000A7D801114F57 AS DateTime), 1, 0, 1, N'Bình Dương', N'Bình Dương', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (23, 2, N'87489', CAST(0x0000A7DC01114F57 AS DateTime), 1, 0, 1, N'Bình Dương', N'Bình Dương', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (24, 1, N'43545', CAST(0x0000A7DD011BCA1A AS DateTime), 1, 0, 1, N'Hà Nội', N'Ha Noi', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (25, 6, N'12345', CAST(0x0000A7F300B4DE00 AS DateTime), 1, 0, 1, N'Ha Noi', N'Ha Noi', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (26, 6, N'34544', CAST(0x0000A7F500E460A5 AS DateTime), 1, 0, 1, N'num1', N'num12', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (27, 2, N'23', CAST(0x0000A8120099B084 AS DateTime), 1, 0, 1, N'giai nhat', N'number one', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (28, 1, N'3.82212e+011', CAST(0x0000A86200BBACCE AS DateTime), 1, 0, 1, N'', N'', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (29, 9, N'1.21212e+011', CAST(0x0000A86200C76AB1 AS DateTime), 1, 0, 1, N'', N'', NULL)
SET IDENTITY_INSERT [dbo].[AwardNumber] OFF
/****** Object:  Table [dbo].[AwardMegaballLog]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AwardMegaballLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[Value] [decimal](18, 5) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AwardMegaballLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ticket Number Of Times' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AwardMegaballLog', @level2type=N'COLUMN',@level2name=N'Value'
GO
SET IDENTITY_INSERT [dbo].[AwardMegaballLog] ON
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (0, N'hathanhpc20@gmail.com', CAST(10.01506 AS Decimal(18, 5)), CAST(0x0000A92D00A062AC AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (1, N'hathanhpc20@gmail.com', CAST(10.01506 AS Decimal(18, 5)), CAST(0x0000A92D00A062AC AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (2, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A062AC AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (3, N'hathanhpc20@gmail.com', CAST(10.01110 AS Decimal(18, 5)), CAST(0x0000A92D00A1AC7B AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (4, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A1AC7B AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (5, N'hathanhpc20@gmail.com', CAST(10.02354 AS Decimal(18, 5)), CAST(0x0000A92D00A3B0EA AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (6, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A3B0EA AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (7, N'hathanhpc20@gmail.com', CAST(10.01110 AS Decimal(18, 5)), CAST(0x0000A92D00A537CE AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (8, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A537CE AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (9, N'hathanhpc20@gmail.com', CAST(10.01856 AS Decimal(18, 5)), CAST(0x0000A92D00A91E5F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (10, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A91E5F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (11, N'hathanhpc20@gmail.com', CAST(10.02354 AS Decimal(18, 5)), CAST(0x0000A92D00AB0832 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (12, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00AB0832 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (13, N'daisy.k@cscjackpot.com', CAST(0.00700 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (14, N'hathanhpc20@gmail.com', CAST(0.00040 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (15, N'nguyenlytruongson@gmail.com', CAST(0.01080 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (16, N'thanhha12196@gmail.com', CAST(0.00620 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (17, N'hathanhpc20@gmail.com', CAST(0.00850 AS Decimal(18, 5)), CAST(0x0000A92D018A7C40 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (18, N'thanhha12196@gmail.com', CAST(0.00580 AS Decimal(18, 5)), CAST(0x0000A92D018A7C40 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (19, N'hathanhpc20@gmail.com', CAST(10.01662 AS Decimal(18, 5)), CAST(0x0000A92E00038A08 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (20, N'thanhha12196@gmail.com', CAST(0.00580 AS Decimal(18, 5)), CAST(0x0000A92E00038A08 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (21, N'hathanhpc20@gmail.com', CAST(3.34455 AS Decimal(18, 5)), CAST(0x0000A92E0008DDA1 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (22, N'thanhha12196@gmail.com', CAST(6.67791 AS Decimal(18, 5)), CAST(0x0000A92E0008DDA1 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (23, N'hathanhpc20@gmail.com', CAST(3.34455 AS Decimal(18, 5)), CAST(0x0000A92E0032DBDF AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (24, N'thanhha12196@gmail.com', CAST(6.67791 AS Decimal(18, 5)), CAST(0x0000A92E0032DBDF AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (25, N'hathanhpc20@gmail.com', CAST(3.34455 AS Decimal(18, 5)), CAST(0x0000A92E00363576 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (26, N'thanhha12196@gmail.com', CAST(6.67791 AS Decimal(18, 5)), CAST(0x0000A92E00363576 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (27, N'hathanhpc20@gmail.com', CAST(3.34455 AS Decimal(18, 5)), CAST(0x0000A92E0036D52D AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (28, N'thanhha12196@gmail.com', CAST(6.67791 AS Decimal(18, 5)), CAST(0x0000A92E0036D52D AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (29, N'hathanhpc20@gmail.com', CAST(0.00420 AS Decimal(18, 5)), CAST(0x0000A92E003C67A4 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (30, N'thanhha12196@gmail.com', CAST(0.02790 AS Decimal(18, 5)), CAST(0x0000A92E003C67A4 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (31, N'aurora.d@cscjackpot.com', CAST(0.00900 AS Decimal(18, 5)), CAST(0x0000A92E005FCDD7 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (32, N'daisy.k@cscjackpot.com', CAST(0.07750 AS Decimal(18, 5)), CAST(0x0000A92E005FCDD7 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (33, N'thanh.ha@bigbrothers.gold', CAST(0.07160 AS Decimal(18, 5)), CAST(0x0000A92E005FCDD7 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (34, N'duc.ly@bigbrothers.gold', CAST(0.04900 AS Decimal(18, 5)), CAST(0x0000A92E0079AEC3 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (35, N'hathanhpc20@gmail.com', CAST(0.07390 AS Decimal(18, 5)), CAST(0x0000A92E0079AEC3 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (36, N'thanh.ha@bigbrothers.gold', CAST(0.02290 AS Decimal(18, 5)), CAST(0x0000A92E0079AEC3 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (37, N'buitanphat1991@gmail.com', CAST(0.00070 AS Decimal(18, 5)), CAST(0x0000A92F00509E9D AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (38, N'duc.ly@bigbrothers.gold', CAST(0.07230 AS Decimal(18, 5)), CAST(0x0000A92F00509E9D AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (39, N'hathanhpc20@gmail.com', CAST(0.02530 AS Decimal(18, 5)), CAST(0x0000A92F00509E9D AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (40, N'thanh.ha@bigbrothers.gold', CAST(0.03090 AS Decimal(18, 5)), CAST(0x0000A92F00509E9D AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (41, N'duc.ly@bigbrothers.gold', CAST(0.03310 AS Decimal(18, 5)), CAST(0x0000A92F0051CDEB AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (42, N'duc.ly@bigbrothers.gold', CAST(0.01180 AS Decimal(18, 5)), CAST(0x0000A92F00594A60 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (43, N'duc.ly@bigbrothers.gold', CAST(0.01120 AS Decimal(18, 5)), CAST(0x0000A92F007C6E53 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (44, N'k.duchappy@gmail.com', CAST(0.02940 AS Decimal(18, 5)), CAST(0x0000A92F007C6E53 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (45, N'duc.ly@bigbrothers.gold', CAST(0.00510 AS Decimal(18, 5)), CAST(0x0000A92F017F3836 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (46, N'k.duchappy@gmail.com', CAST(0.01300 AS Decimal(18, 5)), CAST(0x0000A92F017F3836 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (47, N'thanh.ha@bigbrothers.gold', CAST(0.00790 AS Decimal(18, 5)), CAST(0x0000A92F017F3836 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (48, N'buitanphat1991@gmail.com', CAST(0.00160 AS Decimal(18, 5)), CAST(0x0000A93000335CDB AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (49, N'carcoin123@gmail.com', CAST(0.02260 AS Decimal(18, 5)), CAST(0x0000A93000335CDB AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (50, N'hathanhpc20@gmail.com', CAST(0.01690 AS Decimal(18, 5)), CAST(0x0000A93000335CDB AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (51, N'carcoin123@gmail.com', CAST(0.10800 AS Decimal(18, 5)), CAST(0x0000A93000453A1E AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (52, N'carcoin12345@gmail.com', CAST(0.12240 AS Decimal(18, 5)), CAST(0x0000A93000453A1E AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (53, N'carcoin123@gmail.com', CAST(0.10800 AS Decimal(18, 5)), CAST(0x0000A930004D520C AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (54, N'carcoin12345@gmail.com', CAST(0.12240 AS Decimal(18, 5)), CAST(0x0000A930004D520C AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (55, N'carcoin123@gmail.com', CAST(0.10320 AS Decimal(18, 5)), CAST(0x0000A930005226F6 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (56, N'carcoin12345@gmail.com', CAST(0.12250 AS Decimal(18, 5)), CAST(0x0000A930005226F6 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (57, N'carcoin123@gmail.com', CAST(10.26624 AS Decimal(18, 5)), CAST(0x0000A93000566321 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (58, N'carcoin12345@gmail.com', CAST(0.12250 AS Decimal(18, 5)), CAST(0x0000A93000566321 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (59, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A930005A4ABD AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (60, N'carcoin123@gmail.com', CAST(10.30282 AS Decimal(18, 5)), CAST(0x0000A930005A4ABD AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (61, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A930005A4ABD AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (62, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A930005DC24E AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (63, N'carcoin123@gmail.com', CAST(10.30282 AS Decimal(18, 5)), CAST(0x0000A930005DC24E AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (64, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A930005DC24E AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (65, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A9300060FC2F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (66, N'carcoin123@gmail.com', CAST(10.30282 AS Decimal(18, 5)), CAST(0x0000A9300060FC2F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (67, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A9300060FC2F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (68, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A93000621D66 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (69, N'carcoin123@gmail.com', CAST(10.30282 AS Decimal(18, 5)), CAST(0x0000A93000621D66 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (70, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A93000621D66 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (71, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A93000682999 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (72, N'carcoin123@gmail.com', CAST(10.30282 AS Decimal(18, 5)), CAST(0x0000A93000682999 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (73, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A93000682999 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (74, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A93000689186 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (75, N'carcoin123@gmail.com', CAST(10.30282 AS Decimal(18, 5)), CAST(0x0000A93000689186 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (76, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A93000689186 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (77, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A930006B645A AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (78, N'carcoin123@gmail.com', CAST(10.30282 AS Decimal(18, 5)), CAST(0x0000A930006B645A AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (79, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A930006B645A AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (80, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A930006C7B9F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (81, N'carcoin123@gmail.com', CAST(10.13770 AS Decimal(18, 5)), CAST(0x0000A930006C7B9F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (82, N'carcoin12345@gmail.com', CAST(0.14590 AS Decimal(18, 5)), CAST(0x0000A930006C7B9F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (83, N'buitanphat1991@gmail.com', CAST(0.00240 AS Decimal(18, 5)), CAST(0x0000A930006DDF49 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (84, N'carcoin123@gmail.com', CAST(10.30674 AS Decimal(18, 5)), CAST(0x0000A930006DDF49 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (85, N'carcoin12345@gmail.com', CAST(0.15150 AS Decimal(18, 5)), CAST(0x0000A930006DDF49 AS DateTime))
SET IDENTITY_INSERT [dbo].[AwardMegaballLog] OFF
/****** Object:  Table [dbo].[AwardMegaball]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AwardMegaball](
	[NumberID] [int] IDENTITY(1,1) NOT NULL,
	[AwardID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[FirstNumber] [int] NULL,
	[SecondNumber] [int] NULL,
	[ThirdNumber] [int] NULL,
	[FourthNumber] [int] NULL,
	[FivethNumber] [int] NULL,
	[ExtraNumber] [int] NULL,
	[Priority] [int] NULL,
	[GenCode] [nvarchar](500) NULL,
	[JackPot] [float] NULL,
	[JackPotWinner] [float] NULL,
	[DrawID] [int] NULL,
	[TotalWon] [decimal](18, 5) NULL,
 CONSTRAINT [PK_AwardMegaball] PRIMARY KEY CLUSTERED 
(
	[NumberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total JackPot previous won' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AwardMegaball', @level2type=N'COLUMN',@level2name=N'JackPotWinner'
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](250) NULL,
	[Hashkey] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[FullName] [nvarchar](250) NULL,
	[Email] [nvarchar](250) NULL,
	[Mobile] [char](50) NULL,
	[Address] [nvarchar](250) NULL,
	[Avatar] [nvarchar](250) NULL,
	[IsDelete] [smallint] NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Admin] ON
INSERT [dbo].[Admin] ([AdminID], [UserName], [Password], [Hashkey], [IsActive], [CreateDate], [FullName], [Email], [Mobile], [Address], [Avatar], [IsDelete]) VALUES (1, N'administrator', N'78a8495d6940fc1207e964e88adbe90c', N'1234', 1, CAST(0x0000A79C00000000 AS DateTime), N'Admin', N'admin@gmail.com', N'34346456                                          ', N'Bình Thạnh Hồ Chí Minh', NULL, 0)
INSERT [dbo].[Admin] ([AdminID], [UserName], [Password], [Hashkey], [IsActive], [CreateDate], [FullName], [Email], [Mobile], [Address], [Avatar], [IsDelete]) VALUES (2, N'accounting', N'8d421e892a47dff539f46142eb09e56b', N'1234', 1, CAST(0x0000A8EB010406FC AS DateTime), N'Kế toán', N'accounting@gmail.com', N'3462456757                                        ', N'Hồ Chí Minh', N'', 0)
SET IDENTITY_INSERT [dbo].[Admin] OFF
/****** Object:  Table [dbo].[AccessRight]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessRight](
	[GroupID] [int] NOT NULL,
	[AdminID] [int] NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[AccessRight] ([GroupID], [AdminID]) VALUES (1, 1)
INSERT [dbo].[AccessRight] ([GroupID], [AdminID]) VALUES (2, 2)
/****** Object:  Table [dbo].[Booking]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Booking](
	[BookingID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[NumberValue] [varchar](50) NULL,
	[Quantity] [int] NULL,
	[CreateDate] [datetime] NULL,
	[OpenDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
 CONSTRAINT [PK_Booking] PRIMARY KEY CLUSTERED 
(
	[BookingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AwardWithdraw]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardWithdraw](
	[TransactionID] [char](50) NOT NULL,
	[RequestMemberID] [nvarchar](250) NULL,
	[RequestWalletAddress] [nvarchar](250) NULL,
	[RequestDate] [datetime] NULL,
	[RequestStatus] [smallint] NULL,
	[AwardDate] [datetime] NULL,
	[CoinIDWithdraw] [char](10) NULL,
	[ValuesWithdraw] [numeric](18, 8) NULL,
	[AdminIDApprove] [nvarchar](250) NULL,
	[ApproveDate] [datetime] NULL,
	[ApproveNote] [nvarchar](2000) NULL,
	[IsDeleted] [smallint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
	[BookingID] [int] NULL,
 CONSTRAINT [PK_AwardWithdraw] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BookingMegaball]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BookingMegaball](
	[BookingID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[OpenDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
	[FirstNumber] [int] NULL,
	[SecondNumber] [int] NULL,
	[ThirdNumber] [int] NULL,
	[FourthNumber] [int] NULL,
	[FivethNumber] [int] NULL,
	[ExtraNumber] [int] NULL,
	[Quantity] [int] NULL,
	[ValuePoint] [numeric](18, 10) NULL,
	[DrawID] [int] NULL,
	[IsFreeTicket] [smallint] NULL,
	[IsRevenueFreeTicket] [smallint] NULL,
 CONSTRAINT [PK_BookingMegaball] PRIMARY KEY CLUSTERED 
(
	[BookingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PackageCoin]    Script Date: 08/02/2018 19:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageCoin](
	[PackageID] [int] NOT NULL,
	[CoinID] [nchar](10) NOT NULL,
	[PackageValue] [numeric](18, 2) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Package_Sel_All]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Package_Sel_All]   
AS   
    SET NOCOUNT ON;  
    SELECT * 
    FROM Package  
    WHERE IsActive = 1 
    order by Priority asc ;
GO
/****** Object:  StoredProcedure [dbo].[Package_Insert]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Package_Insert]   
    @PackageName nvarchar(50) = null,
    @PackageValue float,
    @CreateDate datetime,
    @IsActive int,
    @Priority int,
    @NumberExpire int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Package(PackageName,PackageValue,CreateDate,IsActive,Priority,NumberExpire) values(@PackageName,@PackageValue,@CreateDate,@IsActive,@Priority,@NumberExpire) 
END
GO
/****** Object:  StoredProcedure [dbo].[Booking_Sel]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Booking_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select CreateDate, BookingID, MemberID, NumberValue, OpenDate, Quantity, Status,TransactionCode from Booking where MemberID = @MemberID order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[Booking_Insert]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Booking_Insert]   
    @MemberID int,
    @NumberValue nvarchar(50) = null,
    @Quantity int,
    @Status int,
    @OpenDate datetime,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Booking(MemberID,NumberValue,Quantity,Status,OpenDate,CreateDate,TransactionCode) values(@MemberID,@NumberValue,@Quantity,@Status,@OpenDate, @CreateDate, @TransactionCode) 
    update Member set Points = ((select Points from Member where MemberID = @MemberID) - @Quantity) where MemberID = @MemberID
END
GO
/****** Object:  StoredProcedure [dbo].[AwardNumber_Sel]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AwardNumber_Sel]   
    @fromDate datetime,
    @toDate datetime
AS
BEGIN 
     SET NOCOUNT ON 
		SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from awardnumber awn left join award aw on awn.AwardID=aw.AwardID where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0
END
GO
/****** Object:  StoredProcedure [dbo].[AwardNumber_Insert]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AwardNumber_Insert]   
    @AwardID int,
    @CreateDate datetime,
    @IsActive int,
    @IsDelete int,
    @NumberValue nvarchar(50) = null,
    @Priority int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into AwardNumber(AwardID,CreateDate,IsActive,IsDelete,NumberValue,Priority) values(@AwardID,@CreateDate,@IsActive,@IsDelete,@NumberValue,@Priority) 
END
GO
/****** Object:  StoredProcedure [dbo].[Award_Insert]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Award_Insert]   
    @AwardName nvarchar(50) = null,
    @AwardValue float,
    @IsActive int,
    @IsDelete int,
    @Priority int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Award(AwardName,AwardValue,IsActive,IsDelete,Priority) values(@AwardName,@AwardValue,@IsActive,@IsDelete,@Priority) 
    
END
GO
/****** Object:  StoredProcedure [dbo].[Admin_Sel]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Admin_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Admin  
    WHERE UserName = @UserName AND Password = @PassWord  ;
GO
/****** Object:  UserDefinedFunction [dbo].[GetAwardWithdrawRequestStatus]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[GetAwardWithdrawRequestStatus]
(
	@MemberID nvarchar(250),
	@BookingID int,
	@AwardDate datetime
)
returns int
as
begin
declare @status int;
set @status = (select top 1 RequestStatus from AwardWithdraw
where convert(varchar(10), AwardDate, 101)=convert(varchar(10), @AwardDate, 101)
and RequestMemberID=@MemberID
and IsDeleted=0
and BookingID=@BookingID);
if @status is null 
begin
 set @status=-1
 end;
return @status;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckUserNameAdmin]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckUserNameAdmin]
   (
	@userName nvarchar(50)
 )
 as begin 
		select UserName from Admin where UserName=@userName
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckPhoneZipCodeExists_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckPhoneZipCodeExists_CMS]
 (@phoneZipCode char(10),@countryID int)
 as begin
	select PhoneZipCode from Country where PhoneZipCode=@phoneZipCode and IsDeleted=0 and IsActive=1 and CountryID!=@countryID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckOpenDateAwardMegaball]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckOpenDateAwardMegaball]
  (
	@createDate datetime

  )
  as begin 
	select CreateDate from AwardMegaball where CreateDate=@createDate  
  end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckNumberExit]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckNumberExit]
 (
	@num1 int,
	@num2 int,
	@num3 int,
	@num4 int,
	@num5 int,
	@extranum int
 )
 as begin
	select FirstNumber, SecondNumber, ThirdNumber, FourthNumber, FivethNumber, ExtraNumber from AwardMegaball
	where FirstNumber=@num1 or SecondNumber=@num2 or ThirdNumber=@num3 or FourthNumber=@num4 or FivethNumber=@num5 or ExtraNumber=@extranum
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckMobileMemberExist]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckMobileMemberExist](@mobile char(15), @email nvarchar(250))
 as begin 
	 select Mobile from Member where Mobile=@mobile and Email=@email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckMobileExist]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckMobileExist](@mobile char(15))
as begin 
	select Mobile from Member where Mobile=@mobile
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckJackPotWinner_Draw_Sel]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckJackPotWinner_Draw_Sel]
 (
	@DrawID int,
	@FirstNumber int,
	@SecondNumber int,
	@ThirdNumber int,
	@FourthNumber int, 
	@FivethNumber int,
	@ExtraNumber int
 )
 as begin 
		
	Select Sum(1) As JackPotWinner
	From (
			Select BookingMegaball.MemberID,
					BookingMegaball.BookingID,
					BookingMegaball.DrawID,
					BookingMegaball.CreateDate,
					BookingMegaball.FirstNumber,
					BookingMegaball.SecondNumber,
					BookingMegaball.ThirdNumber,
					BookingMegaball.FourthNumber,
					BookingMegaball.FivethNumber,
					BookingMegaball.ExtraNumber,
					Case
						When BookingMegaball.FirstNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End + 
					Case
						When BookingMegaball.SecondNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End +
					Case
						When BookingMegaball.ThirdNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End +
					Case
						When BookingMegaball.FourthNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End +
					Case
						When BookingMegaball.FivethNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End As NumNormal,
					Case
						When BookingMegaball.ExtraNumber = @ExtraNumber Then 1
						Else 0
					End as NumGold
			From BookingMegaball 
			Where BookingMegaball.DrawID = @DrawID
		) BM 
	join Award A on BM.NumNormal = A.NumberBallNormal AND BM.NumGold = A.NumberBallGold
	Where A.Priority = 1;

 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckIPConfig_FE]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_CheckIPConfig_FE]
(
	@IPAddress nvarchar(50),
	@ServiceDomain nvarchar(250),
	@UserName nvarchar(250)
	
)
as
begin
select * from IPConfig ip, Member m  
where 
--ip.IPAddress=@IPAddress
--and 
ip.ServiceDomain=@ServiceDomain
and ip.IsActive=1
and ip.IsDeleted=0
and m.Email=@UserName 

and m.IsActive=1 
and m.IsDelete=0;

end;
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckIPConfig_CMS]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_CheckIPConfig_CMS]
(
	@IPAddress nvarchar(50),
	@ServiceDomain nvarchar(250),
	@UserName nvarchar(250)
	
)
as
begin
select * from IPConfig ip, [Admin] m  
where 
--ip.IPAddress=@IPAddress
--and 
ip.ServiceDomain=@ServiceDomain
and ip.IsActive=1
and ip.IsDeleted=0
and m.UserName=@UserName 

and m.IsActive=1 
and m.IsDelete=0;

end;
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckGroupAdminName]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckGroupAdminName] 
 (
	@groupName  nvarchar(250)
 )
 as begin 
	select GroupName from GroupAdmin where GroupName = @groupName 
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckExpirePackageFE]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckExpirePackageFE]
(
@memberID int,
@dateNow datetime
)
as begin 
	select * from TransactionPackage p where MemberID=@memberID and Status=1 and p.ExpireDate>=@dateNow
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckExistTransactionBitcoinFE]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckExistTransactionBitcoinFE]
(
	@TransactionBitcoin nvarchar(500),
	@MemberID int
)
as begin
	select * 
	from TransactionCoin 
	where TransactionBitcoin = @TransactionBitcoin
	      and MemberID = @MemberID;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckEmailExistsFE]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckEmailExistsFE]
(
@email nvarchar(250)
)
as begin 
	select Email from member where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckEmailAdmin]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckEmailAdmin]
  (
	@email nvarchar(50)
 )
 as begin 
		select Email from Admin where Email=@email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckE_WalletExistsFE]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckE_WalletExistsFE]
	(
@e_wallet nvarchar(250)
)
as begin 
	select E_Wallet from member where E_Wallet=@e_wallet
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckCountryNameExits_CMS]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckCountryNameExits_CMS]
 (@countryName nvarchar(250))
 as begin
	select CountryName from Country where CountryName=@countryName  and IsDeleted=0 and IsActive=1
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckConfigName]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckConfigName]
  (
	@configName nvarchar(250),
	@configID int
 )
 as begin 
		select ConfigName 
		from TicketConfig 
		where ConfigName=@configName 
		and (@configID=0 or ConfigID!=@configID)
 end
GO
/****** Object:  StoredProcedure [guest].[SP_CheckCoinIDExist]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_CheckCoinIDExist]
(
	@coinID nchar(10),
	@exchangeID int
)
as begin 
	select * from ExchangeTicket where CoinID=@coinID and (@exchangeID=0 or ExchangeID!=@exchangeID) and IsActive =1 and IsDelete =0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckCoinIDExist]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_CheckCoinIDExist]
(
	@coinID nchar(10),
	@exchangeID int
)
as begin 
	select * from ExchangeTicket where CoinID=@coinID and (@exchangeID=0 or ExchangeID!=@exchangeID) and IsActive =1 and IsDelete =0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckCoinID]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckCoinID]
 (
	@coinID nvarchar(250),
	@configID int
 )
 as begin 
		select CoinID 
		from TicketConfig 
		where CoinID=@coinID and IsActive=1
		and (@configID=0 or ConfigID!=@configID)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckAwardNameEnglish]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckAwardNameEnglish]
 (
	@awardNameEnglish nvarchar(250),
	@AwardID int
 )
 as begin 
		select * 
		from Award 
		where AwardNameEnglish=@awardNameEnglish and IsActive=1 and IsDelete=0 and AwardID!=@AwardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_BookingMegaball_AwardValue_Sel]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_BookingMegaball_AwardValue_Sel]
 (
	@DrawID int,
	@FirstNumber int,
	@SecondNumber int,
	@ThirdNumber int,
	@FourthNumber int, 
	@FivethNumber int,
	@ExtraNumber int,
	@JackPotValue decimal(18,10)
 )
 as begin 
		Declare @RowCount int = 0;
		Declare @IsExists int = 0;
		Begin
			Select @IsExists = 1
			From TicketWinning
			Where TicketWinning.DrawID = @DrawID;
		End

		If(@IsExists = 0) 
		Begin
			Update Award
			Set Award.AwardValue = @JackPotValue,
				Award.AwardFee = ((@JackPotValue * Award.AwardPercent)/100)
			Where Exists(
						  Select 1
						  From AwardMegaball
						  Where AwardMegaball.DrawID = @DrawID
								And Award.AwardID = AwardMegaball.AwardID);

			if(@@ROWCOUNT > 0)
			BEGIN
				Update AwardMegaball
				Set AwardMegaball.JackPot = @JackPotValue
				Where AwardMegaball.DrawID = @DrawID;

				if(@@ROWCOUNT > 0)
				Begin
					Set @RowCount = 1;
				End;
			End;

			if(@RowCount > 0)
			BEGIN
				--update success

				Select BM.MemberID,
					   BM.BookingID,
					   BM.DrawID,
					   BM.CreateDate BuyDate,
					   M.Email,
					   A.AwardValue,
					   A.AwardID,
					   A.NumberBallNormal,
					   A.NumberBallGold,
					   A.Priority,
					   A.AwardFee,
					   BM.FirstNumber FirstNumberBuy,
					   BM.SecondNumber SecondNumberBuy,
					   BM.ThirdNumber ThirdNumberBuy,
					   BM.FourthNumber FourthNumberBuy,
					   BM.FivethNumber FivethNumberBuy,
					   BM.ExtraNumber ExtraNumberBuy
				From (
						Select BookingMegaball.MemberID,
							   BookingMegaball.BookingID,
							   BookingMegaball.DrawID,
							   BookingMegaball.CreateDate,
							   BookingMegaball.FirstNumber,
							   BookingMegaball.SecondNumber,
							   BookingMegaball.ThirdNumber,
							   BookingMegaball.FourthNumber,
							   BookingMegaball.FivethNumber,
							   BookingMegaball.ExtraNumber,
							   Case
								  When BookingMegaball.FirstNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End + 
							   Case
								  When BookingMegaball.SecondNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End +
							   Case
								  When BookingMegaball.ThirdNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End +
							   Case
								  When BookingMegaball.FourthNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End +
							   Case
								  When BookingMegaball.FivethNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End As NumNormal,
							   Case
								  When BookingMegaball.ExtraNumber = @ExtraNumber Then 1
								  Else 0
							   End as NumGold
						From BookingMegaball 
						Where BookingMegaball.DrawID = @DrawID
					) BM 
				join Award A on BM.NumNormal = A.NumberBallNormal AND BM.NumGold = A.NumberBallGold
				join Member M On M.MemberID = BM.MemberID;

			End;
		End;
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AwardMegaballLog_Add]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_AwardMegaballLog_Add]
 (
	@Email nvarchar(250),
	@Value decimal(18, 5),
	@CreatedDate datetime,
	@Id int out
 )
 as begin 
		Insert into AwardMegaballLog(Email, Value, CreatedDate)
		Values (@Email, @Value, @CreatedDate);

		select @Id = Scope_Identity();
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AwardMegaball_Upd]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_AwardMegaball_Upd]
 (
	@draw int,
	@totalwon decimal(18,5)
 )
 as begin
	update AwardMegaball 
	set TotalWon = @totalwon
	where NumberID=(select top 1 AwardMegaball.NumberID from AwardMegaball where DrawID = @draw)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AWARDMEGABALL_SELRESULT]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_AWARDMEGABALL_SELRESULT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 15.01.2018
	DESCRIPTION: LOAD THÔNG TIN KẾT QUẢ XỔ SỐ GẦN NHẤT
*/
AS BEGIN 
	SELECT AWN.*,
		   AW.AWARDNAME,
		   AW.AWARDVALUE 
	FROM AWARDMEGABALL AWN 
	LEFT JOIN AWARD AW 
			ON AWN.AWARDID = AW.AWARDID 
	WHERE AWN.ISDELETE = 0
	      --AND AWN.ISACTIVE = 1
    ORDER BY AWN.CreateDate DESC;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Award_JackPotValue_Upd]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_Award_JackPotValue_Upd]
 (
	@JackPotValue decimal(18, 8),
	@DrawID int
 )
 as begin 
		Update Award
		Set Award.AwardValue = @JackPotValue,
		    Award.AwardFee = ((@JackPotValue * Award.AwardPercent)/100)
		Where Exists(
					  Select 1
					  From AwardMegaball
					  Where AwardMegaball.DrawID = @DrawID
					        And Award.AwardID = AwardMegaball.AwardID);

		--if(@@ROWCOUNT > 0)
		--BEGIN
		--	Update AwardMegaball
		--	Set AwardMegaball.JackPot = @JackPotValue
		--	Where AwardMegaball.DrawID = @DrawID;
		--End;
 end
GO
/****** Object:  StoredProcedure [dbo].[Member_Update]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_Update] 
	@MemberID int,  
    @Email nvarchar(50) = null,
    @E_Wallet nvarchar(50) = null,
    @Password nvarchar(50) = null,
    @IsActive int,
    @IsDelete int,
    @Points float
AS 
BEGIN 
     SET NOCOUNT ON 
    update Member
	set Email = @Email,
	E_Wallet = @E_Wallet,
	Password = @Password,
	IsActive = @IsActive,
    IsDelete = @IsDelete,
    Points = @Points
    where MemberID = @MemberID
END
GO
/****** Object:  StoredProcedure [dbo].[Member_Insert]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Insert]   
    @Email nvarchar(50) = null,
    @E_Wallet nvarchar(50) = null,
    @Password nvarchar(50) = null,
    @IsActive int,
    @IsDelete int,
    @Points float,
    @CreateDate datetime
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Member(Email,E_Wallet,Password,IsActive,IsDelete,Points,CreateDate) values(@Email,@E_Wallet,@Password,@IsActive,@IsDelete,@Points,@CreateDate) 
    
END
GO
/****** Object:  StoredProcedure [dbo].[Member_Email_Sel]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Email_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE Password = @PassWord and IsActive = 1 and IsDelete = 0 and Email = @UserName or E_Wallet = @UserName;
GO
/****** Object:  StoredProcedure [dbo].[Member_Email_Check]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_Email_Check]   
    @UserName nvarchar(50)
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE Email = @UserName ;
GO
/****** Object:  StoredProcedure [dbo].[Member_E_Wallet_Sel]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_E_Wallet_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @UserName AND Password = @PassWord and IsActive = 1 and IsDelete = 0 ;
GO
/****** Object:  StoredProcedure [dbo].[Member_E_Wallet_Check]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_E_Wallet_Check]   
    @UserName nvarchar(50)
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @UserName ;
GO
/****** Object:  StoredProcedure [dbo].[Member_Check]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Check]   
    @Email nvarchar(50),
    @E_Wallet nvarchar(50)
AS   
    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @E_Wallet or Email = @Email;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewPoint]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewPoint]()
returns int
as
begin
declare @numberPoints int
set @numberPoints=(select COUNT(p.TransactionID) from TransactionPoint p
where DATEADD(DD, DATEDIFF(DD, 0, p.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberPoints);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewPackage]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewPackage]()
returns int
as
begin
declare @numberPackage int
set @numberPackage=(select COUNT(p.TransactionID) from TransactionPackage p
where DATEADD(DD, DATEDIFF(DD, 0, p.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberPackage);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewMember]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewMember]()
returns int
as
begin
declare @numberMember int
set @numberMember=(select COUNT(m.MemberID) from Member m
where DATEADD(DD, DATEDIFF(DD, 0, m.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberMember);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewCoin]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewCoin]()
returns int
as
begin
declare @numberCoin int
set @numberCoin=(select COUNT(c.TransactionID) from TransactionCoin c
where DATEADD(DD, DATEDIFF(DD, 0, c.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberCoin);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewBookingMegaball]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewBookingMegaball]()
returns int
as begin 
	declare @numberBookingMegaball int
	set @numberBookingMegaball=(select COUNT(b.BookingID) from BookingMegaball b
	where DATEADD(DD,DATEDIFF(DD,0,b.CreateDate),0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0));
	return(@numberBookingMegaball);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewBooking]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewBooking]()
returns int
as
begin
declare @numberBooking int
set @numberBooking=(select COUNT(b.BookingID) from Booking b
where DATEADD(DD, DATEDIFF(DD, 0, b.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberBooking);
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballBySearchFrontEnd]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingMegaballBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email from BookingMegaball b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate IS NULL or b.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballBySearchCMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingMegaballBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime = null,
	--@fromDate datetime,
	--@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email from BookingMegaball b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
--and (b.CreateDate between @fromDate and @toDate)
--and (@fromDate= '01/01/1990 12:01:00' or b.CreateDate between @fromDate and @toDate)
and (@fromDate= null or b.CreateDate between @fromDate and @toDate)
and (@status=-1 or b.Status=@status)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballByDraw]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_ListBookingMegaballByDraw]
(@DrawID int,@start int, @end int)
as begin
    select * from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email
	 from bookingmegaball b left join member m on b.MemberID=m.MemberID and b.DrawID = @DrawID) as Products  where Row>=@start and Row<=@end
		 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballByDate]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_ListBookingMegaballByDate]
(
@Date datetime
)
as begin
		select * 
		from BookingMegaball
		where Convert(varchar,OpenDate,103) = CONVERT(varchar, @Date, 103)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingBySearchFrontEnd]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListBookingBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingBySearchCMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListBookingBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)
and (@status=-1 or b.Status=@status)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingByDraw_FE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListBookingByDraw_FE](@DrawID int)
as begin
	select * from BookingMegaball where DrawID=@DrawID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingByDraw]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListBookingByDraw](@draw int)
as begin 
	select * from BookingMegaball where DrawID=@draw
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAwardWithdraw_CMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListAwardWithdraw_CMS]
(
	@UserName nvarchar(250),
	@FromDate datetime,
	@ToDate datetime,
	@RequestStatus smallint,
	@AwardDate datetime,
	@Start int,
	@End int
)
as
begin
select *from 
(
SELECT ROW_NUMBER() OVER (ORDER BY aw.RequestDate desc)as row, 
sum(1) over() as TOTALROWS,
		aw.AdminIDApprove,
		aw.ApproveDate,
		aw.ApproveNote,
		aw.AwardDate,
		aw.CoinIDWithdraw,
		aw.IsDeleted,
		aw.RequestDate,
		aw.RequestMemberID,
		aw.RequestStatus,
		aw.RequestWalletAddress,
		aw.TransactionID,
		aw.ValuesWithdraw
		from AwardWithdraw aw 
		where
		(@UserName is null or @UserName='' or aw.RequestMemberID=@UserName)
		and (@FromDate='01/01/1990' or (aw.RequestDate between @FromDate and @ToDate))
		and (@RequestStatus=-1 or aw.RequestStatus=@RequestStatus)
		and (@AwardDate='01/01/1990' or aw.AwardDate =@AwardDate)
		) as awardresult  
		where Row>=@Start and Row<=@End
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAwardMegaballBySearch]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAwardMegaballBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime = null,
	@toDate datetime = null
	--@awardNameEnglish nvarchar(250)
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
a.*,aw.AwardNameEnglish from AwardMegaball a left join Award aw on a.AwardID=aw.AwardID
where @fromDate=null or a.CreateDate between @fromDate and @toDate
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllTransactionPackage]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllTransactionPackage]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tp.*,p.PackageName,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID) as Products
		  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllPackageFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllPackageFE]

as begin 
	select * from package where IsActive=1 order by Priority ASC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllMember]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllMember]
 as begin
	Select Email from Member order by CreateDate DESC
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeTicket]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangeTicket]
(
	@start int,
	@end int
)
as 
begin
	
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY ex.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, ex.*,ad.UserName from ExchangeTicket ex left join Admin ad on ex.CreateUser= ad.UserName) as Products 
	 where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePointPaging]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangePointPaging]
(
	@start int,
	@end int
)
as 
begin
	
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY ex.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, ex.*,ad.UserName from ExchangePoint ex left join Admin ad on ex.CreateUser= ad.UserName) as Products 
	 where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePointBySearch]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllExchangePointBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime =null,
	@toDate datetime =null
)
as begin 
	select * from (Select ROW_NUMBER() over(Order by ex.CreateDate DESC)as Row,SUM(1) over() as TOTALROWS,
	ex.* from ExchangePoint ex where @fromDate=null or ex.CreateDate between @fromDate and @toDate)
	as Products where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePoint]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangePoint]
as begin
	select * from ExchangePoint order by CreateDate desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeNoPaging]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangeNoPaging]
as begin
	select * from ExchangeTicket order by CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeBySearch]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllExchangeBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime = null,
	@toDate datetime=null
)
as begin 
	select * from (Select ROW_NUMBER() over(Order by ex.CreateDate DESC)as Row,SUM(1) over() as TOTALROWS,
	ex.* from ExchangeTicket ex where @fromDate=null or ex.CreateDate between @fromDate and @toDate)
	as Products where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllCompanypaging]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllCompanypaging]
 (
	@start int,
	@end int
)

 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.MemberID  DESC) as Row,SUM(1) OVER()AS TOTALROWS,c.*, m.FullName from  CompanyInfomation c left join Member m on c.MemberID=m.MemberID
		) as Products
		  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [guest].[SP_ListAllCoin]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_ListAllCoin]
as begin
	select * from Coin where IsActive =1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllCoin]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_ListAllCoin]
as begin
	select * from Coin where IsActive =1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingPaging]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingPaging]
(
		@start int,
		@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet 
		from booking b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballPaging]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaballPaging]
(
	@start int,
	@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, b.*,m.Email 
		from BookingMegaball b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballForRevenueFreeByDraw]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllBookingMegaballForRevenueFreeByDraw]
(
		@IsRevenueFreeTicket int
)
as begin
		select *
		from BookingMegaball
		where IsRevenueFreeTicket = @IsRevenueFreeTicket
		order by CreateDate DESC 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballByDraw]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllBookingMegaballByDraw]
(
		@DrawID int
)
as begin
		select b.*,m.Email 
		from BookingMegaball b 
		left join Member m on b.MemberID=m.MemberID 
		where b.DrawID = @DrawID
		order by b.CreateDate DESC 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballByDate]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaballByDate]
(
		@date datetime
)
as begin
		select b.*,m.Email 
		from BookingMegaball b 
		left join Member m on b.MemberID=m.MemberID 
		where CONVERT(nvarchar(250),b.CreateDate, 103) = CONVERT(nvarchar(250), @date, 103)
		order by b.CreateDate DESC 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaball_FE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaball_FE]
as begin
	select * from BookingMegaball
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaball]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaball]
as begin
		select b.*,m.Email from BookingMegaball b left join Member m on b.MemberID=m.MemberID order by b.CreateDate DESC 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBooking]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBooking]
as begin
		select b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID  order by b.CreateDate DESC
end
GO
/****** Object:  StoredProcedure [guest].[SP_ListAllAdminPaging]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_ListAllAdminPaging]
(   @start int,
	@end int)
as begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,a.*,g.GroupName
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID)
		 as Products  where Row>=@start and Row<=@end

end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllAdminPaging]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllAdminPaging]
(   @start int,
	@end int)
as begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,a.*,g.GroupName,g.GroupID
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID)
		 as Products  where Row>=@start and Row<=@end

end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllAdmin]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllAdmin]
as begin
		select a.*,ac.GroupID,g.GroupName
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID;

end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAccessRightByManagerID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAccessRightByManagerID]
(@adminID int)
as begin
		select *from accessright where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_List_GroupAdmin]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_List_GroupAdmin]
as begin
		select *from groupadmin
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionPointfe]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTransactionPointfe]
(
	@memberID int,
	@points int,
	@createDate datetime,
	@status int,
	@transactionCode nvarchar(500),
	@note nvarchar(2500)

)
as begin 
	insert into transactionpoint(MemberID,Points,CreateDate,Status,TransactionCode,Note) values(@memberID,@points,@createDate,@status,@transactionCode,@note)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionPackageFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTransactionPackageFE]
(
	@memberID int,
	@packageID int,
	@createDate datetime,
	@expireDate datetime, 
	@status int,
	@transactionValue  nvarchar(250),
	@note nvarchar(3500)

)
as begin 
	insert into transactionpackage(MemberID,PackageID,CreateDate,ExpireDate,Status,TransactionCode,Note) values(@memberID,@packageID,@createDate,@expireDate,@status,@transactionValue,@note)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionCoinFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertTransactionCoinFE]
(
	@MemberID int,
	@WalletAddressID char(150),
	@ValueTransaction numeric(18, 10),
	@QRCode  nvarchar(500),
	@CreateDate datetime,
	@ExpireDate datetime,
	@Status smallint,
	@Note nvarchar(2500),
	@WalletID nvarchar(500),
	@TypeTransactionID int,
	@TransactionBitcoin nvarchar(500),
	@TransactionID char(150),
	@CoinID nchar(10)
)
as begin
	insert into TransactionCoin(TransactionID,WalletAddressID,MemberID,ValueTransaction,QRCode,CreateDate,ExpireDate,Status,Note,WalletID,TypeTransactionID,TransactionBitcoin,CoinID) 
                                values(@TransactionID,@WalletAddressID,@MemberID,@ValueTransaction,@QRCode,@CreateDate,@ExpireDate,@Status,@Note,@WalletID,@TypeTransactionID,@TransactionBitcoin,@CoinID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTicketWinning]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_InsertTicketWinning]   
    @AwardNumber nvarchar(50),
	@BookingID int,
	@CreatedDate datetime,
	@DrawID int,
	@ExtraNumber nvarchar(50),
	@FirstNumber nvarchar(50),
	@FivethNumber nvarchar(50),
	@FourthNumber nvarchar(50),
	@MemberID int,
	@SecondNumber nvarchar(50),
	@ThirdNumber nvarchar(50),
	@Value decimal(18,5)
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into TicketWinning(AwardNumber,BookingID,CreatedDate,DrawID,ExtraNumber,FirstNumber,FivethNumber,FourthNumber,MemberID,SecondNumber,ThirdNumber,Value) values(@AwardNumber,@BookingID,@CreatedDate,@DrawID,@ExtraNumber,@FirstNumber,@FivethNumber,@FourthNumber,@MemberID,@SecondNumber,@ThirdNumber,@Value) 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTicketConfig_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTicketConfig_CMS]
(
 	@ConfigName nvarchar(250),
	@CreatedDate datetime,
	@NumberTicket int,
	@IsActive smallint,
	@IsDeleted smallint,
	@CoinID nvarchar(250),
	@CreatedUser nvarchar(250),
	@CoinValues numeric(18,10)
)
as begin 
		insert into TicketConfig(ConfigName,NumberTicket,CoinValues,CoinID,IsActive,IsDeleted,CreatedDate,CreatedUser,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser)
		 values(@ConfigName,@NumberTicket,@CoinValues,@CoinID,@IsActive,@IsDeleted,@CreatedDate,@CreatedUser,'','','','')
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberWalletFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberWalletFE]
(
	@IndexWallet int,
	@IsActive smallint,
	@IsDelete smallint,
	@MemberID int,
 	@NumberCoin float,
	@WalletAddress nvarchar(250),
	@RippleAddress nvarchar(250),
	@SerectKeyRipple nvarchar(250)
)
as begin 
	insert into Member_Wallet(IndexWallet,IsActive,IsDelete,MemberID,NumberCoin,WalletAddress,RippleAddress,SerectKeyRipple) values(@IndexWallet,@IsActive,@IsDelete,@MemberID,@NumberCoin,@WalletAddress,@RippleAddress,@SerectKeyRipple)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberReferenceFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberReferenceFE]
(
	@MemberID int,
	@LinkReference nvarchar(1000),
	@MemberIDReference int,
	@LockID nvarchar(1000),
	@Status int,
	@Amount decimal(18,5)
)
as begin 
	insert into MemberReference(MemberID, CreatedDate, LinkReference, MemberIDReference, LockID, Status, Amount)
	 values(@MemberID, SYSDATETIME(), @LinkReference, @MemberIDReference, @LockID, @Status, @Amount)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberFE]
/*
	Edited By: Lý Kiến Đức
	Edited date: 16.01.2018
	Description: Bổ sung field @FBUserID
*/
(
	@email nvarchar(250),
	@password nvarchar(250) = null,
	@isActive smallint,
	@isDelete smallint,
	@points numeric(18,10),
	@createDate datetime = null,
	@fullName nvarchar(250),
	@mobile char(15) = null,
	@avatar nvarchar(250) = null,
	@gender smallint,
	@birthday datetime = null,
	@IsUserType smallint = 0,
	@UserTypeID varchar(100) = null,
	@LinkLogin nvarchar(250),
	@NumberTicketFree int,
	@LinkReference nvarchar(250),
	@outMemberID int OUTPUT

)
as begin 
	insert into member(Email, Password, IsActive, IsDelete, Points, CreateDate, FullName, Mobile, Avatar, Gender, Birthday, IsUserType, UserTypeID, LinkLogin, NumberTicketFree, LinkReference)
	 values(@email, @password, @isActive, @isDelete, @points, @createDate, @fullName, @mobile, @avatar, @gender, @birthday, @IsUserType, @UserTypeID, @LinkLogin,@NumberTicketFree, @LinkReference)

	 SELECT @outMemberID = SCOPE_IDENTITY() 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertLinkResetPasswordFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertLinkResetPasswordFE]
(
	@Email nvarchar(250),
	@LinkReset nvarchar(1000),
	@ExpireLink nvarchar(1000),
	@IPAddress nvarchar(50)
)
as begin 
		update LinkResetPassword set Status = 1 where CreatedDate <= SYSDATETIME() and Email = @Email
		insert into LinkResetPassword(Email,LinkReset,Status,NumberSend,CreatedDate,ExpireLink,IPAddress)
		 values(@Email,@LinkReset,0,1,SYSDATETIME(),@ExpireLink,@IPAddress)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertGroupAdmin]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertGroupAdmin]
(
	@groupName nvarchar(250),
	@isActive smallint
)
as begin
		insert into groupadmin(GroupName,IsActive) values(@groupName,@isActive)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertFreeTicketDaily]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_InsertFreeTicketDaily] (@NumberTicket int)
AS
BEGIN
  DECLARE @ISEXISTS int;
  DECLARE @TotalTicket int;
  DECLARE @AmountFreeTicket int;
  SET @ISEXISTS = 0;
  SET @TotalTicket = 0;
  BEGIN
    SELECT
      @ISEXISTS = 1
    FROM FreeTicketDaily
    WHERE CONVERT(nvarchar, FreeTicketDaily.Date, 103) = CONVERT(nvarchar, SYSDATETIME(), 103)
    AND CONVERT(nvarchar, FreeTicketDaily.CreatedDate, 103) = CONVERT(nvarchar, SYSDATETIME(), 103)
  END;
  IF (@ISEXISTS = 0)
  BEGIN
    SET @AmountFreeTicket = (SELECT
      Amount
    FROM StoreFreeTicket)
    IF (@AmountFreeTicket > 0)
    BEGIN
      IF (@AmountFreeTicket >= @NumberTicket)
      BEGIN
        SET @TotalTicket = (SELECT
          Total
        FROM FreeTicketDaily
        WHERE IsActive = 1);

        UPDATE FreeTicketDaily
        SET IsActive = 0
        WHERE IsActive = 1;

        INSERT INTO FreeTicketDaily (Date, Total, IsActive, CreatedDate)
          VALUES (SYSDATETIME(), (@TotalTicket + @NumberTicket), 1, SYSDATETIME())

        UPDATE StoreFreeTicket
        SET amount = (@AmountFreeTicket - @NumberTicket)

      END;
      ELSE
      BEGIN
        UPDATE FreeTicketDaily
        SET IsActive = 0
        WHERE IsActive = 1;

        INSERT INTO FreeTicketDaily (Date, Total, IsActive, CreatedDate)
          VALUES (SYSDATETIME(), (@TotalTicket + @AmountFreeTicket), 1, SYSDATETIME())

        UPDATE StoreFreeTicket
        SET amount = 0
      END;
    END;
  END;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertExchangeTicket]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertExchangeTicket]
(
	@pointValue float,
	@ticketNumber int,
	@isActive smallint,
	@isDelete smallint,
	@createDate datetime,
	@createUser nvarchar(250),
	@memberID int,
	@updateDate datetime,
	@updateUser nvarchar(250),
	@deleteDate date,
	@deleteUser nvarchar(250),
	@coinID char(10)
)
as begin
	insert into ExchangeTicket(PointValue,TicketNumber,IsActive,IsDelete,CreateDate,CreateUser,MemberID,UpdateDate,UpdateUser,DeleteDate,DeleteUser,CoinID)
	values(@pointValue,@ticketNumber,@isActive,@isDelete,@createDate,@createUser,@memberID,@updateDate,@updateUser,@deleteDate,@deleteUser,@coinID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertExchangePoint]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertExchangePoint]
(
	@pointValue float,
	@bitcoinValue float,
	@isActive smallint,
	@isDelete smallint,
	@createDate datetime,
	@createUser nvarchar(250),
	@memberID int,
	@updateDate datetime,
	@updateUser nvarchar(250),
	@deleteDate date,
	@deleteUser nvarchar(250)
)
as begin
	insert into ExchangePoint(PointValue,BitcoinValue,IsActive,IsDelete,CreateDate,CreateUser,MemberID,UpdateDate,UpdateUser,DeleteDate,DeleteUser)
	values(@pointValue,@bitcoinValue,@isActive,@isDelete,@createDate,@createUser,@memberID,@updateDate,@updateUser,@deleteDate,@deleteUser)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertDraw]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_InsertDraw]
(
--@AWARDNAME NVARCHAR(250),
@STARTDATE DATETIME,
@ENDDATE DATETIME,
@CREATEDDATE DATETIME
)
AS BEGIN 
declare @ISEXISTS INT;
SET @ISEXISTS = 0;
BEGIN
	SELECT @ISEXISTS = 1 
	FROM DRAW
	WHERE CONVERT(NVARCHAR, DRAW.CREATEDDATE, 103) = CONVERT(NVARCHAR, @CREATEDDATE, 103);
END;
	IF(@ISEXISTS = 0) 
	BEGIN
	INSERT INTO DRAW(STARTDATE,ENDDATE,CREATEDDATE)
		VALUES(@STARTDATE,@ENDDATE,@CREATEDDATE)
		END;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertCountry_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertCountry_CMS]
(
 	@CountryName nvarchar(250),
	@IsActive smallint,
	@IsDeleted smallint,
	@CreatedDate DateTime,
	@CreatedUser nvarchar(250),
	@PhoneZipCode varchar(50)
)
as begin 
	insert into Country(CountryName,PhoneZipCode,IsActive,IsDeleted,CreatedDate,CreatedUser,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser) 
	values(@CountryName,@PhoneZipCode,@IsActive,@IsDeleted,@CreatedDate,@CreatedUser,'','','','')
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertBookingFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertBookingFE]
(
	@memberID int,
	@numberValue int,
	@quantity int,
	@createDate datetime,
	@openDate datetime,
	@status int,
	@transactionCode char(100)
)
as begin 
	insert into booking(MemberID,NumberValue,Quantity,CreateDate,OpenDate,Status,TransactionCode) values(@memberID,@numberValue,@quantity,@createDate,@openDate,@status,@transactionCode)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertBooking_FE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertBooking_FE]
(
	@memberID int,
	@createDate datetime,
	@openDate datetime,
	@status int,
	@transactionCode char(100),
	@note nvarchar(2500),
	@noteEnglish nvarchar(2500),
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@quantity int,
	@ValuePoint numeric(18,10),
	@DrawID int,
	@IsFreeTicket int

)
as begin 
	insert into BookingMegaball(MemberID,CreateDate,OpenDate,Status,TransactionCode,Note,NoteEnglish,FirstNumber,SecondNumber,ThirdNumber,FourthNumber,FivethNumber,ExtraNumber,Quantity,ValuePoint,DrawID,IsFreeTicket,IsRevenueFreeTicket)
	 values(@memberID,@createDate,@openDate,@status,@transactionCode,@note,@noteEnglish,@firstNumber,@secondNumber,@thirdNumber,@fourthNumber,@fivethNumber,@extraNumber,@quantity,@ValuePoint,@DrawID,@IsFreeTicket,0)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardWithdraw]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAwardWithdraw]
(
	@TransactionID char(50),
	@RequestWalletAddress nvarchar(250),
	@RequestMemberID nvarchar(250),
	@CoinIDWithdraw char(10),
	@RequestStatus int,
	@RequestDate datetime,
	@IsDeleted int,
	@AwardDate datetime,
	@ValuesWithdraw numeric(18, 8),
	@BookingID int	
)
as begin 
	insert into AwardWithdraw(TransactionID,RequestMemberID,RequestWalletAddress,RequestDate,RequestStatus,AwardDate,CoinIDWithdraw,ValuesWithdraw,AdminIDApprove,ApproveDate,ApproveNote,IsDeleted,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser,BookingID)
	values(@TransactionID,@RequestMemberID,@RequestWalletAddress,@RequestDate,@RequestStatus,@AwardDate,@CoinIDWithdraw,@ValuesWithdraw,'','','',@IsDeleted,'','','','',@BookingID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardNumberFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardNumberFE]
(
	@numberID int,
	@awardID int,
	@createDate datetime,
	@priority smallint,
	@isDelete smallint,
	@isActive smallint,
	@numberValue int,
	@stationName nvarchar(250)
)
as begin 
	insert into awardnumber(NumberID,AwardID,NumberValue,CreateDate,IsActive,IsDelete,Priority,StationName) values(@numberID,@awardID,@numberValue,@createDate,@isActive,@isDelete,@priority,@stationName)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardNumber]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardNumber]
 (
	@awardID int,
	@numberValue float,
	@createDate datetime,
	@isActive smallint,
	@isDelete smallint,
	@priority int,
	@stationName nvarchar(250),
	@stationNameEnglish nvarchar(250)
 )
 as begin 
		insert into awardnumber(AwardID,NumberValue,CreateDate,IsActive,IsDelete,Priority,StationName,StationNameEnglish)
		 values(@awardID,@numberValue,@createDate,@isActive,@isDelete,@priority,@stationName,@stationNameEnglish)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardMegaball]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAwardMegaball]
/*
	DESCRIPTION: @AWARDDATEPREVIOUS, @JACKPOTPREVIOUS, @TOTALJACKPOT
*/
(
	@awardID int,
	@createDate datetime,
	@isActive smallint,
	@isDelete smallint,
	@priority smallint,
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@genCode nvarchar(500),
	@jackPotDefault float,
	@DrawID int,
	@isExists int out
)
as begin
    DECLARE @AWARDDATEPREVIOUS DATETIME = null,
	        @JACKPOTPREVIOUS FLOAT = @JACKPOTDEFAULT,
	        @TOTALJACKPOT FLOAT = @JACKPOTDEFAULT,
			@JACKPOTWINNER FLOAT = 0;

    Set @isExists = 0;
	BEGIN
		SELECT @isExists = 1 
		FROM AWARDMEGABALL
		WHERE Convert(nvarchar, AWARDMEGABALL.CreateDate, 103) = Convert(nvarchar, @createDate, 103);
	END;

	IF(@isExists = 0) 
	BEGIN
			BEGIN  
				--GET RESULT AWARD WITH CREATEDDATE LAST
				SELECT TOP 1 @AWARDDATEPREVIOUS = CREATEDATE,
							 @JACKPOTPREVIOUS = AWARDMEGABALL.JackPot
				FROM AWARDMEGABALL
				WHERE AWARDMEGABALL.ISDELETE = 0
					  AND AWARDMEGABALL.ISACTIVE = 1
					  ORDER BY CREATEDATE DESC;
			END;

			BEGIN  
				SELECT @TOTALJACKPOT = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
				FROM BOOKINGMEGABALL
				WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
					  AND NOT EXISTS (SELECT 1
								  FROM BOOKINGMEGABALL
								  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
										and (
											   BOOKINGMEGABALL.FirstNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
											   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.SecondNumber = @firstNumber 
											   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
											   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
											   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
											   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
											   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
											   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.FourthNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
											   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.FivethNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
											   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
											   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
											));
			END

			IF(@TOTALJACKPOT IS NULL)
			BEGIN
				--ELSE IF NOW HAVE LEAST 1 MEMBER WINNER -> TOTAL JACKPOT WILL BE DEFAULT (CONFIG SOURCES) 
				--@JACKPOTWINNER: Total jackpot won -> save history
			   BEGIN  
					SELECT @JACKPOTWINNER = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
					FROM BOOKINGMEGABALL
					WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
						  AND EXISTS (SELECT 1
									  FROM BOOKINGMEGABALL
									  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
											and (
											   BOOKINGMEGABALL.FirstNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
											   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
											)
											and (
												   BOOKINGMEGABALL.SecondNumber = @firstNumber 
												   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
												   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
												   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
												   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
												   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
												   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.FourthNumber = @firstNumber 
												   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
												   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
												   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
												   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
												   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.FivethNumber = @firstNumber 
												   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
												   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
												   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
												   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
												   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
												   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
												));
				END
				--set @JACKPOTWINNER = @JACKPOTPREVIOUS;
			   SET @TOTALJACKPOT = @jackPotDefault;
			END
			--ELSE
			--BEGIN
			--   --IF NOT ANY LEAST 1 MEMBER WINNER -> TOTAL = DEFAULT (CONFIG) + JACTPOT PREVIOUS (IF HAVE)
			--   SET @TOTALJACKPOT = @JACKPOTPREVIOUS;
			--END;
			--UPDATE AWARDMEGABALL
			--SET AWARDMEGABALL.AWARDID = @awardID,
			--    AWARDMEGABALL.CreateDate = @createDate,
			--	AWARDMEGABALL.IsActive = @isActive,
			--	AWARDMEGABALL.IsDelete = @isDelete,
			--	AWARDMEGABALL.Priority = @priority,
			--	AWARDMEGABALL.FirstNumber = @firstNumber,
			--	AWARDMEGABALL.SecondNumber = @secondNumber,
			--	AWARDMEGABALL.ThirdNumber = @thirdNumber,
			--	AWARDMEGABALL.FourthNumber = @fourthNumber,
			--	AWARDMEGABALL.FivethNumber = @fivethNumber,
			--	AWARDMEGABALL.ExtraNumber = @extraNumber,
			--	AWARDMEGABALL.GenCode = @genCode,
			--	AWARDMEGABALL.JackPot = @TOTALJACKPOT,
			--	AWARDMEGABALL.JACKPOTWINNER = @JACKPOTWINNER
		 --   WHERE AWARDMEGABALL.IsDelete = 0
			--	  AND AWARDMEGABALL.IsActive = 0;
    
			--IF(@@ROWCOUNT = 0)
			--BEGIN
				INSERT 
				INTO AWARDMEGABALL(AWARDID,
								   CREATEDATE,
								   ISACTIVE,
								   ISDELETE,
								   PRIORITY,
								   FIRSTNUMBER,
								   SECONDNUMBER,
								   THIRDNUMBER,
								   FOURTHNUMBER,
								   FIVETHNUMBER,
								   EXTRANUMBER,
								   GENCODE,
								   JACKPOT, 
								   JackPotWinner,
								   DrawID)
					VALUES(@AWARDID,
						   @CREATEDATE,
						   @ISACTIVE,
						   @ISDELETE,
						   @PRIORITY,
						   @FIRSTNUMBER,
						   @SECONDNUMBER,
						   @THIRDNUMBER,
						   @FOURTHNUMBER,
						   @FIVETHNUMBER,
						   @EXTRANUMBER,
						   @GENCODE,
						   @TOTALJACKPOT,
						   @JACKPOTWINNER,
						   @DrawID)
			--END;
	END;
	
	
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardFE]
(
	@awardName nvarchar(250),
	@awardValue float,
	@createDate datetime,
	@priority smallint,
	@isDelete smallint,
	@isActive smallint

)
as begin 
	insert into award(AwardName,AwardValue,CreateDate,Priority,IsActive,IsDelete) values(@awardName,@awardValue,@createDate,@priority,@isActive,@isDelete)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAward]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAward]
(
	--@awardName nvarchar(250),
	@awardValue decimal(18,10),
	@createDate datetime,
	@priority int,
	@isActive smallint,
	@isDelete smallint,
	@awardNameEnglish nvarchar(250),
	@awardFee decimal(18,10),
	@AwardPercent decimal(18,10)
)
as begin 
		insert into award(AwardName,AwardValue,CreateDate,Priority,IsActive,IsDelete,AwardNameEnglish,AwardFee,AwardPercent)
		 values('',@awardValue,@createDate,@priority,@isActive,@isDelete,@awardNameEnglish,@awardFee,@AwardPercent)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAdmin]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_InsertAdmin]
(
	@userName nvarchar(50),
	@pass nvarchar(250),
	@hashKey nvarchar(250),
	@isActive smallint,
	@createDate datetime,
	@fullName nvarchar(250),
	@email nvarchar(250),
	@mobile char(50),
	@address nvarchar(250),
	@avatar nvarchar(250),
	@isDelete smallint
)
as
begin
	insert into admin(UserName,Password,Hashkey,IsActive,CreateDate,FullName,Email,Mobile,Address,Avatar,IsDelete)
				 values(@userName,@pass,@hashKey,@isActive,@createDate,@fullName,@email,@mobile,@address,@avatar,@isDelete);
				  SELECT SCOPE_IDENTITY()

end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAccessRight]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAccessRight]
(
	@groupID int,
	@adminID int
)
as begin
		insert into accessright(GroupID,AdminID) values(@groupID,@adminID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetTotalsSMSMember_FE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetTotalsSMSMember_FE](@email nvarchar(250),@totalsSMS int)
as begin 
	select * from Member where Email=@email and TotalSmsCodeInput=@totalsSMS
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetPointsMemberFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetPointsMemberFE]
(
@memberID int
)
as begin 
	select Points from member where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetPackageDetail]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetPackageDetail]
(
	@packageID int
)
as begin 
	select * from package where PackageID=@packageID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetNewestDrawID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetNewestDrawID]
as begin 
	select top 1 * from draw  order by createddate desc, DrawID asc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetMemberDetailByIDFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetMemberDetailByIDFE]
(
@memberID int
)
as begin 
	select * from member where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetMemberDetailByEmailFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetMemberDetailByEmailFE]
(
@email nvarchar(250)
)
as begin 
	select * from member where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetManagerIDNewest]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetManagerIDNewest]
as begin
		select top 1 AdminID from admin order by AdminID desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListWinner]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListWinner]
 (
	--@fromDate datetime
	--@toDate datetime,
	--@start int,
	--@end int
	@numberID int
 )
 as begin 
     DECLARE @firstNumber int,
	         @SecondNumber int,
			 @ThirdNumber int,
			 @FourthNumber int,
			 @FivethNumber int,
			 @ExtraNumber int,
			 @AWARDDATENOW datetime,
			 @AWARDDATEPREV datetime = NULL,
			 @JACKPOTWINNER FLOAT,
			 @AwardNameEnglish nvarchar(250);
	BEGIN  
	    --GET RESULT AWARD WITH CREATEDDATE LAST
		SELECT TOP 1 @AWARDDATENOW = AWARDMEGABALL.CREATEDATE,
		             @FirstNumber = AWARDMEGABALL.FirstNumber,
					 @SecondNumber = AWARDMEGABALL.SecondNumber,
					 @ThirdNumber = AWARDMEGABALL.ThirdNumber,
					 @FourthNumber = AWARDMEGABALL.FourthNumber,
					 @FivethNumber = AWARDMEGABALL.FivethNumber,
					 @ExtraNumber = AWARDMEGABALL.ExtraNumber,
					 @JACKPOTWINNER = AWARDMEGABALL.JACKPOTWINNER,
					 @AwardNameEnglish = Award.AwardNameEnglish
		FROM AWARDMEGABALL
		JOIN Award
		  ON Award.AwardID = AWARDMEGABALL.AwardID
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  AND AwardMegaball.NumberID = @numberID;

		--GET AWARD DATE PREVIOUS NOT WON
		SELECT TOP 1 @AWARDDATEPREV = CREATEDATE
		FROM AWARDMEGABALL
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  AND AWARDMEGABALL.NumberID != @numberID
			  AND AWARDMEGABALL.CreateDate < @AWARDDATENOW
			  ORDER BY CREATEDATE DESC;
	END;

	SELECT BOOKINGMEGABALL.BookingID,
	       MEMBER.Email,
		   BookingMegaball.TransactionCode,
		   BOOKINGMEGABALL.FirstNumber,
		   BOOKINGMEGABALL.SecondNumber,
		   BOOKINGMEGABALL.ThirdNumber,
		   BOOKINGMEGABALL.FourthNumber,
		   BOOKINGMEGABALL.FivethNumber,
		   BOOKINGMEGABALL.ExtraNumber, 
		   @JACKPOTWINNER JACKPOTWINNER,
		   BOOKINGMEGABALL.CreateDate,
		   @AWARDDATENOW OpenDate,
		   @AwardNameEnglish AwardNameEnglish
	FROM Member
	JOIN BOOKINGMEGABALL
	  ON BOOKINGMEGABALL.MemberID = Member.MemberID
	WHERE ((@AWARDDATEPREV is null 
	        and BOOKINGMEGABALL.CreateDate < @AWARDDATENOW
	        )
		    or (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREV and @AWARDDATENOW))
		and (
				BOOKINGMEGABALL.FirstNumber = @firstNumber 
				Or BOOKINGMEGABALL.FirstNumber = @secondNumber
				Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
				Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
				Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
				Or BOOKINGMEGABALL.FirstNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.SecondNumber = @firstNumber 
				Or BOOKINGMEGABALL.SecondNumber = @secondNumber
				Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
				Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
				Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
				Or BOOKINGMEGABALL.SecondNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.ThirdNumber = @firstNumber 
				Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
				Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
				Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
				Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
				Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.FourthNumber = @firstNumber 
				Or BOOKINGMEGABALL.FourthNumber = @secondNumber
				Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
				Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
				Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
				Or BOOKINGMEGABALL.FourthNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.FivethNumber = @firstNumber 
				Or BOOKINGMEGABALL.FivethNumber = @secondNumber
				Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
				Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
				Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
				Or BOOKINGMEGABALL.FivethNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.ExtraNumber = @firstNumber 
				Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
				Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
				Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
				Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
				Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
			)
		--and BOOKINGMEGABALL.FirstNumber = @firstNumber
		--AND BOOKINGMEGABALL.SecondNumber = @secondNumber
		--AND BOOKINGMEGABALL.ThirdNumber = @thirdNumber
		--AND BOOKINGMEGABALL.FourthNumber = @fourthNumber
		--AND BOOKINGMEGABALL.FivethNumber = @fivethNumber
		--AND BOOKINGMEGABALL.ExtraNumber = @extraNumber

		--select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.BookingID ASC) 
		--				as Row,SUM(1) OVER()AS TOTALROWS,
		--					m.Email,b.TransactionCode,b.BookingID,b.OpenDate,b.CreateDate,b.FirstNumber,
		--					b.SecondNumber,b.ThirdNumber,b.FourthNumber,b.FivethNumber,
		--					b.ExtraNumber,aw.AwardNameEnglish,a.JackPot
		--				from Member m 
		--				left join BookingMegaball b on m.MemberID=b.MemberID
		--				left join AwardMegaball a 
		--					   on (a.FirstNumber=b.FirstNumber and a.SecondNumber=b.SecondNumber and a.ThirdNumber=b.ThirdNumber
		--					 and a.FourthNumber=a.FourthNumber
		--					and a.FivethNumber=b.FivethNumber and a.ExtraNumber=b.ExtraNumber) 
		--				left join Award aw 
		--				       on a.AwardID=aw.AwardID
		--				where a.IsActive=1 and a.IsDelete=0 and b.Status=1
		--				 --and DATEADD(DD, DATEDIFF(DD, 0, a.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, b.OpenDate), 0)
		--				 and a.CreateDate between @fromDate and @toDate )
		--   as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdatePointsMemberFE]
(
@points numeric(18, 10),
@memberID int
)
as begin 
	update member set Points=Points+@points where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMember_FE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePointsMember_FE]
(
@memberID INT,
@points numeric(18, 10)
)
as begin 
	update member set Points=Points+@points where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMember]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePointsMember]
(
	@points float,
	@memberID int
)
as begin
		update member set Points=Points+@points where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdatePointMemberFE]
/*
	EDITED BY: LÝ KIẾN ĐỨC
	EDITED DATE: 25.01.2018
	DESCRIPTION: UPDATE JACKPOT POINT AwardMegaball
*/
(
	@Points numeric(18, 10),
	@MemberID int,
	@NumberID int,
	@TotalJackPot float
)
as begin 
	update Member set Points=@Points where MemberID=@MemberID;
	--IF(@@ROWCOUNT > 0)
	--BEGIN 
	--	--if query update success -> update jackpot AwardMegaball
	--	DECLARE @ISEXISTS INT;
	--	BEGIN
	--		SELECT @ISEXISTS = 1
	--		FROM AwardMegaball
	--		WHERE AwardMegaball.IsDelete = 0;
	--	END;
	--	IF(@ISEXISTS = 1)
	--		BEGIN
	--		    --IF TABLE AwardMegaball EXISTS LEAST 1 RECORD -> UPDATE JACKPOT
	--			UPDATE AwardMegaball
	--			SET AwardMegaball.JackPot = @TotalJackPot
	--			WHERE AwardMegaball.IsDelete = 0
	--			      AND EXISTS(SELECT 1
	--							FROM AwardMegaball AW
	--							WHERE AW.IsDelete = 0
	--								AND AwardMegaball.CreateDate >=AW.CreateDate
	--							)
	--		END
	--	ELSE 
	--		BEGIN
	--		    --IF TABLE AwardMegaball NOT EXISTS LEAST 1 RECORD -> INSERT 1 RECORD DEFAULT
	--			INSERT 
	--			INTO AWARDMEGABALL(CreateDate, IsDelete,ISACTIVE, JACKPOT)
	--				VALUES(SYSDATETIME(),0,0,@TotalJackPot)
	--		END;
		
	--END;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePasswordMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePasswordMemberFE]
(
	@password nvarchar(250),
	@email nvarchar(250)
)
as begin 
	update member set Password=@password where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePasswordMember]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePasswordMember]
(
	@password nvarchar(250),
	@fullname nvarchar(250)
)
as begin
		update member set Password=@password where Fullname=@fullname
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateNewPasswordMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateNewPasswordMemberFE]
(
	@newpassword nvarchar(250),
	@email nvarchar(250),
	@password nvarchar(250)
)
as begin 
	update member set Password=@newpassword where Email=@email and Password=@password
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateMemberReferenceFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateMemberReferenceFE]
(
	@MemberID int,
	@LockID nvarchar(1000),
	@MemberIDReference varchar(1000),
	@Status int,
	@Amount decimal(18,5)
)
as begin 
		declare @TotalNumberTicket int;
		set @TotalNumberTicket = 0;
		set @TotalNumberTicket = (select total from FreeTicketDaily where IsActive = 1);

		update MemberReference set LockID = @LockID, Status = @Status,Amount=@Amount
		where MemberID = @MemberID and MemberIDReference = @MemberIDReference

		if(@Status = 0)
		begin
			if(@TotalNumberTicket > 0)
				begin
					update FreeTicketDaily set Total =@TotalNumberTicket - 1 where IsActive = 1;
				end;
		end;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateJackpotAwardMegaball]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateJackpotAwardMegaball]
 (
	@jackpot decimal(18,5),
	@draw int,
	@totalwon decimal(18,5)
 )
 as begin
	update AwardMegaball 
	set 
		JackPot = @jackpot,
		TotalWon = @totalwon
	where NumberID=(select top 1 AwardMegaball.NumberID from AwardMegaball where DrawID = @draw)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateInformationMember_FE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateInformationMember_FE]
(
	@email nvarchar(250),
	@mobile char(15),
	@smsCode nvarchar(50),
	@isIndentifySms int,
	@expireSmSCode datetime,
	@totalSmsCodeInput int
)
as begin
	update Member set Mobile=@mobile, SmsCode=@smsCode,IsIndentifySms=@isIndentifySms,TotalSmsCodeInput=@totalSmsCodeInput,ExpireSmSCode=@expireSmSCode
	where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateGroupAdmin]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateGroupAdmin]
(
		@groupName nvarchar(250),
		@groupID int
)
as begin 
		update groupadmin set GroupName=@groupName where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateFreeTicketMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateFreeTicketMemberFE]
(
	@numberticketfree int,
	@memberID int
)
as begin
		update member set NumberTicketFree=@numberticketfree where MemberID=@memberID;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateExchangeTicket]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateExchangeTicket]
 (
	@exchangeID int,
	@pointValue float,
	@ticketNumber int,
	@createUser nvarchar(250),
	@updateUser nvarchar(250),
	@updateDate datetime,
	@coinID char(10)
	
 )
 as begin
	update ExchangeTicket set PointValue=@pointValue, TicketNumber=@ticketNumber, CreateUser=@createUser, UpdateDate=@updateDate,UpdateUser=@updateUser,CoinID=@coinID
	where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateExchangePoint]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateExchangePoint]
 (
	@exchangeID int,
	@pointValue float,
	@bitcoinValue float,
	@createUser nvarchar(250),
	@updateUser nvarchar(250),
	@updateDate datetime
	
 )
 as begin
	update ExchangePoint set PointValue=@pointValue, BitcoinValue=@bitcoinValue, CreateUser=@createUser, UpdateUser=@updateUser,UpdateDate=@updateDate
	where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateCountry_CMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateCountry_CMS]
(
    @CountryID int,
 	@CountryName nvarchar(250),
	@IsActive smallint,
	@IsDeleted smallint,
 	@UpdatedDate DateTime,
	@UpdatedUser nvarchar(250),
	@PhoneZipCode varchar(50)
)
as begin 
	Update Country set CountryName=@CountryName, PhoneZipCode=@PhoneZipCode,IsActive=@IsActive,IsDeleted=@IsDeleted,UpdatedDate=@UpdatedDate,UpdatedUser=@UpdatedUser
	Where CountryID=@CountryID and IsActive=1 and IsDeleted=0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateBookingMaegaball_FreeTicket]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateBookingMaegaball_FreeTicket]
(
	@BookingID varchar(1000) 
)
as begin 
	--update BookingMegaball set IsRevenueFreeTicket =1 where BookingID in (select * from Stringsplit(@BookingID,','));
	update BookingMegaball set IsRevenueFreeTicket = 1 where IsFreeTicket = 0 and CONVERT(nvarchar(250),CreateDate,103) = (CONVERT(nvarchar(250),(select CreateDate from BookingMegaball where BookingID = @BookingID),103));
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardNumber]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateAwardNumber]
 (
	@numberID int,
	@awardID int,
	@numberValue float,
	@stationName nvarchar(250),
	@stationNameEnglish nvarchar(250)

 )
 as begin  
		update awardnumber set AwardID=@awardID,NumberValue=@numberValue,StationName=@stationName,StationNameEnglish=@stationNameEnglish
		 where NumberID=@numberID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardMegaball]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAwardMegaball]
 (
	@numberID int,
	@awardID int,	
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@genCode nvarchar(500),
	@jackPotDefault float
 )
 as begin
	  DECLARE @AWARDDATEPREVIOUS DATETIME = null,
	        @JACKPOTPREVIOUS FLOAT = @JACKPOTDEFAULT,
	        @TOTALJACKPOT FLOAT = @JACKPOTDEFAULT,
			@JACKPOTWINNER FLOAT = 0;
	BEGIN  
	    --GET RESULT AWARD WITH CREATEDDATE LAST
		SELECT TOP 1 @AWARDDATEPREVIOUS = CREATEDATE,
		             @JACKPOTPREVIOUS = AWARDMEGABALL.JackPot
		FROM AWARDMEGABALL
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  ORDER BY CREATEDATE DESC;
	END;

	BEGIN  
		SELECT @TOTALJACKPOT = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
		FROM BOOKINGMEGABALL
		WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
		      AND NOT EXISTS (SELECT 1
			              FROM BOOKINGMEGABALL
						  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
								and (
									   BOOKINGMEGABALL.FirstNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
									   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.SecondNumber = @firstNumber 
									   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
									   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
									   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
									   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
									   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
									   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.FourthNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
									   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.FivethNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
									   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
									   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
									));
	END

	IF(@TOTALJACKPOT IS NULL)
	BEGIN
		--ELSE IF NOW HAVE LEAST 1 MEMBER WINNER -> TOTAL JACKPOT WILL BE DEFAULT (CONFIG SOURCES) 
		--@JACKPOTWINNER: Total jackpot won -> save history
	   BEGIN  
			SELECT @JACKPOTWINNER = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
			FROM BOOKINGMEGABALL
			WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
				  AND EXISTS (SELECT 1
							  FROM BOOKINGMEGABALL
							  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
									and (
									   BOOKINGMEGABALL.FirstNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
									   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
									)
									and (
										   BOOKINGMEGABALL.SecondNumber = @firstNumber 
										   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
										   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
										   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
										   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
										   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
										   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.FourthNumber = @firstNumber 
										   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
										   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
										   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
										   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
										   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.FivethNumber = @firstNumber 
										   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
										   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
										   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
										   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
										   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
										   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
										));
		END
		--set @JACKPOTWINNER = @JACKPOTPREVIOUS;
	   SET @TOTALJACKPOT = @jackPotDefault;
	END

	update AwardMegaball 
	set AwardID = @awardID,
	    FirstNumber = @firstNumber,
		SecondNumber = @secondNumber,
		ThirdNumber = @thirdNumber,
		FourthNumber = @fourthNumber,
		FivethNumber = @fivethNumber,
		ExtraNumber = @extraNumber, 
		GenCode = @genCode, 
		JackPot = @TOTALJACKPOT,
		JACKPOTWINNER = @JACKPOTWINNER
	where NumberID=@numberID

 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAwardFE]
(
	@awardName nvarchar(250),
	@awardValue float,
	@awardID int

)
as begin 
	update award set AwardName=@awardName,AwardValue=@awardValue where AwardID=@awardID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAward]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAward]
(
	@awardID int,
	--@awardName nvarchar(250),
	--@awardValue float,
	@awardNameEnglish nvarchar(250),
	@awardValue decimal(18,10),
	@awardFee decimal(18, 10),
	@AwardPercent decimal(18,10)
)
as begin 
		update award set AwardNameEnglish=@awardNameEnglish, AwardValue =@awardValue, AwardFee=@awardFee ,AwardPercent=@AwardPercent
		 where AwardID=@awardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAdmin]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAdmin]
(
	@adminID int,
	@fullName nvarchar(250),
	@email nvarchar(250),
	@mobile char(50),
	@address nvarchar(250)
)
as begin
		update admin set FullName=@fullName,Email=@email,Mobile=@mobile,Address=@address where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateActive]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateActive]
(
	@email nvarchar(250)
)
as begin 
	update member set IsActive=1 where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_TicketWinning_Add]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_TicketWinning_Add]
 (
	@BookingID int,
	@DrawID int,
	@BuyDate DateTime,
	@AwardID int,
	@FirstNumberBuy int,
	@SecondNumberBuy int,
	@ThirdNumberBuy int,
	@FourthNumberBuy int, 
	@FivethNumberBuy int,
	@ExtraNumberBuy int,
	@FirstNumberAward int,
	@SecondNumberAward int,
	@ThirdNumberAward int,
	@FourthNumberAward int, 
	@FivethNumberAward int,
	@ExtraNumberAward int,
	@NumberBallNormal int,
	@NumberBallGold int, 
	@AwardValue decimal(18, 5),
	@AwardDate DateTime,
	@MemberID int,
	@Priority int,
	@AwardFee decimal(18, 8),
	@TicketWinningID int out
 )
 as begin 
		Declare @IsExists int = 0;
		Declare @TicketWinID int = 0;
		Begin
			Select @IsExists = 1
			From TicketWinning
			Where TicketWinning.DrawID = @DrawID
			      And TicketWinning.BookingID = @BookingID;
		End

		If(@IsExists = 0) 
		Begin
			Insert into TicketWinning(BookingID, 
									DrawID, 
									BuyDate, 
									AwardID, 
									FirstNumberBuy, 
									SecondNumberBuy, 
									ThirdNumberBuy, 
									FourthNumberBuy, 
									FivethNumberBuy, 
									ExtraNumberBuy,
									FirstNumberAward,
									SecondNumberAward,
									ThirdNumberAward,
									FourthNumberAward, 
									FivethNumberAward,
									ExtraNumberAward,
									NumberBallNormal,
									NumberBallGold, 
									AwardValue,
									AwardDate,
									MemberID,
									Priority,
									AwardFee)
			Values(
					@BookingID, 
					@DrawID, 
					@BuyDate, 
					@AwardID, 
					@FirstNumberBuy, 
					@SecondNumberBuy, 
					@ThirdNumberBuy, 
					@FourthNumberBuy, 
					@FivethNumberBuy, 
					@ExtraNumberBuy,
					@FirstNumberAward,
					@SecondNumberAward,
					@ThirdNumberAward,
					@FourthNumberAward, 
					@FivethNumberAward,
					@ExtraNumberAward,
					@NumberBallNormal,
					@NumberBallGold, 
					@AwardValue,
					@AwardDate,
					@MemberID,
					@Priority,
					@AwardFee
				  );

				  Set @TicketWinID = Scope_Identity();
		End;
		

	Select @TicketWinningID = @TicketWinID; 
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_TicketNumberTimeLog_Add]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_TicketNumberTimeLog_Add]
 (
	@Email nvarchar(250),
	@TicketNumber int,
	@CreatedDate datetime,
	@Id int out
 )
 as begin 
		Insert into TicketNumberTimeLog(Email, TicketNumber, CreatedDate)
		Values (@Email, @TicketNumber, @CreatedDate);

		select @Id = Scope_Identity();
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_SumValuePoints]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_SumValuePoints]
  as begin
	select Sum(ValuePoint) as TotalCount from BookingMegaball 
  end
GO
/****** Object:  StoredProcedure [dbo].[SP_SumValuePointByMemberID]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SumValuePointByMemberID]
	 (
		@memberID int
	 )
	 as begin 
		 select sum(ValuePoint) as TotalSum from bookingmegaball where MemberID=@memberID
	end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberPaging]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SearchMemberPaging]
(
	@keyword nvarchar(250)
)	
as
begin
select COUNT(Email) as Totalkeyword from Member where Email like N'%'+@keyword+'%';
end
GO
/****** Object:  StoredProcedure [guest].[SP_SearchMemberNoPaging]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_SearchMemberNoPaging]
(
	@Keyword nvarchar(400))
as begin 
	select * from Member m

    where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberNoPaging]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_SearchMemberNoPaging]
(
	@Keyword nvarchar(400))
as begin 
	select * from Member m

    where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
end
GO
/****** Object:  StoredProcedure [guest].[SP_SearchMemberByBookingMegaball]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [guest].[SP_SearchMemberByBookingMegaball]
(
	@Keyword nvarchar(400),
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
	)
as begin 
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,b.*,m.Email 
	 from BookingMegaball b left join Member m on b.MemberID=m.MemberID
	-- where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	--) as product
	--where Row>=@start and Row<=@end
    where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberByBookingMegaball]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_SearchMemberByBookingMegaball]
(
	@Keyword nvarchar(400),
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
	)
as begin 
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,b.*,m.Email 
	 from BookingMegaball b left join Member m on b.MemberID=m.MemberID
	-- where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	--) as product
	--where Row>=@start and Row<=@end
    where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberByAdmin]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_SearchMemberByAdmin]
(
	@Keyword nvarchar(400),
	@start int,
	@end int,
	@fromDate datetime = null,
	@toDate datetime = null
)

as
begin

--select * from Member m

--where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,m.* from Member m
where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
and (@fromDate= null or m.CreateDate between @fromDate and @toDate))
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchCompanyByKeyword]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SearchCompanyByKeyword]
(
	@Keyword nvarchar(400)
)

as
begin

select * from CompanyInfomation c

where ((@Keyword is null) or (@Keyword='0') or (c.CompanyName like '%' + @Keyword + '%') or c.Email like '%' + @Keyword + '%' )

end;
GO
/****** Object:  StoredProcedure [dbo].[SP_Member_Wallet_SELPOINT]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_Member_Wallet_SELPOINT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 11.01.2018
	DESCRIPTION: LOAD INFO POINT AND ADDRESS BY MEMBERID
*/
(
	@MemberID int
)
as
begin
	SELECT MEM.Points,
	       MW.WalletAddress,
		   MW.RippleAddress,
		   MW.SerectKeyRipple
	FROM Member_Wallet MW
	JOIN Member MEM
	  ON MEM.MemberID = MW.MemberID
	WHERE MW.MemberID = @memberID;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SELPOINT]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[SP_MEMBER_SELPOINT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 02.03.2018
	DESCRIPTION: LOAD POINT BY MEMBERID
*/
(
	@MemberID int
)
AS BEGIN 
	SELECT MEMBER.Points
	FROM MEMBER
	WHERE MEMBER.MemberID = @MemberID;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SELBYMEMID]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_MEMBER_SELBYMEMID]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 16.01.2018
	DESCRIPTION: Load Info Member By MemberID
*/
(
	@MemberID int
)
AS BEGIN
	SELECT M.MemberID,
	       M.Email,
		   M.E_Wallet,
		   M.Password,
		   M.Points,
		   M.FullName,
		   M.IsUserType,
		   M.UserTypeID,
		   --M_W.WalletAddress,
		   --M_W.RippleAddress,
		   --M_W.SerectKeyRipple,
		   M.Points,
		   M.NumberTicketFree,
		   M.LinkReference
	FROM MEMBER M
	--JOIN Member_Wallet M_W
	--  ON M_W.MemberID = M.MemberID
	WHERE M.IsDelete = 0
	      AND M.MemberID = @MemberID;
END;
GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SEL_CHECKMEM]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_MEMBER_SEL_CHECKMEM]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 16.01.2018
	DESCRIPTION: CHECK MEMBER FACEBOOK EXISTS
*/
(
	@Email nvarchar(250)
)
AS BEGIN
	SELECT M.MemberID
	FROM MEMBER M
	WHERE M.IsDelete = 0
	      AND M.Email = @Email;
END;
GO
/****** Object:  StoredProcedure [dbo].[SP_Member_ResetPassword]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Member_ResetPassword]   
    @Email nvarchar(250)
AS   
    Update Member set Password = '' where Email = @Email
GO
/****** Object:  StoredProcedure [dbo].[SP_Member_DEL]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_Member_DEL]   
    @memberID int
AS   
    delete Member where MemberID = @memberID
GO
/****** Object:  StoredProcedure [dbo].[SP_LoginMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LoginMemberFE]
(
	@email nvarchar(250),
	@password nvarchar(250)


)
as begin 
	select m.* from member m where Email=@email and Password=@password and m.IsActive=1 and m.IsDelete=0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LoginManagerAccount]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LoginManagerAccount]
(
	@userName nvarchar(50),
	@pass nvarchar(250)
)
as begin
        select UserName,Password,ac.GroupID from admin a left join AccessRight ac on a.AdminID=ac.AdminID
		 where UserName=@userName and Password=@pass and IsActive=1 and IsDelete=0;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LoginAdmin]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_LoginAdmin]
(
	@userName nvarchar(250),
	@pass nvarchar(250)
)
as 
begin
select UserName,Password,ac.GroupID from admin a left join AccessRight ac on a.AdminID=ac.AdminID 
where UserName=@userName 
and Password=@pass 
and IsActive=1 
and IsDelete=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndUnlockMemberFE]
(
	@isActive smallint,
	@memberID int
)
as begin 
	update member set IsActive=@isActive where MemberID=@memberID and IsActive = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockMember]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndUnlockMember]
(
	@isActive smallint,
	@memberID int
)
as begin 

		update member set IsActive=@isActive where MemberID=@memberID and IsActive = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockExchangeTicket]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockExchangeTicket]
 (
 @exchangeID int,
 @isActive smallint
 )
 as begin
	update ExchangeTicket set IsActive=@isActive where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockExchangePoint]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockExchangePoint]
 (
 @exchangeID int,
 @isActive smallint
 )
 as begin
	update ExchangePoint set IsActive=@isActive where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockCountry_CMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockCountry_CMS](
	@CountryID int,
	@IsActive smallint
 )
 as begin
		update Country set IsActive=@IsActive where CountryID=@CountryID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardNumber]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardNumber]
 (
	@isActive smallint,
	@numberID int
 )
 as begin 
		update awardnumber set IsActive=@isActive where NumberID=@numberID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardMegaball]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardMegaball]
 (
	@numberID int,
	@isActive smallint
 )
 as begin
		update AwardMegaball set IsActive=@isActive where NumberID=@numberID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardFE]
(
	@isActive smallint,
	@awardID int
)
as begin 
	update award set IsActive=@isActive where AwardID=@awardID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAward]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAward]
 (
	@awardID int,
	@isActive smallint
 )
 as begin
		update award set IsActive=@isActive where AwardID=@awardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndLockConfigTicket_CMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndLockConfigTicket_CMS]
 (
	@configID int,
	@isActive smallint
 )
 as begin
		update TicketConfig set IsActive=@isActive where ConfigID=@configID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletCoinFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionWalletCoinFE]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime,
	@coinID nchar(10)
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from TransactionCoin tp left join Coin c on tp.CoinID= c.CoinID
						 left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and(tp.CoinID=@coinID)
and (@fromDate ='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
and status=0
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletBySearchFrontEnd]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionWalletBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from TransactionCoin tp left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@fromDate IS NULL or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionWalletByMemberFE]
(
@memberID int,
@start int,
@end int
)
as begin
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from TransactionCoin tp left join member m on tp.MemberID=m.MemberID 
	where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionsBookingByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionsBookingByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID
	 where b.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointStatus]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPointStatus]
(
	@status int,
	@transactionID int
)
as begin 
		update transactionpoint set Status=@status where TransactionID=@transactionID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointPage]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionPointPage]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID) as Products
		  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointBySearchFrontEnd]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPointBySearchFrontEnd]
/*
	EDITED BY: LÝ KIẾN ĐỨC
	EDITED DATE: 05.03.2018
	DESCRIPTION: CHANGE @FROMDATE = '1/1/1990 12:00:00 AM' -> @FROMDATE IS NULL
*/
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
	
)
as
begin
--select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
--tp.*,m.Email,m.E_Wallet 
--from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
--where (@memberID=0 or tp.MemberID=@memberID)

--and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
--) as Products  where Row>=@start and Row<=@end

	SELECT *
	FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY TP.CREATEDATE DESC) AS ROW,
			       SUM(1) OVER()AS TOTALROWS, 
				   TP.*,
				   M.EMAIL,
				   M.E_WALLET 
			FROM TRANSACTIONPOINT TP 
			LEFT JOIN MEMBER M 
			       ON TP.MEMBERID = M.MEMBERID 
			WHERE (@MEMBERID = 0 OR TP.MEMBERID = @MEMBERID)
				   AND (@FROMDATE IS NULL 
					    OR TP.CREATEDATE BETWEEN @FROMDATE AND @TODATE)
	) AS PRODUCTS  
	WHERE ROW >= @START 
		  AND ROW <= @END
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointBySearchCMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPointBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime =null,
	@toDate datetime=null,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@status=-1 or tp.Status=@status)
and (@fromDate=null or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPointByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
	where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPoint]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPoint]
as begin 
	select tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID  order by tp.CreateDate DESC	
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageBySearchFrontEnd]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPackageBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,p.PackageName,p.PackageNameEnglish,m.Email,m.E_Wallet 
from transactionpackage tp left join package p on tp.PackageID=p.PackageID 
left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageBySearchCMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPackageBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,p.PackageName,p.PackageNameEnglish, m.Email,m.E_Wallet 
from transactionpackage tp left join package p on tp.PackageID=p.PackageID 
left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@status=-1 or tp.Status=@status)
and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageByMemberFE]
(
@memberID int
)
as begin 
	select tp.*,p.PackageName,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID where tp.MemberID=@memberID  order by CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageByMember_FE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageByMember_FE]
(
@end int,
@start int,
@memberID int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS,tp.*,p.PackageName,p.PackageNameEnglish,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID
	 where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageActiveByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageActiveByMemberFE]
(
@memberID int
)
as begin 
	select top 1 * from TransactionPackage where MemberID=@memberID and Status=1 order by ExpireDate desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinStatus]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinStatus]
(
	@status int,
	@transactionID char(150)
)
as begin 
		update TransactionCoin set Status=@status where TransactionID=@transactionID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinPaging]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinPaging]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tc.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tc.* from TransactionCoin tc) as Products
		  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinID]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionCoinID]
(
	@start int,
	@end int,
	@fromDate Datetime,
	@toDate Datetime
)
as begin 
select * from (
							SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
							tc.* from TransactionCoin tc left join member m on tc.MemberID=m.MemberID
												left join Coin c on tc.CoinID=c.CoinID 
							where (@fromDate= null or m.CreateDate between @fromDate and @toDate)
							)
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinBySearchCMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime =null,
	@toDate datetime=null,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tc.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tc.*,m.Email
from TransactionCoin tc left join member m on tc.MemberID=m.MemberID 
where (@memberID=0 or tc.MemberID=@memberID)
and (@status=-1 or tc.Status=@status)
and (@fromDate=null or tc.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoin]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionCoin]
as begin 
	select tc.*,m.Email from TransactionCoin tc left join member m on tc.MemberID=m.MemberID  order by tc.CreateDate DESC	
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingOrderByDateFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingOrderByDateFE]
(
	@start int,
	@end int
)
as begin 
	select * from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet
	 from bookingmegaball b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingMegaballByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingMegaballByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select * from(SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
	 b.*,m.Email,m.E_Wallet from BookingMegaball b left join member m on b.MemberID=m.MemberID where b.MemberID=@memberID and b.Status = 1  
	 ) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByMemberPagingFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByMemberPagingFE]
(
	@memberID int,
	@start int,
	@end int
)	
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet 
	from bookingmegaball b left join member m on b.MemberID=m.MemberID left join AwardMegaball aw on b.CreateDate=aw.CreateDate
	 where b.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionBookingByMemberFE]
(
	@memberID int
)
as begin 
	select b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID where b.MemberID=@memberID  order by CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByDateFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByDateFE]
(
	@fromDate datetime,
	@toDate datetime
)
as begin 
	select Distinct b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID where b.CreateDate between @fromDate and @toDate and b.Status=1 order by b.CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByDate_FE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByDate_FE]
(
	@fromDate datetime,
	@toDate datetime
)
as begin 
	select b.*,m.Email,m.E_Wallet 
	from bookingmegaball b 
	left join member m 
	       on b.MemberID=m.MemberID 
    where (b.CreateDate between @fromDate and @toDate) 
	      and b.Status=1 
    order by b.CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTicketWinningByMemberID]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTicketWinningByMemberID]
(
	@memberID int
)
as begin
		select BM.*
		from 
		BookingMegaball BM
		join AwardMegaball AM
		on BM.DrawID = am.DrawID
		where BM.MemberID = @memberID;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTicketConfig]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTicketConfig]

as begin 
	select *from TicketConfig 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTicketByMonth_CMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[SP_ListTicketByMonth_CMS]
(
	@IsFreeTicket smallint,
	@Year int
)
as
begin
select COUNT(bookingid) TotalTicket, 
MONTH(CreateDate) as MonthDate 
from BookingMegaball 
where MONTH(CreateDate) in (1,2,3,4,5,6,7,8,9,10,11,12) 
and YEAR(CreateDate)=@Year
and (@IsFreeTicket=-1 or IsFreeTicket=@IsFreeTicket)
group by MONTH(CreateDate);
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListSummaryPrize_CMS]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[SP_ListSummaryPrize_CMS]
as
begin
select sum(tw.awardvalue) as awardvalues, 
count(memberID) as TotalMember,
a.AwardID,
a.AwardName
 from TicketWinning tw
left join Award a on tw.AwardID=a.AwardID
group by a.AwardID,a.AwardName;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListMemberReferenceFEByMemberID]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListMemberReferenceFEByMemberID]
(
	@MemberID int
)
as begin 
	select *from MemberReference where MemberID = @MemberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListMember_FE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListMember_FE]
(
	@start int,
	@end int
)
as
begin
select *from 
(SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,
m.MemberID,
mb.RippleAddress,
m.FullName,
m.Avatar,
m.Mobile,
m.Email,
m.Points,
mb.NumberCoin,
mb.IndexWallet,
mb.WalletAddress
from Member m left join Member_Wallet mb on m.MemberID=mb.MemberID where m.IsDelete=0 
group by m.MemberID,
mb.RippleAddress,
m.FullName,
m.Avatar,
m.Mobile,
m.Email,
m.Points,
mb.NumberCoin,
mb.IndexWallet,
mb.WalletAddress) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListGroupAdmin]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListGroupAdmin]
as begin
		select *from groupadmin
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListExchangeTicketByMemberFE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListExchangeTicketByMemberFE]
(
	@memberID int,
	@CoinID nchar(10)
)
as begin
	select *from ExchangeTicket where memberID = @memberID and isdelete = 0 and isactive = 1 and CoinID = @CoinID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListExchangePointByMember]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListExchangePointByMember]
(
@memberID int
)
as begin 
	select *from ExchangePoint where memberID = @memberID and isdelete = 0 and isactive = 1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListDetailsByMemberIDRef]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListDetailsByMemberIDRef]
(
	@MemberID int
)
as begin 
	select *from MemberReference where MemberIDReference = @MemberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateUserNamedAdmin]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateUserNamedAdmin]
(
	@userName nvarchar(50),
	@adminID int
)
as begin
		update admin set UserName=@userName where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateUserNameAndPasswordAdmin]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateUserNameAndPasswordAdmin]
(
	@adminID int,
	@userName nvarchar(50),
	@password nvarchar(250)
)
as begin 
		update admin set UserName=@userName,Password=@password where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateTicketConfig_CMS]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateTicketConfig_CMS]
  (
	@ConfigName nvarchar(250),
	@ConfigID int,
	@NumberTicket int,
	@CoinValues numeric(18,10),
	@CoinID nvarchar(250),
	@UpdateDate Datetime
 )
 as begin  
		update TicketConfig set CoinID=@CoinID,NumberTicket=@NumberTicket,CoinValues=@CoinValues,ConfigName=@ConfigName, UpdatedDate=@UpdateDate
		 where ConfigID=@ConfigID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusTransaction]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusTransaction]
(
	@status int,
	@transactionID int
)
as begin 
	update TransactionPackage set Status=@status where TransactionID=@transactionID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusMember]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusMember]
(
	@memberID int
)
as begin 
	update member set IsActive=1 where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusGroupAdmin]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusGroupAdmin]
(
	@isActive smallint,
	@groupID int
)
as begin
			update groupadmin set IsActive=@isActive where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusExchangeTicket]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateStatusExchangeTicket]
(
	@status int,
	@exchangeID int,
	@deleteUser nvarchar(250),
	@deleteDate datetime
)
as begin 
		update ExchangeTicket set IsDelete =@status, DeleteUser=@deleteUser,DeleteDate=@deleteDate where ExchangeID=@exchangeID 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusExchangePoint]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusExchangePoint]
(
	@status int,
	@exchangeID int,
	@deleteUser nvarchar(250),
	@deleteDate datetime
)
as begin 
		update ExchangePoint set IsDelete =@status, DeleteUser=@deleteUser,DeleteDate=@deleteDate where ExchangeID=@exchangeID 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBookingMegaball]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBookingMegaball]
(
	@status int,
	@bookingID int
)
as begin 
		update BookingMegaball set status =@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBookingFE]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBookingFE]
(
	@bookingID int,
	@status int
)
as begin 
	update booking set Status=@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBooking_FE]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBooking_FE]
(
	@bookingID int,
	@status int
)
as begin 
	update bookingmegaball set Status=@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBooking]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBooking]
(
	@status int,
	@bookingID int
)
as begin
		update booking set Status=@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusAwardWithdraw_CMS]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_UpdateStatusAwardWithdraw_CMS]
(
	@TransactionID char(50),
	@RequestStatus smallint,
	@AdminIDApprove nvarchar(250),
	@UpdatedUser nvarchar(250)
)
as
begin
update AwardWithdraw
set RequestStatus=@RequestStatus,
AdminIDApprove=@AdminIDApprove,
UpdatedUser=@UpdatedUser,
UpdatedDate=GETDATE(),
ApproveDate=GETDATE()
where TransactionID=@TransactionID
and IsDeleted=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusAdmin]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusAdmin]
(
	@isDelete smallint,
	@adminID int
)
as begin
		update admin set IsDelete=@isDelete where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusActive_CMS]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusActive_CMS]
(
	@isAutoLottery smallint,
	@configID int
)
as begin 
	update SystemConfig set IsAutoLottery=@isAutoLottery where ConfigID=@configID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningByBookingID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningByBookingID]   
	@BookingID int,
	@awardvalue decimal(18,5),
	@awardfee decimal(18,8),
	@priority int,
	@DrawID int,
	@MemberID int
AS 
BEGIN 
     SET NOCOUNT ON 
    select *
	from TicketWinning
	where BookingID = @BookingID 
	and AwardValue = @awardvalue
	and AwardFee = @awardfee
	and [Priority] = @priority
	and DrawID = @DrawID
	and MemberID = @MemberID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListSystemConfig_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListSystemConfig_CMS]
 as begin 
	select * from SystemConfig
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListMember]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListMember]
(
	@start int,
	@end int
)

 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,SUM(1) OVER()AS TOTALROWS,m.* from  member m ) as Products
		  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListLinkResetPasswordFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListLinkResetPasswordFE]
(
	@Date datetime,
	@Email nvarchar(250)
)

 as begin
		select *from LinkResetPassword 
		where CONVERT(nvarchar(250),CreatedDate, 103) = CONVERT(nvarchar(250),@Date, 103) and Email = @Email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListCountryBySearch_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListCountryBySearch_CMS]
(
	@Keyword nvarchar(400),
	@start int,
	@end int
)
as begin
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.CreatedDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,c.* from Country c
where ((@Keyword is null) or (@Keyword='0') or c.CountryName like '%' + @Keyword + '%' ))
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListCountry_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListCountry_CMS]
(
		@start int,
		@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.CountryName Asc) as Row,SUM(1) OVER()AS TOTALROWS, c.* 
		from Country c where IsActive=1 and IsDeleted=0) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListBookingByRaw]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListBookingByRaw]
 (
	@Raw int
 )
 as begin

		SELECT BookingMegaball.*,Member.Email
		from BookingMegaball
		join Member 
		on BookingMegaball.MemberID = Member.MemberID
		where Convert(varchar,BookingMegaball.OpenDate,103) in (select Convert(varchar,AwardMegaball.CreateDate,103) from AwardMegaball where AwardMegaball.NumberID = @Raw)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardOrderByDateFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardOrderByDateFE]

as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.drawID desc) as Row,awn.*,aw.AwardName,aw.AwardValue 
	from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	 where awn.IsActive=1 and awn.IsDelete=0 ) as Products
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardNumberByDate]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardNumberByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,aw.*,a.AwardName,a.AwardNameEnglish from awardnumber aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardNumber]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardNumber]
 (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,aw.*,a.AwardName,a.AwardNameEnglish
		 from awardnumber aw left join award a on aw.AwardID=a.AwardID) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [guest].[SP_GetListAwardMegaballResultByDate]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_GetListAwardMegaballResultByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,
		aw.*,a.AwardName,a.AwardNameEnglish from awardmegaball aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaballResultByDate]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMegaballResultByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,
		aw.*,a.AwardName,a.AwardNameEnglish from awardmegaball aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end and IsActive=1 and IsDelete=0
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaballFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMegaballFE]
(
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.NumberID desc) as Row, sum(1) over() as TOTALROWS,awn.*,aw.AwardName,aw.AwardValue from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaball_FE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardMegaball_FE]
as begin 
	select * from AwardMegaball where IsActive=1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMagaball]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMagaball]
  (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY DrawID DESC)as row, sum(1) over() as TOTALROWS
		 ,aw.*,a.AwardNameEnglish from awardmegaball aw left join  Award a on aw.AwardID=a.AwardID where aw.IsActive=1 and aw.IsDelete=0) as Products  where Row>=@start and Row<=@end 
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardFE]
(
@start int,
@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY AwardID DESC) as Row,aw.* from award aw) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardDetail_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardDetail_CMS]
 (@awardID int)
 as begin
	select * from Award where AwardID=@awardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDayFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDayFE]
(
	@fromDate datetime,
	@toDate datetime

)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID
	 where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDateFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDateFE]
(
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int

)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from awardnumber awn left join award aw on awn.AwardID=aw.AwardID 
	where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDate]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDate]
(
	@date datetime
 )
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Createdate desc) as Row,awn.*,aw.AwardName,aw.AwardValue 
	from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	 where awn.IsActive=1 and awn.IsDelete=0 and Convert(varchar,awn.CreateDate,103) = CONVERT(varchar,@date, 103) ) as Products
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAward]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAward]
 (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY AwardID DESC)
		 as Row,sum(1) over() as TOTALROWS,
		  aw.* from award aw) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetGroupAdminDetails]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetGroupAdminDetails]
(@groupID int)
as begin
		select *from groupadmin where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetGroupAdminDetail]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetGroupAdminDetail]
(@groupID int)
as begin
		select *from groupadmin where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetFreeTicketDailyActive]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetFreeTicketDailyActive]
as begin
		select * from FreeTicketDaily where IsActive=1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetDrawIDByDate]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetDrawIDByDate]
(
	@date datetime
)
as begin 
	select * from draw where @date >= startdate and @date <= enddate order by createddate desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetDetailLinkResetPasswordFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetDetailLinkResetPasswordFE]
(
	@LinkReset nvarchar(1000)
)
as
begin
	SELECT *
	FROM LinkResetPassword
	WHERE LinkReset = @LinkReset and status = 0 and ExpireLink >= SYSDATETIME();
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetCompanyByMemberID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetCompanyByMemberID]
(
	@memberID int
)
as begin 
	select * from CompanyInfomation where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardWithdrawDetail_FE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetAwardWithdrawDetail_FE]
(
	@RequestMemberID nvarchar(250),
	@AwardDate datetime,
	@BookingID int
)
as
begin
select * from AwardWithdraw
where convert(varchar(10), AwardDate, 101)=convert(varchar(10), @AwardDate, 101)
and RequestMemberID=@RequestMemberID
and IsDeleted=0
and (@BookingID=-1 or BookingID=@BookingID);
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardWithdrawDetail_CMS]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_GetAwardWithdrawDetail_CMS]
(
	@TransactionID nvarchar(250)
)
as
begin
select * from AwardWithdraw
where TransactionID=@TransactionID
and IsDeleted=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardMegabalByDrawID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetAwardMegabalByDrawID]
  (
	@DrawID int
 )
 as begin
		
		select * from AwardMegaball where DrawID=@DrawID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardMagaballByID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_GetAwardMagaballByID]
(
	@NumberID int
)
as begin 
	select *from AwardMegaball where NumberID = @NumberID and IsActive = 1 and IsDelete = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAwardWithDraw_FE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetAllAwardWithDraw_FE]
as begin 
	select *from AwardWithdraw where AwardWithdraw.IsDeleted=0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAwardFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_GetAllAwardFE]

as begin 
	select *from award where IsActive = 1 and IsDelete = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GerAdminDetailByID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GerAdminDetailByID](@adminID nvarchar(50))
as begin
		select * from admin where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GerAdminDetail]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GerAdminDetail](@userName nvarchar(50))
as begin
		select * from admin where UserName=@userName
end
GO
/****** Object:  StoredProcedure [dbo].[TransactionPoint_Sel]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPoint_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select t.MemberID, t.CreateDate, t.Points, t.Status, t.TransactionID, m.E_Wallet,t.TransactionCode from TransactionPoint t, Member m where t.MemberID = m.MemberID and m.MemberID = @MemberID order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[TransactionPoint_Insert]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPoint_Insert]   
    @MemberID int,
    @Status int,
    @Points int,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS
BEGIN 
     SET NOCOUNT ON 
    insert into TransactionPoint(MemberID,Status,Points,CreateDate,TransactionCode) values(@MemberID,@Status,@Points,@CreateDate,@TransactionCode) 
    
END
GO
/****** Object:  UserDefinedFunction [guest].[fc_CountNewAward]    Script Date: 08/02/2018 19:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [guest].[fc_CountNewAward]()
returns int
as begin
	declare @numberAward int
	set @numberAward=(select COUNT(aw.AwardID) from AwardMegaball aw
	where DATEADD(DD, DATEDIFF(DD, 0, aw.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) 
	); 
	return(@numberAward);
end;
GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Sel_OrderByDate]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[TransactionPackage_Sel_OrderByDate]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select top 1 t.CreateDate, t.ExpireDate, t.MemberID, t.PackageID, t.Status, t.TransactionID, p.PackageName,t.TransactionCode from TransactionPackage t, Package p where t.PackageID = p.PackageID and MemberID = @MemberID and t.Status = 1 order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Sel]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPackage_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select t.CreateDate, t.ExpireDate, t.MemberID, t.PackageID, t.Status, t.TransactionID, p.PackageName,t.TransactionCode from TransactionPackage t, Package p where t.PackageID = p.PackageID and MemberID = @MemberID order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Insert]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPackage_Insert]   
    @MemberID int,
    @PackageID int,
    @Status int,
    @ExpireDate datetime,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS
BEGIN 
     SET NOCOUNT ON 
    insert into TransactionPackage(MemberID,PackageID,Status,ExpireDate,CreateDate,TransactionCode) values(@MemberID,@PackageID,@Status,@ExpireDate,@CreateDate,@TransactionCode) 
    
END
GO
/****** Object:  StoredProcedure [dbo].[CheckEmailExists]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[CheckEmailExists]
(
	@email nvarchar(250)
)
 as begin
		select Email from member where Email=@email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteAccessRight]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_DeleteAccessRight]
(@adminID int)
as begin
		delete from accessright where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountMemberBuyNumberFE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMemberBuyNumberFE]
(
	@fromDate datetime,
	@toDate datetime,
	@numberValue int,
	@memberID int
)
as begin 
	select count(BookingID) as TotalRecord from booking where CreateDate between @fromDate and @toDate and NumberValue=@numberValue and MemberID not in(@memberID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountMemberBuyNumber_FE]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMemberBuyNumber_FE]
(
	@fromDate datetime,
	@toDate datetime,
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fiveNumber int,
	@memberID int


)
as begin 
	select count(BookingID) as TotalRecord from bookingmegaball 
	where CreateDate between @fromDate and @toDate and FirstNumber=@firstNumber and SecondNumber=@secondNumber and ThirdNumber=@thirdNumber and FourthNumber=@fourthNumber and FivethNumber=@fiveNumber and MemberID not in(@memberID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountMember]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMember](@curdate datetime)
  as begin 
  select count(MemberID) as TotalCount from BookingMegaball where CreateDate=@curdate
  end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountAllBookingMegaball]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountAllBookingMegaball]
as begin
	select COUNT(b.BookingID) as TotalAllBookingMegaball,
	dbo.CountNewBookingMegaball() as TotalNewBookingMegaball
	from BookingMegaball b;
end
GO
/****** Object:  StoredProcedure [guest].[SP_CountAllAwardMegaball]    Script Date: 08/02/2018 19:21:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_CountAllAwardMegaball]
as begin 
	select COUNT(aw.AwardID) as TotalAllAward,
	guest.fc_CountNewAward() as TotalNewAward 
	from AwardMegaball aw
	where aw.IsActive=1 and aw.IsDelete=0;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountAllAwardMegaball]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_CountAllAwardMegaball]
as begin 
	select COUNT(aw.AwardID) as TotalAllAward,
	guest.fc_CountNewAward() as TotalNewAward 
	from AwardMegaball aw
	where aw.IsActive=1 and aw.IsDelete=0;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingWinningBySearch_FE]    Script Date: 08/02/2018 19:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingWinningBySearch_FE]
(
	@memberID int,
	@start int,
	@end int,
	@fromDate Datetime,
	@todate Datetime
)
as begin 
	select * from(SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
	 b.*,m.Email,m.E_Wallet,
	 dbo.GetAwardWithdrawRequestStatus(m.Email,b.BookingID,b.OpenDate) as AwardWithdrawStatus
	  from BookingMegaball b left join member m on b.MemberID=m.MemberID
	  where b.MemberID=@memberID and b.Status = 1  and (@fromDate IS NULL or b.CreateDate between @fromDate and @toDate)
	 ) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningBySearch]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningBySearch]   
	@memberID int,
	@start int,
	@end int,
	@startdate datetime,
	@enddate datetime
AS 
BEGIN 
     SET NOCOUNT ON 
    select *from (SELECT ROW_NUMBER() OVER (ORDER BY tw.AwardDate DESC) as row, sum(1) over() as TotalRecord,tw.*,
	dbo.GetAwardWithdrawRequestStatus(mb.Email,tw.BookingID,tw.AwardDate) as AwardWithdrawStatus
	from TicketWinning tw
	join Member mb on mb.MemberID = tw.MemberID
	where (@memberID=0 or tw.MemberID = @memberID) and (tw.AwardDate between @startdate and @enddate)) as Products  where row>=@start and row<=@end
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningByMemberID]    Script Date: 08/02/2018 19:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningByMemberID]   
	@MemberID int,
	@start int,
	@end int
AS 
BEGIN 
     SET NOCOUNT ON 
    select *from (SELECT ROW_NUMBER() OVER (ORDER BY tw.buydate DESC) as row, sum(1) over() as TotalRecord,tw.*,
	dbo.GetAwardWithdrawRequestStatus(mb.Email,tw.BookingID,tw.AwardDate) as AwardWithdrawStatus
	from TicketWinning tw
	join Member mb on mb.MemberID = tw.MemberID
	where tw.MemberID = @MemberID) as Products  where row>=@start and row<=@end
	
END
GO
/****** Object:  StoredProcedure [dbo].[CountAllPoints]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllPoints]
as
begin
select COUNT(p.TransactionID) as TotalAllPoints,
dbo.CountNewPoint() as TotalNewPoints
 from TransactionPoint p;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllPackage]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllPackage]
as
begin
select COUNT(p.TransactionID) as TotalAllPackage,
dbo.CountNewPackage() as TotalNewPackage
 from TransactionPackage p;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllMember]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllMember]
as
begin
select COUNT(m.MemberID) as TotalAllMember,
dbo.CountNewMember() as TotalNewMember
 from Member m
where m.IsActive=1 
and m.IsDelete=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllCoin]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[CountAllCoin]
as
begin
select COUNT(c.TransactionID) as TotalAllCoin,
dbo.CountNewCoin() as TotalNewCoin
 from TransactionCoin c;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllBooking]    Script Date: 08/02/2018 19:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllBooking]
as
begin
select COUNT(b.BookingID) as TotalAllBooking,
dbo.CountNewBooking() as TotalNewBooking
 from Booking b;
end;
GO
/****** Object:  Default [DF_Award_NumberBallGold]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_NumberBallGold]  DEFAULT ((0)) FOR [NumberBallGold]
GO
/****** Object:  Default [DF_Award_NumberBallNormal]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_NumberBallNormal]  DEFAULT ((0)) FOR [NumberBallNormal]
GO
/****** Object:  Default [DF_AwardMegaball_JackPotWinner]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[AwardMegaball] ADD  CONSTRAINT [DF_AwardMegaball_JackPotWinner]  DEFAULT ((0)) FOR [JackPotWinner]
GO
/****** Object:  Default [DF_AwardMegaball_TotalWon]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[AwardMegaball] ADD  CONSTRAINT [DF_AwardMegaball_TotalWon]  DEFAULT ((0)) FOR [TotalWon]
GO
/****** Object:  Default [DF_BookingMegaball_IsFreeTicket]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[BookingMegaball] ADD  CONSTRAINT [DF_BookingMegaball_IsFreeTicket]  DEFAULT ((0)) FOR [IsFreeTicket]
GO
/****** Object:  Default [DF_Country_IsActive]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Country_IsDeleted]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_IPConfig_IsDeleted]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[IPConfig] ADD  CONSTRAINT [DF_IPConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_Member_IsLoginType]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_IsLoginType]  DEFAULT ((0)) FOR [IsUserType]
GO
/****** Object:  Default [DF_Member_NumberTicketFree]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_NumberTicketFree]  DEFAULT ((0)) FOR [NumberTicketFree]
GO
/****** Object:  Default [DF_SystemConfig_IsAutoLottery]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[SystemConfig] ADD  CONSTRAINT [DF_SystemConfig_IsAutoLottery]  DEFAULT ((0)) FOR [IsAutoLottery]
GO
/****** Object:  Default [DF_TicketConfig_IsDeleted]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[TicketConfig] ADD  CONSTRAINT [DF_TicketConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_TicketWinning_Status]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[TicketWinning] ADD  CONSTRAINT [DF_TicketWinning_Status]  DEFAULT ((0)) FOR [Status]
GO
/****** Object:  Default [DF_TypeConfig_IsDeleted]    Script Date: 08/02/2018 19:21:32 ******/
ALTER TABLE [dbo].[TypeConfig] ADD  CONSTRAINT [DF_TypeConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
